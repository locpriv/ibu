# A java library for estimating distributions on private data

The main objective of this library is to provide an experimental framework for evaluating the quality of estimating statistical properties on private data of the users. Precisely it allows modeling the sensitive data, obfuscating these data using a variety of privacy mechanisms, estimating the probability distribution on the original data using different estimation methods, and measuring the statistical distance between the original and estimated distributions.

## Current features of the library

1. Modeling the user sensitive datum flexibly as a discrete `location' (x,y) in a 2-dimensional grid.
2. The user data can be artificially sampled from a probability distribution.
3. The user data may be drawn from a real geographic dataset.   
4. Various mechanisms are available to obfuscate every user datum to produce sanitized data. 
5. Various statistical estimation methods are available to reconstruct the distribution of the original data. 
6. Various statistical distances are available to compare between the original and estimated distributions. 

## Structure of the source code:
The structure of the implementation is reflected by the organization of the subdirectories in the main directory [src](https://gitlab.com/locpriv/jpriv/tree/master/src) as follows. 
- [src/priv](https://gitlab.com/locpriv/jpriv/tree/master/src/priv) contains the implementation of privacy mechanisms, probability distributions on locations as grid cells, and tools to access dataset files (gdset), and privacy metrics, quality of service loss functions.
- [src/stat](https://gitlab.com/locpriv/jpriv/tree/master/src/stat) contains mainly the implementation of statistical estimators and representations for real and noisy data. 


## Supported privacy mechanisms
Source code available in: [src/priv/line/mechanisms/](https://gitlab.com/locpriv/jpriv/tree/master/src/priv/line/mechanisms/) and [src/priv/plan/mechanisms/](https://gitlab.com/locpriv/jpriv/tree/master/src/priv/plan/mechanisms/)

- Truncated Planar Laplace mechanism (TPLapMechanism)
- Truncated Planar Geometric mechanism (TPGeomMechanism)
- Truncated Linear Geometric mechanism (TGeomMechanism)
- Exponential mechanism (ExponentialMechanism)
- Basic One-Time Rappor mechanism (BasicOneTimeRappor)
- Exponential mechanism (ExponentialMechanism)
- k-ary Randomized Response mechanism (PRRespMechanism)
- Tight-Constraints mechanism (TCRectMechanism, TCSqtMechanism) 
- Truncated Planar Geometric mechanism (TPGeomMechanism)
- Arbitrary mechanism defined by a stochastic matrix (MatrixFileMechanism)

## Supported estimation methods 
Source code available in: [src/stat/estimators/](https://gitlab.com/locpriv/jpriv/tree/master/src/stat/estimators/)

- Expectation-Estimation method (EM)
- Matrix Inversion with normalization (INVN) 
- Matrix Inversion with Projection (INVP) 
- Matrix Inversion for RAPPOR with normalization (BOTRAPPOR_INVN) 
- Matrix Inversion for RAPPOR with projection (RAPPOR_INVP)
- Matrix Inversion for kRR with normalization (KRR_INVN)
- Matrix Inversion for kRR with projection (KRR_INVP)

## Dataset (gdata)
Our implementation uses the [Gowalla](https://snap.stanford.edu/data/loc-gowalla.html) dataset as a source of real user data. In our experiments, the library extracts the data for any region specified by the latitudes (north, south), and the longitudes (east, west). The directory [src/gdata/](https://gitlab.com/locpriv/jpriv/blob/master/gdata/) contains the data extracted for a zone in San Francisco, which we used for our experiments. 


## Native Libraries
These are available in [src/lib/](https://gitlab.com/locpriv/jpriv/blob/master/lib/):
- fastemd*.jar and libemd*.dylib: An efficient library that evaluates the earth mover's distance (EMD). 
- glpk-java*.jar: A library for solving Linear Programs. 
- jblas*.jar and libjblas*.jnilib : An efficient library for evaluating Linear Algebraic expressions. 

