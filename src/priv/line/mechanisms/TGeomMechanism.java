package priv.line.mechanisms;

import priv.plan.mechanisms.Mechanism;

// A linear truncated geometric mechanism: hsize =1
public class TGeomMechanism extends Mechanism {
	
      private double eps;                    // used to construct TGeometric matrix 
      
    // this constructor creates the TGeom mechanism 
    // specified by the size of the grid, the cell_side (in km), the type, and privacy parameter
    public TGeomMechanism(int p_vsize, double p_cell_side, double p_eps) {
    	super(1,p_vsize,p_cell_side);	
        eps = p_eps;
    	String info = "// Linear Truncated Geormetric mechanism  \n"
                + "// epsilon =" + eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
        add_info(info);
    }
    
 	/**
 	 *  returns the entry of the truncated geometric mechanism for input x,y and output x,y. 
 	 *  here p_in_x, p_out_x are note used. 
 	 */
 	public double getProb(int p_in_x,int p_in_y,int p_out_x,int p_out_y) {
 	 	 //  This implementation is based on the fact that this mechanism is also a TC mechanism 
 	 	 //  Therefore it evaluates the diagonal element c[out_y, out_y] and then the evaluate the 
 		 // required probability as c[out_y, out_y]*alpha^d(in_y, out_y).
 		double alpha = Math.exp(-eps*cell_side);
 		double prob_y;

 		if (p_out_y==0 || p_out_y==vsize-1) 
 			prob_y= (double)1/(1+alpha);
 		else prob_y = (double) (1-alpha)/(1+alpha);
 		prob_y = prob_y * Math.pow(alpha, Math.abs(p_out_y-p_in_y));
 		
 		return prob_y;
 	}
 	
}
