package priv.plan.gdset;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import priv.plan.dist.Distance;
import priv.plan.priors.Prior;
import stat.spaces.GridEmpData;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
/**
 *
 * @author javadb.com
 */
public class GDataset {
	// A GDataset object is a list of check-in records read 
	// (according to boundaries and user criterion) from a source file in Gowalla format. 
	// An instance is constructed with the following parameters: 
	// data_file name:  the file containing the records in Gowalla format. 
	// region boundaries: south, north, west, and east.
	// a user criteria: to obtain only records in which the user_id matches a given regular expression
	// read_size : number of records read from the data source file.
	
	// properties:
	// 1. region boundaries
	// 2. default cell side: the region must form a grid of square cells having side def_cell_side
	// 3. hsize,vsize: of the region grid
	// 4. list of check-ins
	// 5. number of users
	
	// methods:
	// 1. write_to_file(p_data_file_name): write the data to a file in Gowalla format.
	// 2. write_map_script: write a jscript file to plot the checkins in the dataset. 
	// 3. get_prior(cell_side): use cell_side to construct a grid and return a global prior on this grid.
	//    Here cell_side must be multiples of def_cell_side, to get fine-grained prior.
	// 4. write_all_priors_to_file(p_priors_file_name): 
	
	// important: mapping between x,y coordinates of the cells and the lat/lng geographical coordinates
	// necessary to construct priors. 
	// x coordinate increases as we go in the east direction
	// y coordinate increases as we go towards the north.
	

	private double south;
	private double north;
	private double west;
	private double east;

	private List<GCheckin> checkins;
	
	private double def_cell_side;   
	// At the construction, the boundaries of the region is adjusted according to a 
	// (default) def_cell_side to form a grid of square cells. def_cell_side is 
	// also used to construct the grid on a the map when required to be shown.
	
    private int num_users=0;  // number of users in the data set
    private String dataset_info;

    
    
    // if read_size is not specified it is set to a number larger than Gowalla dataset.
    public GDataset(String p_data_file, double p_south, double p_north, double p_west, double p_east, 
			double p_def_cell_side, String p_user_id_regex) {
    	this(p_data_file, p_south, p_north, p_west, p_east, 
    			p_def_cell_side, p_user_id_regex, GSettings.default_read_size);
    }

    
    public GDataset(String p_data_file, double p_south, double p_north, double p_west, double p_east, 
			double p_def_cell_side, String p_user_id_regex, int p_read_size) {

    	south = p_south;   north = p_north;
		west = p_west;     east = p_east;
		def_cell_side = p_def_cell_side;
		adjust_boundaries();
		read_checkins(p_data_file,p_user_id_regex,p_read_size);
	}

	// read the data file sequentially and store the check-ins that are in the 
	// region and match p_user_id_regex
	private void read_checkins(String p_data_file, String p_user_id_regex, int p_read_size) {
		 
		checkins = new ArrayList<GCheckin>(); // dynamic array of Check-ins

		BufferedReader br_in_data = null;      // to read from input data_file

		String in_data_line;         // to hold a line string from the input file
		try {    
			br_in_data = new BufferedReader(new FileReader(p_data_file));

			// summary data            
			int read_row_count=0;   // number of valid rows read from in_data_file
			
			String old_user_id ="*";   
		    // holds the previous user in the output to be compared 
			// with the new (curr) user.  This is important to count the users in the dataset

 			in_data_line = br_in_data.readLine();   // read the first row
			while (in_data_line != null && read_row_count<=p_read_size)
				if (in_data_line.matches("") || in_data_line.startsWith("//"))
					in_data_line=br_in_data.readLine();   // skip the commented or empty line
				else {
					read_row_count=read_row_count+1;  // additional row from source file is read
					GCheckin chkin = get_checkin_from_data_line(in_data_line);
					if (!chkin.lat.equals("") && !chkin.lng.equals(""))
						// check the current row: if required, add to output file, inc out_row_count
						if (Double.parseDouble(chkin.lat)>= south && 
						Double.parseDouble(chkin.lat)<= north && 
						Double.parseDouble(chkin.lng)>= west && 
						Double.parseDouble(chkin.lng)<= east)
							if (chkin.user_id.matches(p_user_id_regex)) {
								// add the check-in to the list
								checkins.add(chkin);
								// check if a new user is encountered
								if (!chkin.user_id.equals(old_user_id)) {
									old_user_id = chkin.user_id;
									num_users++;
								}
							}
					in_data_line=br_in_data.readLine();
				}

			// set data set info string.
			String info_str = "// Boundaries: \n";
			info_str = info_str+ "// South: "+ String.format("%03.4f", south)+
					",   North: "+ String.format("%03.4f", north)+"\n";
			info_str = info_str+ "// West:  "+ String.format("%03.4f", west)+
					",    East:  "+ String.format("%03.4f", east)+"\n";            
			info_str = info_str+ "// Number of checkins = " + checkins.size() +"\n";
			info_str = info_str+ "// filtered from  "+ (read_row_count-1)+ "  input rows"+"\n";
			info_str = info_str+ "// Total number of users in the region = " + num_users;
			dataset_info = info_str;

		} catch (FileNotFoundException ex) {
			System.out.println("input dataset file is not found"); 
			ex.printStackTrace();
			System.exit(0);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			//Close the BufferedReader, writer
			try {
				if (br_in_data != null) {
					br_in_data.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		 
	}
	
	private GCheckin get_checkin_from_data_line(String p_data_line) {
		// get the first word, user_id
		int pos_in_line=0; int pos_ahead=0;
		while (pos_in_line<p_data_line.length() && p_data_line.charAt(pos_in_line)==' ')
			pos_in_line = pos_in_line +1;  // searches for the start of word
		while (pos_ahead<p_data_line.length() && p_data_line.charAt(pos_ahead)!='\t')
			pos_ahead = pos_ahead+1;
		String user_id = p_data_line.substring(pos_in_line, pos_ahead);

		//System.out.println(curr_user_id);

		// get the 2nd word, date-time
		pos_ahead = pos_ahead+1; pos_in_line= pos_ahead;
		while (pos_in_line<p_data_line.length() && p_data_line.charAt(pos_in_line)==' ')
			pos_in_line = pos_in_line +1;  // searches for the start of word
		while (pos_ahead<p_data_line.length() && p_data_line.charAt(pos_ahead)!='\t')
			pos_ahead = pos_ahead+1;       // search for the end of the word
		String datetime = p_data_line.substring(pos_in_line, pos_ahead);

		// get the 3rd word, lat
		pos_ahead = pos_ahead+1; pos_in_line= pos_ahead;
		while (pos_in_line<p_data_line.length() && p_data_line.charAt(pos_in_line)==' ')
			pos_in_line = pos_in_line +1;  // searches for the start of word
		while (pos_ahead<p_data_line.length() && p_data_line.charAt(pos_ahead)!='\t')
			pos_ahead = pos_ahead+1;       // search for the end of the word
		String lat = p_data_line.substring(pos_in_line, pos_ahead);

		// get the 4th word, lng
		pos_ahead = pos_ahead+1; pos_in_line= pos_ahead;
		while (pos_in_line<p_data_line.length() && p_data_line.charAt(pos_in_line)==' ')
			pos_in_line = pos_in_line +1;  // searches for the start of word
		while (pos_ahead<p_data_line.length() && p_data_line.charAt(pos_ahead)!='\t')
			pos_ahead = pos_ahead+1;       // search for the end of the word
		String lng = p_data_line.substring(pos_in_line, pos_ahead);

		// get the 5th word, loc_id
		pos_ahead = pos_ahead+1; pos_in_line= pos_ahead;
		while (pos_in_line<p_data_line.length() && p_data_line.charAt(pos_in_line)==' ')
			pos_in_line = pos_in_line +1;  // searches for the start of word
		while (pos_ahead<p_data_line.length() && p_data_line.charAt(pos_ahead)!='\t')
			pos_ahead = pos_ahead+1;       // search for the end of the word
		String loc_id = p_data_line.substring(pos_in_line, pos_ahead);
		// finished a data row
		
		return new GCheckin(user_id,datetime,lat,lng,loc_id);

	}
	


	// get the number of check-ins for every user having this number 
    // above the given threshold. In this version, repeated points are 
	// considered one point. 
	public List<UserStatistic> get_users_statistics_no_repeated(int p_user_checkins_threshold) {
		
		List<UserStatistic> us_list = new ArrayList<UserStatistic>();

		    String old_user_id = checkins.get(0).user_id;
		    List<String> old_user_points = new ArrayList<String>();
		    old_user_points.add(checkins.get(0).lat+" "+checkins.get(0).lng);
		    //int old_user_checkins=1;

			for (int k=1;k<checkins.size();k++) {

				// check if a new user is the same as before
				if (checkins.get(k).user_id.equals(old_user_id)) {
					if (!old_user_points.contains(checkins.get(k).lat+" "+checkins.get(k).lng)) {
					old_user_points.add(checkins.get(k).lat+" "+checkins.get(k).lng);
					//old_user_checkins++;
					}
				}
				else {
					// process the info of the old user
					if (old_user_points.size() >= p_user_checkins_threshold) {
						UserStatistic us = new UserStatistic(old_user_id,old_user_points.size());
						us_list.add(us);
					}
					// now we have a new user
					old_user_id = checkins.get(k).user_id;
					old_user_points.clear();
					old_user_points.add(checkins.get(k).lat+" "+checkins.get(k).lng);
					//old_user_checkins=1;
				}
			}
			// process the info of the last user
			if (old_user_points.size() >= p_user_checkins_threshold) {
				UserStatistic us = new UserStatistic(old_user_id,old_user_points.size());
				us_list.add(us);
			}
			return us_list;
		}

	// get the number of check-ins for every user having this number 
    // above the given threshold. In this version, repeated checkins for the same user 
	// are considered different.   
	public List<UserStatistic> get_users_statistics(int p_user_checkins_threshold) {
		
		List<UserStatistic> us_list = new ArrayList<UserStatistic>();

		    String old_user_id = checkins.get(0).user_id;
		    int old_user_checkins=1;

			for (int k=1;k<checkins.size();k++) {

				// check if a new user is the same as before
				if (checkins.get(k).user_id.equals(old_user_id)) {
					old_user_checkins++;
				}
				else {
					// process the info of the old user
					if (old_user_checkins >= p_user_checkins_threshold) {
						UserStatistic us = new UserStatistic(old_user_id,old_user_checkins);
						us_list.add(us);
					}
					// now we have a new user
					old_user_id = checkins.get(k).user_id;
					old_user_checkins=1;
				}
			}
			// process the info of the old user
			if (old_user_checkins >= p_user_checkins_threshold) {
				UserStatistic us = new UserStatistic(old_user_id,old_user_checkins);
				us_list.add(us);
			}
			return us_list;
		}

	
	
	// return an element in the dataset
	public GCheckin get_checkin(int index) {
		return checkins.get(index);
	}
	
	// returns the number of check-ins in the dataset.
	public int get_size() {
		return checkins.size();
	}
	
	// return the max distance in the region.
	public double get_max_distance() {
		return Distance.get_distance_on_earth(north, east, south, west);
	}
	
	// returns the number of users in the data set
	public int get_num_users() {
		return num_users;
	}
	
	// get information string of the data set
	public String get_info() {
		return dataset_info;
	}
	
	
	// write the data to the given file in Gwala format.
	public void write_data_to_file(String p_data_file_name) {

		BufferedWriter bw_out_data = null;     // to write to the out_data_file
		try {
			bw_out_data = new BufferedWriter(new FileWriter(p_data_file_name));
			GCheckin chkin;
			for (int i=0;i<checkins.size();i++) {
				chkin = checkins.get(i);
				bw_out_data.write(chkin.user_id+"\t"+chkin.datetime+"\t"+chkin.lat+"\t"+chkin.lng+"\t"+chkin.loc_id+"\n");
			}
			bw_out_data.write("\n\n"+ dataset_info);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			//Close the BufferedReader, writer
			try {
				if (bw_out_data != null) {
					bw_out_data.flush();
					bw_out_data.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	
	}
	
	
	// write all check-ins in the data set to a script that plots them on google maps
	// the file name should have the extension ".html". p_plot_type takes one of values {"MARKERS","HEATMAP"}
	// to determines whether to plot the markers of points, or a heatmap. 
	//p_show_grid determines whether 
	// or not to show the grip with the default cell_side on the map.
	public void write_map_script(String p_script_file_name, String p_plot_type, boolean p_show_grid) {
		
		BufferedReader br_in_script = null;    // to read from input script_template
		BufferedWriter bw_out_script = null;     // to write to output locs (script) file

		String in_script_line;
		try {    
			br_in_script = new BufferedReader(new FileReader(GSettings.map_script_flex_grid_template));
			bw_out_script = new BufferedWriter(new FileWriter(p_script_file_name));

			// Here we copy the template_script to out_script until "// data" is found          
			in_script_line = br_in_script.readLine();
			while (!(in_script_line.startsWith("// to be replaced"))) {
				bw_out_script.write(in_script_line+"\n");
				in_script_line = br_in_script.readLine();
			}

			// Here we start writing the out_data
			bw_out_script.write("var rect_boundaries = {\n" +
					"      south: "+ south+ ",\n"+
					"      north: "+ north+ ",\n"+
					"      west : "+ west + ",\n"+
					"      east : "+ east + ",\n"+
					"    };\n" + 
					"//\n"+
					"// checkins\n" +
					"var locs = [\n");
			
			for (int i=0;i<checkins.size();i++)
				bw_out_script.write("            {lat:"+checkins.get(i).lat+" , lng:"+checkins.get(i).lng+"},\n");

			//  At this point data has been filtered, now write the variables. 
			bw_out_script.write("];\n"+
					"// var loc_count ="+ checkins.size() +"; \n"+
					"//\n"+
					"// grid settings: \n"+
					"var grid_required = "+ p_show_grid +"; \n"+
					"var plot_type = \""+ p_plot_type +"\"; \n"+
					"var d_u ="+ def_cell_side + "; \n");

			// Continue to copy from the template script to the out_script          
			in_script_line = br_in_script.readLine();
			while (in_script_line != null) {
				bw_out_script.write(in_script_line+"\n");
				in_script_line = br_in_script.readLine();
			}

		} catch (FileNotFoundException ex) {
			System.out.println("script template file is not found"); 
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			//Close the BufferedReader, writer
			try {
				if (bw_out_script != null) {
					bw_out_script.flush();
					bw_out_script.close();
				}
				if (br_in_script != null) {
					br_in_script.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}
	

	// construct and return a prior using the check-ins in the data set 
	// on the grid created by p_cell_side.  
	public Prior get_prior(double p_cell_side) {
		
		double scaling_ratio = def_cell_side/p_cell_side;
		if (scaling_ratio != (int) scaling_ratio) {
			System.out.println("unable to get a prior from dataset:");
			System.out.println("The default cell side must be multiples of the given cell side");
			System.exit(0);
		}

		// create a matrix for prior and the dimensions of every cell
		double prior_array[][] = new double[get_hsize(p_cell_side)][get_vsize(p_cell_side)];
		for (int i=0;i<checkins.size();i++) {
			//update the prior_matrix;
			int cell_x = get_cell_x_of_lng(Double.parseDouble(checkins.get(i).lng),p_cell_side);
			int cell_y = get_cell_y_of_lat(Double.parseDouble(checkins.get(i).lat),p_cell_side);
			prior_array[cell_x][cell_y]=prior_array[cell_x][cell_y]+1;
		}

		// average the prior_array entries, and create a prior using prior_array
		for (int i=0; i< get_hsize(p_cell_side); i++)
			for (int j=0; j< get_vsize(p_cell_side);j++)
				prior_array[i][j] = prior_array[i][j]/checkins.size();

		Prior prior = new Prior(prior_array, p_cell_side);
		prior.add_info(dataset_info);
		
		return prior;
	}
	
	// construct and return a prior using the check-ins in the data set and  
	// the default cell_side 
	public Prior get_prior() {
		return get_prior(def_cell_side);
	}
	
	
	// returns the global prior in a different way: constructs the prior of every 
	// user and then computes the global one by averaging users priors. 
	public Prior get_average_prior(double p_cell_side) {

		double scaling_ratio = def_cell_side/p_cell_side;
		if (scaling_ratio != (int) scaling_ratio) {
			System.out.println("unable to construct average prior from dataset:");
			System.out.println("The default cell side must be multiples of the given cell side");
			System.exit(0);
		}

			// create a matrix for the global prior and the dimensions of every cell
			double gprior_array[][] = new double[get_hsize(p_cell_side)][get_vsize(p_cell_side)];
			// create a matrix for the the current user prior
			double uprior_array[][] = new double[get_hsize(p_cell_side)][get_vsize(p_cell_side)];
			// summary data            
			int out_users_count=0;  // count the number of users in the output

			// handle initial user
			int k=0;
			int cell_x = get_cell_x_of_lng(Double.parseDouble(checkins.get(k).lng),p_cell_side);
			int cell_y = get_cell_y_of_lat(Double.parseDouble(checkins.get(k).lat),p_cell_side);
			String old_user_id = checkins.get(k).user_id;
			int old_user_checkins=1;
			// update the user's prior matrix 
			uprior_array[cell_x][cell_y]=uprior_array[cell_x][cell_y]+1;
			

			for (k=1;k<checkins.size();k++) {
				// determine the cell of the curr_coordinates.
				cell_x = get_cell_x_of_lng(Double.parseDouble(checkins.get(k).lng),p_cell_side);
				cell_y = get_cell_y_of_lat(Double.parseDouble(checkins.get(k).lat),p_cell_side);

				// check if a new user is the same as before
				if (checkins.get(k).user_id.equals(old_user_id)) {
					old_user_checkins++;
					uprior_array[cell_x][cell_y]=uprior_array[cell_x][cell_y]+1;
				}
				else {
					// process the info of the old user
						out_users_count++;
						// here we normalize the user's array and update the global prior matrix
						for (int i=0; i< get_hsize(p_cell_side); i++)
							for (int j=0; j< get_vsize(p_cell_side);j++) {
								uprior_array[i][j] = uprior_array[i][j]/old_user_checkins;
								gprior_array[i][j] = gprior_array[i][j] + uprior_array[i][j];
							}
						
					// now process new user
					// reset the uprior array
					for (int i=0; i< get_hsize(p_cell_side); i++)
						for (int j=0; j< get_vsize(p_cell_side);j++)
							uprior_array[i][j] = 0;
					old_user_id = checkins.get(k).user_id;
					old_user_checkins=1;
					// update uprior 
					uprior_array[cell_x][cell_y]=uprior_array[cell_x][cell_y]+1;
				}
			}
			// process the last user
				out_users_count++;
				//  here we normalize the user's array and update gprior array
				for (int i=0; i< get_hsize(p_cell_side); i++)
					for (int j=0; j< get_vsize(p_cell_side);j++) {
						uprior_array[i][j] = uprior_array[i][j]/old_user_checkins;
						gprior_array[i][j] = gprior_array[i][j] + uprior_array[i][j];
					}

			// average the global prior_matrix entries (over users), and create a prior using gprior_array
			for (int i=0; i< get_hsize(p_cell_side); i++)
				for (int j=0; j< get_vsize(p_cell_side);j++)
					gprior_array[i][j] = gprior_array[i][j]/out_users_count;
			Prior gprior = new Prior(gprior_array, p_cell_side);
			gprior.add_info(dataset_info);
			
			return gprior;


	}

	// construct and return the average prior using the check-ins in the data set and  
	// the default cell_side 
	public Prior get_average_prior() {
		return get_average_prior(def_cell_side);
	}
	
	
	// construct EmpData object, with the original empirical data based on the checkins 
	// (without obfuscation). 
	// each datum consists of a location and the frequency of observing this location. 
	// Obfuscation is performed using the given mechanism p_mech. The granularity of the 
	// resulting empirical data is determined by p_cell_side.  
	// (which must be multiples of def_cell_side).  
	public GridEmpData get_emp_data(double p_cell_side) {
		
		double scaling_ratio = def_cell_side/p_cell_side;  // allows finer grids but not coarser.
		if (scaling_ratio != (int) scaling_ratio) {
			System.out.println("unable to get a prior from dataset:");
			System.out.println("The default cell side must be multiples of the given cell side");
			System.exit(0);
		}
		
		

		int hsize = get_hsize(p_cell_side); 
		int vsize = get_vsize(p_cell_side);

		// create an array to record the frequency of every cell. Cells are ordered in the array
		// in the linear order. 
		int freq_array[] = new int[hsize*vsize];
		for (int i=0;i<checkins.size();i++) {
			//update freq_array;
			int cell_x = get_cell_x_of_lng(Double.parseDouble(checkins.get(i).lng),p_cell_side);
			int cell_y = get_cell_y_of_lat(Double.parseDouble(checkins.get(i).lat),p_cell_side);
			freq_array[cell_y * hsize + cell_x] = freq_array[cell_y * hsize + cell_x]+1;
		}

        // create the required empdata. In this creation process zero-frequency cells are removed. 
		GridEmpData empdata = new GridEmpData(hsize,vsize, freq_array);
		
		return empdata;
	}


	
	// construct a prior for every user in the data set and write it as a vector to 
	// the given file, i.e. in S format. 
	// parameters: 
	// p_cell_side define the grid for which priors are constructed . 
	// threshold: restricts the chosen users having at least this number of points. 
	public void write_priors_of_users(String p_file, double p_cell_side, int p_user_checkins_threshold) {

		BufferedWriter bw_priors =null;       // to write to priors file
		try {    
			bw_priors = new BufferedWriter(new FileWriter(p_file));

//			double gprior_array[][] = new double[get_hsize(p_cell_side)][get_vsize(p_cell_side)];
			double uprior_matrix[][] = new double[get_hsize(p_cell_side)][get_vsize(p_cell_side)];
			
			// summary data            
			int out_users_count=0;  // count the number of users in the output
			
			// process the first check-in
			int k=0;
			int cell_x = get_cell_x_of_lng(Double.parseDouble(checkins.get(k).lng),p_cell_side);
			int cell_y = get_cell_y_of_lat(Double.parseDouble(checkins.get(k).lat),p_cell_side);
//			gprior_array[cell_x][cell_y]=gprior_array[cell_x][cell_y]+1;
			String old_user_id = checkins.get(k).user_id;
			int old_user_checkins=1;  // counting the number of check-ins for the old user
			// add the new position 
			uprior_matrix[cell_x][cell_y]=uprior_matrix[cell_x][cell_y]+1;

			for (k=1;k<checkins.size();k++) {
				// determine the cell of the curr_coordinates.
				cell_x = get_cell_x_of_lng(Double.parseDouble(checkins.get(k).lng),p_cell_side);
				cell_y = get_cell_y_of_lat(Double.parseDouble(checkins.get(k).lat),p_cell_side);
//				gprior_array[cell_x][cell_y]=gprior_array[cell_x][cell_y]+1;

				// check if a new user is the same as before
				if (checkins.get(k).user_id.equals(old_user_id)) {
					old_user_checkins++;
					uprior_matrix[cell_x][cell_y]=uprior_matrix[cell_x][cell_y]+1;
				}
				else {
					// process the info of the old user
					if (old_user_checkins >= p_user_checkins_threshold) {
						out_users_count++;
						// here we normalize the user's matrix 
						for (int i=0; i< get_hsize(p_cell_side); i++)
							for (int j=0; j< get_vsize(p_cell_side);j++)
								uprior_matrix[i][j] = uprior_matrix[i][j]/old_user_checkins;
						// create a prior and write its data to the file	
						Prior uprior = new Prior(uprior_matrix, p_cell_side);
						bw_priors.write("// user "+ old_user_id +": no of checkins = "+old_user_checkins+ "\n");
						bw_priors.write(uprior.get_prior_as_string() + "\n");
					}
					// now we have a new user
					// reset the matrix
					for (int i=0; i< get_hsize(p_cell_side); i++)
						for (int j=0; j< get_vsize(p_cell_side);j++)
							uprior_matrix[i][j] = 0;
					old_user_id = checkins.get(k).user_id;
					old_user_checkins=1;
					// add the new position 
					uprior_matrix[cell_x][cell_y]=uprior_matrix[cell_x][cell_y]+1;
				}
			}
			// process the last user
			if (old_user_checkins >= p_user_checkins_threshold) {
				out_users_count++;
				//  here we normalize the user's matrix 
				for (int i=0; i< get_hsize(p_cell_side); i++)
					for (int j=0; j< get_vsize(p_cell_side);j++)
						uprior_matrix[i][j] = uprior_matrix[i][j]/old_user_checkins;
				// create a prior and write its data to the file	
				Prior uprior = new Prior(uprior_matrix, p_cell_side);
				bw_priors.write("// user "+ old_user_id +": no of checkins = "+old_user_checkins+ "\n");
				bw_priors.write(uprior.get_prior_as_string() + "\n");
				// reset the matrix
				for (int i=0; i< get_hsize(p_cell_side); i++)
					for (int j=0; j< get_vsize(p_cell_side);j++)
						uprior_matrix[i][j] = 0;
			}

			// average the global prior_matrix entries, and create a prior using prior_matrix
//			for (int i=0; i< get_hsize(p_cell_side); i++)
//				for (int j=0; j< get_vsize(p_cell_side);j++)
//					gprior_array[i][j] = gprior_array[i][j]/checkins.size();
//			Prior gprior = new Prior(gprior_array, p_cell_side);
//			bw_priors.write("// global prior: no of checkins = "+checkins.size()+ "\n");
//			bw_priors.write(gprior.get_prior_as_string() + "\n");

			// prepare the tail summary data to be written to out_data_file
			String info_str = dataset_info + "\n"+
			  "// Number of users (with at least "+p_user_checkins_threshold +" checkins) = " + out_users_count +"\n";

			bw_priors.write("\n\n"+info_str);


		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			//Close the BufferedReader, writer
			try {
				if (bw_priors != null) {
					bw_priors.flush();
					bw_priors.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	
	
	// write two priors for every user in p_ustat_list
	public void write_train_test_priors_of_users(List<UserStatistic> p_ustat_list, String p_train_file, String p_test_file,double p_cell_side, double p_train_percentage) {

		BufferedWriter bw_train_priors =null;       // training priors file
		BufferedWriter bw_test_priors =null;       // test priors file
		try {    
			bw_train_priors = new BufferedWriter(new FileWriter(p_train_file));
			bw_test_priors = new BufferedWriter(new FileWriter(p_test_file));

			// create a matrix for the the current user prior
			double train_uprior_array[][] = new double[get_hsize(p_cell_side)][get_vsize(p_cell_side)];
			double test_uprior_array[][] = new double[get_hsize(p_cell_side)][get_vsize(p_cell_side)];

			// summary data            
			//int out_users_count=0;  // count the number of users in the output

			String old_user_id ="*";  
			// holds the previous user in the output to be compared with the new (curr) user.
			int old_user_train_checkins =0; 
			int old_user_test_checkins =0; 
			// check if the user is in the list
			boolean old_user_in_list = false;
			int old_user_index_in_list =0;
			while (old_user_index_in_list< p_ustat_list.size() && 
					!p_ustat_list.get(old_user_index_in_list).user_id.equals(old_user_id)) 
				old_user_index_in_list++;
			if (old_user_index_in_list< p_ustat_list.size()) 
				old_user_in_list = true;
			
			
			// counting the number of check-ins for the old user. 

			for (int k=0;k<checkins.size();k++) {
				// determine the cell of the curr_coordinates.
				int cell_x = get_cell_x_of_lng(Double.parseDouble(checkins.get(k).lng),p_cell_side);
				int cell_y = get_cell_y_of_lat(Double.parseDouble(checkins.get(k).lat),p_cell_side);

				// check if a new user is the same as before
				if (checkins.get(k).user_id.equals(old_user_id)) {
					//update the user's prior_matrix;
					if (old_user_in_list) {
						if (old_user_train_checkins+old_user_test_checkins+1 < p_train_percentage*p_ustat_list.get(old_user_index_in_list).count) {
							train_uprior_array[cell_x][cell_y]=train_uprior_array[cell_x][cell_y]+1;
							old_user_train_checkins++;
						}
						else {
							test_uprior_array[cell_x][cell_y]=test_uprior_array[cell_x][cell_y]+1;
							old_user_test_checkins++;
						}
					}
				}
				else {
					// process the info of the old user
					if (old_user_in_list) {
						// here we normalize the user's matrices 
						for (int i=0; i< get_hsize(p_cell_side); i++)
							for (int j=0; j< get_vsize(p_cell_side);j++)
								train_uprior_array[i][j] = train_uprior_array[i][j]/old_user_train_checkins;
						for (int i=0; i< get_hsize(p_cell_side); i++)
							for (int j=0; j< get_vsize(p_cell_side);j++)
								test_uprior_array[i][j] = test_uprior_array[i][j]/old_user_test_checkins;
						
						// create priors and write its data to the file	
						Prior train_uprior = new Prior(train_uprior_array, p_cell_side);
						Prior test_uprior = new Prior(test_uprior_array, p_cell_side);
						bw_train_priors.write("// user "+ old_user_id +": no of checkins = "+old_user_train_checkins+ "\n");
						bw_train_priors.write(train_uprior.get_prior_as_string() + "\n");
						bw_test_priors.write("// user "+ old_user_id +": no of checkins = "+old_user_test_checkins+ "\n");
						bw_test_priors.write(test_uprior.get_prior_as_string() + "\n");
					}
					// now we have a new user
					// reset the matrix
					for (int i=0; i< get_hsize(p_cell_side); i++)
						for (int j=0; j< get_vsize(p_cell_side);j++) {
							train_uprior_array[i][j] = 0;
							test_uprior_array[i][j] = 0;
						}
					old_user_id = checkins.get(k).user_id;
					
					// initialize data for the new user
					old_user_train_checkins =0; 
					old_user_test_checkins =0; 
					// check if the user is in the list
					old_user_in_list = false;
					old_user_index_in_list =0;
					while (old_user_index_in_list< p_ustat_list.size() && 
							!p_ustat_list.get(old_user_index_in_list).user_id.equals(old_user_id) ) 
						old_user_index_in_list++;
					if (old_user_index_in_list< p_ustat_list.size()) 
						old_user_in_list = true;

					old_user_train_checkins=1;
					if (old_user_in_list)
						train_uprior_array[cell_x][cell_y]=train_uprior_array[cell_x][cell_y]+1;
				}
			}
			// to display the information of the last user
			// process the info of the old user
			if (old_user_in_list) {
				// here we normalize the user's matrices 
				for (int i=0; i< get_hsize(p_cell_side); i++)
					for (int j=0; j< get_vsize(p_cell_side);j++)
						train_uprior_array[i][j] = train_uprior_array[i][j]/old_user_train_checkins;
				for (int i=0; i< get_hsize(p_cell_side); i++)
					for (int j=0; j< get_vsize(p_cell_side);j++)
						test_uprior_array[i][j] = test_uprior_array[i][j]/old_user_test_checkins;
				
				// create priors and write its data to the file	
				Prior train_uprior = new Prior(train_uprior_array, p_cell_side);
				Prior test_uprior = new Prior(test_uprior_array, p_cell_side);
				bw_train_priors.write("// user "+ old_user_id +": no of checkins = "+old_user_train_checkins+ "\n");
				bw_train_priors.write(train_uprior.get_prior_as_string() + "\n");
				bw_test_priors.write("// user "+ old_user_id +": no of checkins = "+old_user_test_checkins+ "\n");
				bw_test_priors.write(test_uprior.get_prior_as_string() + "\n");
			}
			// now we have a new user
			// reset the matrix
			for (int i=0; i< get_hsize(p_cell_side); i++)
				for (int j=0; j< get_vsize(p_cell_side);j++) {
					train_uprior_array[i][j] = 0;
					test_uprior_array[i][j] = 0;
				}

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			//Close the BufferedReader, writer
			try {
				if (bw_train_priors != null) {
					bw_train_priors.flush();
					bw_train_priors.close();
				}
				if (bw_test_priors != null) {
					bw_test_priors.flush();
					bw_test_priors.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}
	
	
	// The following function adjusts the boundaries to be in form of grid
	// where the side of every cell is def_cell_side
	private void adjust_boundaries() {
		double d_ns = Distance.get_distance_on_earth(north, east, south, east);
		double d_ew = Distance.get_distance_on_earth(north, east, north, west);
		int vsize = (int) Math.round(d_ns/def_cell_side);
		int hsize = (int) Math.round(d_ew/def_cell_side);
		north = south+ (north-south)*(def_cell_side/d_ns)*vsize;
		east = west+ (east-west)*(def_cell_side/d_ew)*hsize;
	}
	
	private int get_hsize(double p_cell_side) {
		double d_ew = Distance.get_distance_on_earth(north, east, north, west);
		return (int) Math.round(d_ew/p_cell_side);
	}
	private int get_vsize(double p_cell_side) {
		double d_ns = Distance.get_distance_on_earth(north, east, south, east);
		return (int) Math.round(d_ns/p_cell_side);
	}
	
	
	public int get_number_of_distinct_checkins() {
		List<String> points = new ArrayList<String>();
		for (int k=0;k<checkins.size();k++) 
			if (!points.contains(checkins.get(k).lat+" "+checkins.get(k).lng))
				points.add(checkins.get(k).lat+" "+checkins.get(k).lng);
		return points.size();
	}

	
	
	// returns the horizontal index (int) of the cell (with length p_cell_side)
	// containing the given point assuming that cell(0,0) is the most south-west cell 
	private int get_cell_x_of_lng(double p_lng, double p_cell_side) {
		int hsize = get_hsize(p_cell_side);
		double h_spacing = (east - west)/hsize;
		return (int)((p_lng- west)/h_spacing);
	}

	// returns the vertical index (int) of the cell (with length p_cell_side)
	// containing the given point assuming that cell(0,0) is the most south-west cell 
	private int get_cell_y_of_lat(double p_lat, double p_cell_side) {
		int vsize = get_vsize(p_cell_side);
		double v_spacing = (north - south)/vsize;
		return (int)((p_lat- south)/v_spacing);
	}
	


}

