package priv.plan.gdset;

public class GSettings {
	
    public static String data_file = "./gdata/Gowalla_totalCheckins.txt"; // data source file
//    public static String data_file = "./gdata/Brightkite_totalCheckins.txt"; // data source file
    public static String train_data_file = "./gdata/gowalla-train.txt"; // train_data source file
    public static String test_data_file = "./gdata/gowalla-test.txt"; // test_data source file
    public static int default_read_size = 7000000;  // default number of records to be read from a dataset 
    
    //public static boolean grid_required = true;   // set if the grid created for the region 
                                           // is required to be displayed 


    // The following template is used when a flexible grid is created according to a 
    // specified cell size (cell_side) in the Settings.  
    static String map_script_flex_grid_template = "./jscripts/map_script_flex_grid_template.html";

    // used when a grid of fixed grid size (no. of rows & no. of columns) is created. 
    // In this case the cell size depends on the grid size. 
    static String map_script_fix_grid_template = "./jscripts/fixed_grid/map_script_fixed_grid_template.html";
    
}
