package priv.plan.gdset;

public class GCheckin {
	
	public String user_id, datetime, lat, lng, loc_id;
	
	public GCheckin(String p_user_id, String p_datetime, String p_lat, String p_lng, String p_loc_id) {
		user_id = p_user_id; 
		datetime = p_datetime;
		lat = p_lat;
		lng = p_lng;
		loc_id = p_loc_id;
	}

}
