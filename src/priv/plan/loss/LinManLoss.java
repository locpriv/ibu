package priv.plan.loss;

public class LinManLoss extends Loss{

	public LinManLoss(double p_saturation_dist, double p_max_loss) {
		super(p_saturation_dist,p_max_loss);
	}

	public double get_loss(double i_x, double i_y, double j_x, double j_y) {
	          return get_loss_by_dist(Math.abs(i_x-j_x)+ Math.abs(i_y-j_y));   
		}

	public double get_loss_by_dist(double p_man_dist) {
		if (p_man_dist<=saturation_dist)
			return p_man_dist;
		else
			return max_loss;
	    }
	
	
	public String get_name() {
		return "LIN_MAN";
	}

    
}

