package priv.plan.loss;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.Settings;

/** This class specifies the necessary function (get_loss) that computes 
    the loss based on the coordinates of given cells. Every loss function
    is assumed to be increasing. This function behaves as follows. 
    loss = get_loss_by_dist(distance) For all 0<=distance<= saturation_dist, 
    loss = max_loss for all saturation_dist < dist.
*/
public abstract class Loss {
	
	protected double saturation_dist;  // default max distance which get_loss() respects.
	protected double max_loss;    // loss for distances larger than saturation_dist
	
	public Loss(double p_saturation_dist, double p_max_loss) {
        // the max_loss must be at least equal to get_loss(saturation_dist)
		if (p_max_loss < get_loss_by_dist(p_saturation_dist)) {
			System.out.println("unable to create loss function: specified max loss is too low");
			System.exit(0);
		}
		saturation_dist = p_saturation_dist;
		max_loss = p_max_loss;
	}
	
	/* Loss for two points i,j: 
		 * gets the loss for input i=(i_x, i_y) and output j=(j_x,j_y) */
		public abstract double get_loss(double i_x, double i_y, double j_x, double j_y); 
	        
	    
		/* Loss for a distance:
		 * This function takes the distance p_dist between two points 
		 * and return the loss due to this distance. 
		 * The semantics of the distance depends on the type of the loss */
		public abstract double get_loss_by_dist(double p_dist); 
		
		// get a descriptive name for the loss function
		public abstract String get_name();
		
		public double get_saturation_dist() {
			return saturation_dist;
		}
		public double get_max_loss() {
			return max_loss;
		}
	 	
		/**
		 for a grid with dimensions hsize,vsize, cell_side returns a matrix 
		 representing the loss for every pair of input and output cells.
		 */
		public DoubleMatrix get_loss_matrix(int p_hsize, int p_vsize, double p_cell_side) {
			DoubleMatrix loss_matrix = new DoubleMatrix(p_hsize*p_vsize, p_hsize*p_vsize);
			for (int in_y=0; in_y<p_vsize; in_y++)
				for (int in_x=0; in_x<p_hsize;in_x++)
					for (int out_y=0; out_y<p_vsize;out_y++)
						for (int out_x=0; out_x<p_hsize;out_x++)
							loss_matrix.put(in_y*p_hsize+in_x, out_y*p_hsize+out_x,
									get_loss(in_x*p_cell_side, in_y*p_cell_side, out_x*p_cell_side, out_y*p_cell_side));

			return loss_matrix;
		}
		
		/**
		 for a grid with dimensions hsize,vsize, cell_side returns a matrix in double array format  
		 representing the loss for every pair of input and output cells.
		 */
		public double[][] get_loss_DoubleArray(int p_hsize, int p_vsize, double p_cell_side) {
			double[][] lossDArray = new double[p_hsize*p_vsize][p_hsize*p_vsize];
			for (int in_y=0; in_y<p_vsize; in_y++)
				for (int in_x=0; in_x<p_hsize;in_x++)
					for (int out_y=0; out_y<p_vsize;out_y++)
						for (int out_x=0; out_x<p_hsize;out_x++)
							lossDArray[in_y*p_hsize+in_x][out_y*p_hsize+out_x] =
									get_loss(in_x*p_cell_side, in_y*p_cell_side, out_x*p_cell_side, out_y*p_cell_side);

			return lossDArray;
		}

		public void write_kostas_loss_to_file1(String p_mech_file, int p_hsize, int p_vsize, double p_cell_side) {
	 		BufferedWriter bufferedWriter = null;
	        try {    
	        	bufferedWriter = new BufferedWriter(new FileWriter(p_mech_file));
	        	// write stuff
		        int data_spacing = Settings.prob_spacing;  // spaces between probabilities;
		 		for (int in_y=0; in_y<p_vsize;in_y++)
		    		for (int in_x=0; in_x<p_hsize;in_x++) {  // for every input location
//		    			in_loc=String.format("%0"+x_digits+"d", in_x)+
//		    					String.format("%0"+y_digits+"d", in_y);
//		    			outputPosLabels = outputPosLabels + in_loc;
//		    			for (int k=0;k<label_spacing;k++) outputPosLabels=outputPosLabels+" ";
//		        		for (int k=0;k<data_spacing;k++) matrixStr = matrixStr +" ";
		               	for (int out_y=0;out_y<p_vsize;out_y++) 
		               		for (int out_x=0;out_x<p_hsize;out_x++) {// for every output location
		                    	 
		                    	   bufferedWriter.write(String.format("%06."+Settings.prop_decimal_places+"f", 
		                    			      get_loss(in_x*p_cell_side,in_y*p_cell_side,out_x*p_cell_side,out_y*p_cell_side)));
		                    	for (int k=0;k<data_spacing;k++) bufferedWriter.write(" ");
		               		}
		               	bufferedWriter.write("\n"); // move to a new row
		    		}
	        	// end of writing
	        } catch (IOException ex) {
	            ex.printStackTrace();
	        } finally {
	            //Close the BufferedReader
	        	try {
	        		if (bufferedWriter != null) {
	        			bufferedWriter.flush();
	        			bufferedWriter.close();
	        		}
	        	} catch (IOException ex) {
	        		ex.printStackTrace();
	        	}
	        }
	 	}
	 	
	 	
}

