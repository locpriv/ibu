package priv.plan.loss;


// loss is 0 within distance saturation_dist, and is max_loss for larger distances
// note: in the special case if saturation_dist=0, the loss is 0 only when dist=0, 
// and max_loss otherwise. 
public class BinLoss extends Loss{
	
	public BinLoss(double p_saturation_dist, double p_max_loss) {
		super(p_saturation_dist, p_max_loss);    
		// the loss is 0 for distance<=saturation_dist, and is max_loss otherwise.
	}
	
	public double get_loss(double i_x, double i_y, double j_x, double j_y) {
		return get_loss_by_dist(Math.sqrt(Math.pow(i_x-j_x, 2)+Math.pow(i_y-j_y, 2)));
	}

	public double get_loss_by_dist(double p_dist) {
		if (p_dist<=saturation_dist)
			return 0;
		else
			return max_loss;
	}
	
	public String get_name() {
		return "BIN";
	}

}

