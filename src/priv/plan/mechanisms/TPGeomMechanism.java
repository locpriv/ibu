package priv.plan.mechanisms;

import priv.plan.loss.Loss;

// Truncated planar geometric mechanism on a rectangular grid.
// The mechanism matrix has the form 
// (double) mechanism[i_x][i_y][j_x][j_y] where the first 
// coordinates represent the row (mechanism input) and the other two 
// coordinates represent the column (mechanism output). 

// Here we optimize the construction and sampling memory by using the symmetries. 
// Basically for every interior output (out_x, out_y), we do not need to store the 
// conditional probability of producing (out_x,out_y) from a given input (in_x, in_y) 
// because this probability is obtained easily as G.e^-eps*d(in,out). However, due to 
// the truncation, we need to store for every border output (out_x,0), and every input 
// (in_x, in_y) the probability of producing (out_x,0) given input (in_x, in_y). We 
// also need to do the same for vertical border outputs (0, out_y). 


public class TPGeomMechanism extends Mechanism {
	
      public double eps;     // epsilon
      private double[][][] hborder_prob;   // probabilities for outputs on the horizontal border  
      private double[][][] vborder_prob;   // probabilities for outputs on the vertical border
      // hborder_prob[in_x][in_y][out_x] is the probability of producing the output (out_x,0)
      // from the input (in_x, in_y). 

      private double g;       // The normalization constant, also is probability that out = in
  	  final int infinity = 200; // maximum index used in numerical summations.
   
    
    // this constructor creates the TPGeom mechanism
    // specified by the hsize of the squared grid, the cell_side (in km), and privacy parameter
  	// note that TGeomMechanism is only used to satisfy eps-geo-indistinguishability. 
  	// Therefore p_eps is the privacy per 1km
  	// This is different from e.g. TCRectMechanism which is generic with respect to privacy 
  	// and therefore takes privMetric parameter. 
    public TPGeomMechanism(int p_hsize, int p_vsize, double p_cell_side, double p_eps) {
    	super(p_hsize,p_vsize,p_cell_side);
        eps = p_eps;
    	compute_g();
    	fill_border_probs();
    	String info = "// Truncated Planar Geometric mechanism  \n"
                + "// epsilon =" + eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
    	this.add_info(info);
    }
    
 	
    /** 
     * Constructs a mechanism that satisfies a given loss value for the uniform prior 
     * (with respect to the given loss object). This method is used to compare mechanisms with respect 
     * to statistical utility. 
     * @param p_hsize
     * @param p_vsize
     * @param p_cell_side
     * @param p_loss
     * @param p_exp_loss
     */
    public TPGeomMechanism(int p_hsize, int p_vsize, double p_cell_side, Loss p_loss, double p_exp_loss) {
 		super(p_hsize,p_vsize,p_cell_side);
  		double eps0 = 0.1; double eps1 = 20;    // initial bounds for eps
 		double u;
 		do {
 			eps = (eps0+eps1)/2;
 	    	compute_g();
 	    	fill_border_probs();
 	    	// compute utility 
 			u = this.get_mech_matrix().mul(p_loss.get_loss_matrix(hsize, vsize, cell_side)).sum()/(hsize*vsize);
 			//System.out.println("Geometric: "+ "eps = "+ eps+ ", u = " + u);
 			if (p_exp_loss<u) eps0=eps; else eps1= eps;
 		}
 		while (Math.abs(p_exp_loss - u)>0.001);

 		
 		String info = "// Truncated Planar Geometric mechanism  \n"
                + "// epsilon =" + eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
    	this.add_info(info);
    }	
 		

    private void compute_g() {
    	// Compute the normalization constant G = sum_{(i,j) in Z^2}  e^{- d sqrt(i^2+j^2)}
    	// sum terms: 
    	// i=0, j=0  --> G0 = 1;    // 1 time
    	// i>0, j=0  --> G1 = sum_{i>0} e^{-eps*d*i} = e^-eps*d / (1 - e^-eps*d) // 4 times
    	// i=j>0     --> G2 = sum_{i>0} e^{-eps*d*sqrt(2)*i} = e^-eps*d*sqrt(2)/ ( 1- e^-eps*d*sqrt(2)) // 4 times 
    	// i>0,i>j>0 --> G3 = sum_{i>0,i>j>0} e^{-eps*d*sqrt(i^2+j^2)}  // 8 times
    	g = 1;
    	g = g + 4 *(double) Math.exp(- eps*cell_side)/(1-Math.exp(- eps*cell_side));
    	g = g + 4 *(double) Math.exp(- eps*cell_side*Math.sqrt(2.0))/(1-Math.exp(- eps*cell_side*Math.sqrt(2.0)));
    	
 		boolean g_min_precision_reached = false; 
 		double prob;
    	
    	int i=2; int j=1;
    	while (!g_min_precision_reached) {
    		prob = 8* Math.exp(- eps*cell_side*Math.sqrt(i*i+j*j));
        	if (prob<Math.pow(0.1,Settings.prop_decimal_places))
    			g_min_precision_reached = true;
    		g = g + prob;
    		j++; 
    		if (j==i) {
    			i++;j=1;
    		}
    	}
    	g = (double) 1/g;
    }
    
    // fill the mechanism matrix elements
    private void fill_border_probs() {
    	hborder_prob=new double[hsize][vsize][hsize];	
    	vborder_prob=new double[hsize][vsize][vsize];	
    	
    	// compute the entries of the hborder_prob
    	for (int in_x=0; in_x<hsize;in_x++)
    		for (int in_y=0; in_y<vsize;in_y++) 
               	for (int out_x=0;out_x<hsize;out_x++)
               			hborder_prob[in_x][in_y][out_x] = get_tgeom_prob(in_x,in_y,out_x,0);

    	// compute the entries of the vborder_prob
    	for (int in_x=0; in_x<hsize;in_x++)
    		for (int in_y=0; in_y<vsize;in_y++) 
               	for (int out_y=0;out_y<vsize;out_y++)
               			vborder_prob[in_x][in_y][out_y] = get_tgeom_prob(in_x,in_y,0,out_y);
    
    }
    
 	// returns the mechanism probability  
 	// for input x,y and output x,y
 	private double get_tgeom_prob(int p_in_x,int p_in_y,int p_out_x,int p_out_y) {
 		
 		int out_x_start= p_out_x; // starting x for summation.
 		int out_x_end =  p_out_x; // end x for summation.
 		int out_y_start= p_out_y; // starting y for summation.
 		int out_y_end =  p_out_y; // ending y for summation.
 		
 		int xstep=1, ystep=1;  // specifies whether the summations goes forward or backward.
 		
 		// adjust the boundaries of the sum if out is on the borders
 		if (p_out_x==0) {
 			out_x_end = out_x_end-infinity; 
 			xstep=-1;
 			}; 
 		if (p_out_x==hsize-1) {
 			out_x_end = out_x_end+infinity; 
 			xstep =1; 
 		}
 		if (p_out_y==0) {
 			out_y_end = out_y_end-infinity; 
 			ystep = -1;
 		}
 		if (p_out_y==vsize-1) {
 			out_y_end =out_y_end+infinity; 
 			ystep = 1;
 		}

        // now make the summation to compute the required probability
 		double dist; // distance between input and output positions
 		double sum=0;

 		int out_x_curr, out_y_curr;
 		double xprob; 
 		double yprob;
 		boolean x_min_precision_reached = false; 
 		boolean y_min_precision_reached = false; 
 		
 		out_x_curr=out_x_start;  
 		while ( ((xstep>0 && out_x_curr <= out_x_end) || (xstep< 0 && out_x_curr >= out_x_end)) 
 				&& !x_min_precision_reached) {
        	xprob=0;
        	out_y_curr=out_y_start;
        	y_min_precision_reached = false;
        	while (((ystep>0 && out_y_curr <= out_y_end) || (ystep< 0 && out_y_curr >= out_y_end))
        			&& !y_min_precision_reached) {
    		    dist=Math.sqrt(Math.pow(out_x_curr- p_in_x, 2)+Math.pow(out_y_curr-p_in_y, 2));
        		yprob = g*Math.exp(-eps*cell_side*dist);
    		    xprob = xprob + yprob;
        		out_y_curr=out_y_curr + ystep;
            	if (yprob<Math.pow(0.1,Settings.prop_decimal_places))
            			y_min_precision_reached = true;
        	} 
        	out_x_curr = out_x_curr+xstep;
        	sum = sum + xprob;
        	if (xprob<Math.pow(0.1,Settings.prop_decimal_places))
        			x_min_precision_reached = true;
        } 
 		
 		return sum;
 	}
 	
 	public double getProb(int p_in_x, int p_in_y, int p_out_x, int p_out_y) {
 			
 		double ret=0;
 		// check if the output is on the horizontal border, i.e. with p_out_y=0 or p_out_y=vsize-1 
 		if (p_out_y==0)
 			ret=hborder_prob[p_in_x][p_in_y][p_out_x];
 		else if (p_out_y==vsize-1)
 			ret=hborder_prob[p_in_x][vsize-1-p_in_y][p_out_x];
 		// check if the output is on the vertical border, i.e. with p_out_x=0 or p_out_x=hsize-1 
 		else if (p_out_x==0)
 			ret=vborder_prob[p_in_x][p_in_y][p_out_y];
 		else if (p_out_x==hsize-1)
 			ret=vborder_prob[hsize-1-p_in_x][p_in_y][p_out_y];
 		// otherwise the output is in the interior
 		else
 			ret=this.get_tgeom_prob(p_in_x, p_in_y, p_out_x, p_out_y);
 		
 		return ret;
 	}
 

}
