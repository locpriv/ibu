package priv.plan.mechanisms;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.jblas.DoubleMatrix;

// A mechanism (on a grid) read from a given annotated mechanism file
// Requires mechanism matrix to store the probabilities 
// (double) mechanism(in,out) where in/out is index of the mechanism input/output in S order.  


public class AnnotatedFileMechanism extends Mechanism {
    
	private DoubleMatrix mechanism_matrix; // mechanism_matrix 

    // constructs a mechanism from a given file. Any line starting with // is skipped.
    public AnnotatedFileMechanism(String p_mech_input_file) {
    	super(0,0,0);
    	BufferedReader bufferedReader = null;
    	
    	String line;      // to hold a line string from the input file
        try {    
        	bufferedReader = new BufferedReader(new FileReader(p_mech_input_file));
        	
        	// skip any comments and empty lines
            do {line=bufferedReader.readLine();}  
            while (line.startsWith("//")||line.equals(""));
            
            // added 2-7-2016: now we expect to have the line with cell_side
        	if (line.matches("cell_side_in_km=.*"))
    			cell_side=Double.parseDouble(line.substring(16));  // read the cell side length
        	else {
        		System.out.println("error: cell side length is not specified");
        		System.exit(0);
        	}
        	// again skip any comments and empty lines
            do {line=bufferedReader.readLine();}  
            while (line.startsWith("//")||line.equals(""));
            /////////////////////////////////////////////////////////////////
                   
            // now we have the column labels of the mechanism from which we 
        	//  identify h_size and v_size, and therefore the size of the mechanism
        	int pos_in_line=0; hsize=0; vsize=0;
        	String s; int x,y;
        	int x_digits = Settings.get_x_digits();
        	int y_digits = Settings.get_y_digits();
        	String label_pattern ="";
        	for (int k=0;k<(x_digits+y_digits); k++) label_pattern = label_pattern + "\\d";
       	
        	while (pos_in_line<=line.length()-x_digits-y_digits) {
        		s=line.substring(pos_in_line, pos_in_line+x_digits+y_digits);
        		if (s.matches(label_pattern)) { // a column label is found
        			x= Integer.parseInt(s.substring(0, x_digits));
        			y= Integer.parseInt(s.substring(x_digits, x_digits+y_digits));
        			if (x+1>hsize) hsize= x+1;
        			if (y+1>vsize) vsize= y+1;
        			}
        		pos_in_line= pos_in_line+1;  // advance the line cursor
        		}
        	
        	// now read the mechanism probabilities line by line and fill the mechanism matrix
            mechanism_matrix = new DoubleMatrix(hsize*vsize,hsize*vsize);
        	int row_count=0;                //will count number of rows for validation 
            
            int curr_in_y=0; int curr_in_x=0;   // the current line corresponds 
                                                      // to an input location with x,y
            int curr_out_y; int curr_out_x;    
            String prop_str;
            boolean prob_found = false;
            while ((line = bufferedReader.readLine()) != null && !line.startsWith("//")) {
            	pos_in_line=0;
            	curr_out_y=0; curr_out_x=0;            
            	prob_found = false;
            	while (pos_in_line<line.length()-1) {
            		s=line.substring(pos_in_line, pos_in_line+2);
            		if (s.matches("[0|1]\\.")) {
            			prob_found = true;
            			prop_str = line.substring(pos_in_line, 
            					                  pos_in_line+2+Settings.prop_decimal_places);
            			mechanism_matrix.put(curr_in_y*hsize+curr_in_x,
            					curr_out_y*hsize+curr_out_x,Double.parseDouble(prop_str));
           			    if (curr_out_x< hsize-1) curr_out_x++;
           			    else { 
           			    	curr_out_x =0;
           			    	curr_out_y++;
           			    }
            		}
            		pos_in_line++;
            	}                      // here we reach end of the line
            	if (prob_found) { 
	                   // A complete row of probabilities has been finished
         		   // prob_found indicates that this row has at least one probability
         		if (curr_out_y!=vsize) {      // validate the obtained row
         			System.out.println("invalid mechanism:too few/many probabilities in one row");
         			System.exit(0);
         		}
         		row_count++;  // a valid row has been found: get the next input x,y
         		if (curr_in_x<hsize-1) curr_in_x++;
         		else {
         			curr_in_x=0;
         			curr_in_y++; 
         			}
         	}
         }
         if (row_count != hsize*vsize) {
 		    System.out.println("invalid mechanism:too few/many probability rows");
 		    System.exit(0);
         }
       } catch (FileNotFoundException ex) {
    	    System.out.println("mechanism input file is not found");
            ex.printStackTrace();
            System.exit(0);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedWriter
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    // returns the mechanism matrix: override the super class method to improve performance 
    public DoubleMatrix get_mech_matrix() {
 		return mechanism_matrix;
    }

    // returns a mechanism matrix row probabilities: override the super class method to improve performance
    // used For EM. p_in is the out location in linear order, i.e. out_y*hsize +out_x. 
   public double[] get_mech_row_probabilities(int p_in) {
	   return mechanism_matrix.getRow(p_in).toArray();
   }

    // returns the mechanism matrix column: override the super class method to improve performance
    // p_out is the out location in linear order, i.e. out_y*hsize +out_x. 
    public DoubleMatrix get_mech_column(int p_out) {
    	return mechanism_matrix.getColumn(p_out);
    }

    
 	// returns the entry of the mechanism 
 	// for input in_x,in_y and output out_x,out_y
 	public double getProb(int p_in_x,int p_in_y,int p_out_x,int p_out_y) {
 		return mechanism_matrix.get(p_in_y*hsize+p_in_x, p_out_y*hsize+p_out_x);
 		}

}
