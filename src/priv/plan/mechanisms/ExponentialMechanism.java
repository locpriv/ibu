package priv.plan.mechanisms;

import org.jblas.DoubleMatrix;

import priv.plan.privMetrics.PrivMetric;

public class ExponentialMechanism extends Mechanism {
	
	private PrivMetric privObj;
    private DoubleMatrix mechanism_constants;  
    // stores the normalization constants for input location (x,y) the mechanism in S order y*hsize+x. 

    // Note that this mechanism is generic with respect to privacy metric
    // Therefore it accepts a privMetric parameter
    public ExponentialMechanism(int p_hsize, int p_vsize, double p_cell_side, PrivMetric p_privObj) {
    	super(p_hsize,p_vsize,p_cell_side);
    	
    	privObj = p_privObj;
        construct_norm_constants();
    	
    	String info = "// Exponential mechanism  \n"
    			+ "// Privacy type: "+ privObj.get_name() + "\n"
                + "// epsilon =" + privObj.eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
    	this.add_info(info);
    }
    
    // set the normalization constants
    private void construct_norm_constants() {
    	mechanism_constants = new DoubleMatrix(hsize*vsize,1);
    	for (int in_y=0; in_y<vsize;in_y++)
    		for (int in_x=0; in_x<hsize;in_x++)  {
    			double sum =0;
    	    	for (int out_y=0; out_y<vsize;out_y++)
    	    		for (int out_x=0; out_x<hsize;out_x++)  {
    	        		sum = sum + Math.exp(-0.5*privObj.get_priv_distance(
    	        				in_x*cell_side, in_y*cell_side,
    	                        out_x*cell_side, out_y*cell_side));
    	    		}
    	    	mechanism_constants.put(in_y*hsize+in_x, (double)1.0/sum);
    			}
    }
    
 	
 	public double getProb(int p_in_x, int p_in_y, int p_out_x, int p_out_y) {
 		return mechanism_constants.get(p_in_y*hsize+p_in_x)*
 				Math.exp(-0.5*privObj.get_priv_distance(
				p_in_x*cell_side, p_in_y*cell_side,
                p_out_x*cell_side, p_out_y*cell_side));
 	}

}
