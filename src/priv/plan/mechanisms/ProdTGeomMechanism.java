package priv.plan.mechanisms;

// A mechanism is defined by two independent Truncated linear geometric mechanisms
// randomizing on x and y dimensions respectively: 
// P(out given in) = P(out_x given in_x) P(out_y given in_y)
// This mechanism satisfies Manhattan privacy

// This mechanism is also the planar tight-constraints mechanism with respect to Manhattan privacy
// This mechanism is also the  truncated planar geometric mechanism with respect to Manhattan privacy.

public class ProdTGeomMechanism extends Mechanism {
	
      private double eps;                    // used to construct TGeometric matrix 
      
    // this constructor creates the TGeom mechanism 
    // specified by the size of the grid, the cell_side (in km), the type, and privacy parameter
    public ProdTGeomMechanism(int p_hsize, int p_vsize, double p_cell_side, double p_eps) {
    	super(p_hsize,p_vsize,p_cell_side);	
        eps = p_eps;
    	String info = "// Product Truncated Geormetric mechanism  \n"
                + "// epsilon =" + eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
        add_info(info);
    }
    
 	// returns the entry of the truncated geometric mechanism 
 	// for input x,y and output x,y
 	public double getProb(int p_in_x,int p_in_y,int p_out_x,int p_out_y) {
 		double alpha = Math.exp(-eps*cell_side);
 		double prob_x, prob_y;
 		if (p_out_x==0 || p_out_x==hsize-1) 
 			prob_x= (double)1/(1+alpha);
 		else prob_x = (double) (1-alpha)/(1+alpha);
 		prob_x = prob_x * Math.pow(alpha, Math.abs(p_out_x-p_in_x));

 		if (p_out_y==0 || p_out_y==vsize-1) 
 			prob_y= (double)1/(1+alpha);
 		else prob_y = (double) (1-alpha)/(1+alpha);
 		prob_y = prob_y * Math.pow(alpha, Math.abs(p_out_y-p_in_y));
 		
 		return (prob_x*prob_y);
 	}
 	
}
