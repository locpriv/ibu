package priv.plan.mechanisms;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.jblas.DoubleMatrix;

import priv.plan.loss.Loss;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.PrivMetric;



// A mechanism on a grid of hsize*vsize. 
// can be constructed from a matrix or a file, or using particular privacy parameters 
// of particular mechanisms. Mechanisms that are defined on a finite discrete space 
// of secrets, e.g. TCMechanism, ExponentialMechanism, and RRespMechanism may be constructed 
// using any privacy metric in priv.plan.privMetrics. However other mechanisms, e.g. 
// TPLapMechanism and TPGeomMechanism are defined only on Euclidean metric space, i.e. 
// defined only for geo-indistinguishability; therefore they are defined by the privacy 
// parameter \eps which describes the privacy for 1km. 
// 
// The rows (inputs) and columns (outputs) of the mechanism matrix are ordered in S 

public abstract class Mechanism {
	
	// mechanism types
	public static final String LAP = "LAP";
	public static final String GEOM = "GEOM";
	public static final String EXP = "EXP";
	public static final String KRR = "KRR";
	public static final String BOTRAPPOR = "BOTRAPPOR";

    // basic mechanism attributes
    public int hsize,vsize;      // The grid for which the mechanism is designed
    public double cell_side=0;     // The length of the side of one square cell in the grid. 
    private String mech_info="";

    
    /*    
    // copy constructor: it create an object that is a copy of the given one;
    // called from the constructor of a subclass which adds more attributes/methods
    public Mechanism(Mechanism p_m) {
    	hsize = p_m.hsize; vsize=p_m.vsize; cell_side=p_m.cell_side;
    	// mechanism_matrix = p_m.mechanism_matrix;
    	this.add_info(p_m.get_info());
    }
    */


 // default constructor
    public Mechanism(int p_hsize, int p_vsize, double p_cell_side) {
    	hsize=p_hsize; 
    	vsize=p_vsize;
    	cell_side=p_cell_side;
    }  
    

	// computes the expected loss of the current mechanism with respect to 
    // the given prior and loss function. 
    public double get_exp_loss_for_prior(Prior p_prior, Loss p_loss) {
        if (!(p_prior.hsize==hsize && p_prior.vsize==vsize && p_prior.cell_side==cell_side)) {
        	System.out.println("error: can not compute expected loss - invalid prior for the mechanism");
        	System.exit(0);
        }
 		double exp_loss=0;
        for (int in_x=0; in_x<hsize;in_x++)
    		for (int in_y=0; in_y<vsize;in_y++) 
               	for (int out_x=0;out_x<hsize;out_x++)
               		for (int out_y=0;out_y<vsize;out_y++) 
               			exp_loss= exp_loss + 
               			p_loss.get_loss(in_x*cell_side, in_y*cell_side, 
               					      out_x*cell_side, out_y*cell_side)*
               			getProb(in_x,in_y,out_x,out_y)*
               			p_prior.getProb(in_x, in_y);
        return exp_loss;
        // a single linear algebraic expression: does not improve performance 
        // return get_mech_matrix().mul(p_loss.get_loss_matrix(hsize, vsize, cell_side)).transpose().mmul(p_prior.get_prior_vector()).sum();
 	}
    
    
    // returns the mechanism matrix: it is executed only if its subclass does not 
    // already have a mechanism matrix. If it has a matrix, the subclass should 
    // override this method
    public DoubleMatrix get_mech_matrix() {
    	DoubleMatrix mech_matrix = new DoubleMatrix(hsize*vsize,hsize*vsize);
		for (int in_y=0; in_y<vsize;in_y++) 
			for (int in_x=0; in_x<hsize;in_x++)
           		for (int out_y=0;out_y<vsize;out_y++) 
           			for (int out_x=0;out_x<hsize;out_x++)
               			mech_matrix.put(in_y*hsize+in_x, out_y*hsize+out_x,getProb(in_x,in_y,out_x,out_y));
		return mech_matrix;
    }

    // returns a mechanism row distribution (probabilities) at p_in: 
    // it is executed only if its subclass does not 
    // already have a mechanism matrix. If the subclass does, it should 
    // override this method for performance. This method is used by EmpData class 
    // in the EM framework to generate obfuscated data. 
    // p_in is specified in linear order (y*hsize + x)
    public double[] get_mech_row_probabilities(int p_in) {
    	int in_y = p_in/hsize;
    	int in_x = p_in%hsize;
    	double[] row_probabilities = new double[hsize*vsize]; 
		for (int out_y=0; out_y<vsize;out_y++) 
			for (int out_x=0; out_x<hsize;out_x++)
				row_probabilities[out_y*hsize+out_x]= getProb(in_x,in_y,out_x,out_y);
		return row_probabilities;
    }

    // returns a mechanism column (out) as a column vector: executed only if its subclass does not 
    // already have a mechanism matrix. If it has a matrix, it should override 
    // this method for performance.
    // p_out is specified in linear order (y*hsize + x)
    public DoubleMatrix get_mech_column(int p_out) {
    	int out_y = p_out/hsize;
    	int out_x = p_out%hsize;
    	DoubleMatrix mech_col = new DoubleMatrix(hsize*vsize);
		for (int in_y=0; in_y<vsize;in_y++) 
			for (int in_x=0; in_x<hsize;in_x++)
               	mech_col.put(in_y*hsize+in_x,getProb(in_x,in_y,out_x,out_y));
		return mech_col;
    }
    
    // This method writes the current mechanism to a given file.
 	public void write_mechanism_to_file(String p_mech_file) {
 		BufferedWriter bufferedWriter = null;
        try {    
        	bufferedWriter = new BufferedWriter(new FileWriter(p_mech_file));
        	// write stuff
        	bufferedWriter.write(mech_info+"\n\n");
            bufferedWriter.write("cell_side_in_km= "+ cell_side + "\n\n");
            bufferedWriter.write(get_mechanism_labels_and_data_str());
        	
        	// end of writing
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
        	try {
        		if (bufferedWriter != null) {
        			bufferedWriter.flush();
        			bufferedWriter.close();
        		}
        	} catch (IOException ex) {
        		ex.printStackTrace();
        	}
        }
 	}
 	
 	// writes the mechanism matrix to a file. 
 	// Used to interface with Kostas implementation
 	public void write_mechanism_matrix_to_file(String p_file) {
 		BufferedWriter bufferedWriter = null;
        try {    
            int data_spacing = Settings.prob_spacing;  // spaces between probabilities;
        	bufferedWriter = new BufferedWriter(new FileWriter(p_file));
            for (int in_y=0; in_y<vsize;in_y++)
        		for (int in_x=0; in_x<hsize;in_x++) {  // for every input location
                   	for (int out_y=0;out_y<vsize;out_y++)
                   		for (int out_x=0;out_x<hsize;out_x++) {// for every output location
                   			bufferedWriter.write(String.format("%01."+Settings.prop_decimal_places+"f", 
                        			      getProb(in_x,in_y,out_x,out_y)));
                        	for (int k=0;k<data_spacing;k++) bufferedWriter.write(" ");
                   		}
                   	bufferedWriter.write("\n"); // move to a new row
        		}
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
        	try {
        		if (bufferedWriter != null) {
        			bufferedWriter.flush();
        			bufferedWriter.close();
        		}
        	} catch (IOException ex) {
        		ex.printStackTrace();
        	}
        }
 	}
 	
 	// This method writes the current mechanism to a given file. May use the above standard
 	public void write_signature_to_file(PrivMetric p_privObj, String p_sign_file) {
 		BufferedWriter bufferedWriter = null;
        try {    
        	bufferedWriter = new BufferedWriter(new FileWriter(p_sign_file));
        	// write stuff
        	bufferedWriter.write(mech_info+"\n\n");
            bufferedWriter.write("cell_side_in_km= "+ cell_side + "\n\n");
            bufferedWriter.write(get_mechanism_signature_str(p_privObj));
        	
        	// end of writing
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
 	}

 	// labels and data are written in S order
 	private String get_mechanism_labels_and_data_str() {

 		int x_digits =Settings.get_x_digits();   // the number of digits representing the x coordinate
    	int y_digits =Settings.get_y_digits();   // the number of digits representing the y coordinate
        int data_spacing = Settings.prob_spacing;  // spaces between probabilities;
        int label_spacing = 2+Settings.prop_decimal_places + data_spacing - 
        		            (x_digits+y_digits);  // spaces between column labels
 		
        String outputPosLabels ="";  // the column labels
 		for (int k=0; k<(x_digits+y_digits+data_spacing);k++) outputPosLabels = outputPosLabels+" "; // initial space 

 		String matrixStr="";   // this string will accumulates the mechanism matrix
        // to be written to the file later after writing 
        // column labels. 

    	String in_loc;
        for (int in_y=0; in_y<vsize;in_y++)
    		for (int in_x=0; in_x<hsize;in_x++) {  // for every input location
    			in_loc=String.format("%0"+x_digits+"d", in_x)+
    					String.format("%0"+y_digits+"d", in_y);
    			outputPosLabels = outputPosLabels + in_loc;
    			for (int k=0;k<label_spacing;k++) outputPosLabels=outputPosLabels+" ";
        		matrixStr = matrixStr + "\n"+in_loc;
        		for (int k=0;k<data_spacing;k++) matrixStr = matrixStr +" ";
               	for (int out_y=0;out_y<vsize;out_y++)
               		for (int out_x=0;out_x<hsize;out_x++) {// for every output location
                    	matrixStr = matrixStr + 
                    	   String.format("%01."+Settings.prop_decimal_places+"f", 
                    			      getProb(in_x,in_y,out_x,out_y));
                    	for (int k=0;k<data_spacing;k++) matrixStr=matrixStr+" ";
               		}
    		}
        return outputPosLabels+matrixStr;
 	}
 	
 	// return a matrix (similar to the mechanism one) representing the signature of the mechanism,
 	// i.e. the entry (in, out) is exactly 1/d(in,out)*ln(P(out|in)/P(out|out)). This helps to see 
 	// how similar the mechanism is to the tg-mechanism, and also to verify the privacy. Every entry
 	// should be at most 1. 
 	private String get_mechanism_signature_str(PrivMetric p_privObj) {

 		int x_digits =Settings.get_x_digits();   // the number of digits representing the x coordinate
    	int y_digits =Settings.get_y_digits();   // the number of digits representing the y coordinate
        int data_spacing = Settings.prob_spacing;  // spaces between probabilities;
        int label_spacing = 2+Settings.prop_decimal_places + data_spacing - 
        		            (x_digits+y_digits);  // spaces between column labels
 		
        String outputPosLabels ="";  // the column labels
 		for (int k=0; k<(x_digits+y_digits+data_spacing);k++) outputPosLabels = outputPosLabels+" "; // initial space 

 		String matrixStr="";   // this string will accumulates the mechanism matrix
        // to be written to the file later after writing 
        // column labels. 

    	String in_loc;
    	double entry;
    	double prob_in_out;
    	String zentry ="zz";  for (int k=0;k<Settings.prop_decimal_places;k++) zentry=zentry+"z";
        for (int in_y=0; in_y<vsize;in_y++)
    		for (int in_x=0; in_x<hsize;in_x++) {  // for every input location
    			in_loc=String.format("%0"+x_digits+"d", in_x)+
    					String.format("%0"+y_digits+"d", in_y);
    			outputPosLabels = outputPosLabels + in_loc;
    			for (int k=0;k<label_spacing;k++) outputPosLabels=outputPosLabels+" ";
        		matrixStr = matrixStr + "\n"+in_loc;
        		for (int k=0;k<data_spacing;k++) matrixStr = matrixStr +" ";
               	for (int out_y=0;out_y<vsize;out_y++)
               		for (int out_x=0;out_x<hsize;out_x++) {// for every output location
               			prob_in_out = getProb(in_x,in_y,out_x,out_y);
               			if (prob_in_out<=0)    // if this prob is zero, all all column is zero 
               				matrixStr = matrixStr + zentry; 
               			else {
               				entry = (getProb(out_x,out_y,out_x,out_y)/prob_in_out)
               						/Math.exp(p_privObj.get_priv_distance(in_x, in_y, out_x, out_y));
               				matrixStr = matrixStr + 
               						String.format("%01."+Settings.prop_decimal_places+"f", entry);
               			}
               			for (int k=0;k<data_spacing;k++) matrixStr=matrixStr+" ";
               		}
    		}
        return outputPosLabels+matrixStr;
 	}

 	
 	public void add_info(String p_extra_info) {
 		mech_info = mech_info + "\n" + p_extra_info;
 	}
 	
 	public String get_info() {
 		return mech_info;
 	}
 	
    public void write_diagonal_to_file(String p_diag_file) {
    	int data_spacing = Settings.prob_spacing;
    	String mu_data ="";
    	BufferedWriter bufferedWriter = null;
        try {    
        	bufferedWriter = new BufferedWriter(new FileWriter(p_diag_file));
            for (int y=0;y<vsize;y++) {
            	for (int x=0;x<hsize;x++) {
            		for (int k=0;k<data_spacing;k++) mu_data = mu_data+" ";
            		mu_data = mu_data + String.format("%01."+Settings.prop_decimal_places+"f",
     		               getProb(x,y,x,y));
            	}
        		mu_data = mu_data + "\n"; // move to next line
        	}
//            bufferedWriter.write("epsilon = "+ eps+" per 1km, cell_side_in_km= "+ cell_side+"\n\n");
            bufferedWriter.write(mu_data);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

 	
 	abstract public double getProb(int p_in_x, int p_in_y,int p_out_x,int p_out_y);
 	
}
