package priv.plan.mechanisms;

import java.util.ArrayList;
import java.util.List;

import org.jblas.DoubleMatrix;

import priv.plan.loss.Loss;
import priv.plan.priors.Prior;

public class RMechanism extends Mechanism {
	
	  // A mechanism constructed by remapping the given mechanism using
	  // a specific remap function, or by constructing the optimal remap 
	  // with respect a given prior.
	
	private DoubleMatrix mechanism_mat;   
	private List<List<Integer>> opt_remap;
	  //ith entry of the above list contains a list of locations that are mapped to i
	  // a location is specified as the index in the S order
	
	private double opt_loss=0;
	
	// constructs a mechanism from a mechanism p_mech and a remap p_remap. 
	public RMechanism(Mechanism p_mech, Remap p_remap) {
		super(p_mech.hsize,p_mech.vsize,p_mech.cell_side);
		if (p_mech.hsize != p_remap.hsize || p_mech.vsize != p_remap.vsize) {
			System.out.println("unable to create mechanism: dimensions of the mechanism and remap are inconsistent");
			System.exit(0);
		}
		String info = "// Remapped mechanism  \n"
				+ "// Size: hsize = "+ hsize +", vsize = "+ vsize;
		add_info(info);

		mechanism_mat = p_mech.get_mech_matrix().mmul(p_remap.get_remap_matrix());
	}
	
	// construct a remapped version of p_mech using the optimal remap computed 
	// using the prior p_prior and the loss p_lossObj;
	public RMechanism(Mechanism p_mech, Prior p_prior, Loss p_lossObj) {
 		super(p_prior.hsize,p_prior.vsize,p_prior.cell_side);
 		if (p_mech.hsize != p_prior.hsize 
 				|| p_mech.vsize != p_prior.vsize
 				|| p_mech.cell_side != p_prior.cell_side) {
 			System.out.println("the mechanism and the prior are incomatiple");
 			System.exit(0);
 		}
 		
        //A header for the remapped mechanism
        String info = "// Remapped mechanism  \n"
                    + "// Size: hsize = "+ hsize +", vsize = "+ vsize;
        add_info(info);

        DoubleMatrix prior_vect = p_prior.get_prior_vector();   // a column vector
        mechanism_mat = p_mech.get_mech_matrix();
        DoubleMatrix loss_mat = p_lossObj.get_loss_matrix(hsize, vsize, cell_side);

 	   // create list of targets and and add to each target a dummy output (-1) to make it nonempty
        opt_remap= new ArrayList<List<Integer>>();
 	   for (int t=0;t<hsize*vsize;t++) {
 		  List<Integer> l= new ArrayList<Integer>();
 		  l.add(-1);
 		  opt_remap.add(l);
 	   }
 	   
 	   for (int out=0;out<hsize*vsize;out++) {
		   // notes: In general we have x--perturbed--> y and we want to map y --> z 
		   // using the formula z(y) = argmin_z Sum_x P(x|y).loss(x,z)
 		   //                        = argmin_z Sum_x P(x).P(y|x).loss(x,z)
 		   // The above formula is computed by element-wise multiplying column vectors P(x) 
 		   // and P(y|x) to yield column vector C of dim(X*1) and then matrix-multiplying 
 		   // the loss_matrix L of dim(Z*X) and the vector C.
 		   // This yield a column vector target_losses with dim(Z*1) in which the minimum
 		   // entry is the minimum loss, and the corresponding z is the required target for
 		   // y=out. Note in our case Z is exactly the domain X, however if they are different
 		   // the above loss_matrix has to be in the form Z*X, i.e. |Z| rows and |X| columns.
 		   // note also the minimum entry in target_losses is exactly the conditional 
 		   // vulnerability when the observable out is y.
 		   // performing this operation as a linear algebraic expression instead of using 
 		   // loops has significantly improved the computation.  

 		   //System.out.println("processing out = "+ out);
 		   // target_losses = loss_mat * (prior_vect % out_col)
 		   // out_col is the out'th column in the mechanism matrix.
 		   // * is matrix product, % is element-wise product
 		   DoubleMatrix out_col = mechanism_mat.getColumn(out);
 		   DoubleMatrix targets_losses = loss_mat.mmul(prior_vect.mul(out_col)); 
 		   int best_target = targets_losses.argmin();
		   opt_loss=opt_loss + targets_losses.get(best_target);
		   opt_remap.get(best_target).add(out);
 	   }
 		
 		
    	// writing the remapping information
        info = "// optimal remapping of the mechanism outputs \n" 
             + "// "+ get_opt_remap_str()+"\n\n"
        // writing the expected loss before and after remap
             + "// loss type: "+ p_lossObj.get_name() +"\n"
             + "// expected loss before remapping= "+ p_mech.get_exp_loss_for_prior(p_prior, p_lossObj) +"\n"
             + "// expected loss with optimal remapping)= "+ opt_loss;
        add_info(info);

	}
	
	private String get_opt_remap_str() {
		String ret="";
		for (int i=0;i<opt_remap.size();i++) 
			for (int j=1;j<opt_remap.get(i).size();j++)  // 1 to skip the first dummy element -1
				ret = ret + opt_remap.get(i).get(j)+"->"+i+" ";
		
		return ret;
		
	}
    
   
	public double getProb(int p_in_x, int p_in_y, int p_out_x, int p_out_y) {
		double ret =0;
		int in = p_in_y*hsize+p_in_x;
		int t = p_out_y*hsize+p_out_x;
		for (int j=1;j<opt_remap.get(t).size();j++)  // 1 to skip the first dummy element which is -1
			ret = ret + mechanism_mat.get(in , opt_remap.get(t).get(j));
		return ret;
 	}
    
}

