package priv.plan.mechanisms;

import java.util.ArrayList;
import java.util.List;

import priv.plan.dist.Distance;
import priv.plan.gdset.GDataset;
import priv.plan.gdset.UserStatistic;
import priv.plan.loss.Loss;

// This is a mechanism placed to cover the geographic region 
// with south boundary p_south (minimum latitude)
// and west boundary p_west(minimum longitude) 
// Recall that the information about the grid size and cell_size are stored 
// in the mechanism file 

// important: mapping between x,y coordinates of the cells and the lat/lng geographical coordinates
// necessary to construct priors. 
// x coordinate increases as we go in the east direction
// y coordinate increases as we go towards the north.

public class GMechanism {
	public double south;
	public double west;
	Mechanism mech;


// This constructor is used when GMechanism extends Mechanism
// to copy the attributes of p_mech to the basic attributes in 
// the super class Mechanism, and then add the new attributes. 
/*   
   public GMechanism(Mechanism p_mech, double p_south, double p_west) {
		super(p_mech);        // effectively copies the attributes from p_mech
		south = p_south;
		west = p_west;
	}	
*/	
	public GMechanism(Mechanism p_mech, double p_south, double p_west) {
			mech = p_mech;        
			south = p_south;
			west = p_west;
		}	
	

	

	// compute the expected losses of the mechanism for the priors of the 
	// users in a given gdataset object (containing a list of locations).
	// p_cell_side specifies the grid granularity of the input locations 
	// on which user priors is constructed. However the grid granularity 
	// of the output locations is determined by mech.cell_side. This is 
	// use for evaluating loss for optimal mechanism (e.g. mech.cell_side = 2km) 
	// for priors on a higher resolutions grid (e.g. p_cell_side=0.2km)
	//
	// It returns a list of these losses. 
	// Every loss is computed only to the users having at least the given 
	// threshold of check-ins.
	public List<Double> get_exp_losses_for_gdata_priors(GDataset p_gdset, double p_cell_side, Loss p_lossObj, int p_user_checkins_threshold) {

		// get the list of users having above the thresholds. 
		List<UserStatistic> us = p_gdset.get_users_statistics(p_user_checkins_threshold);
		System.out.println("the list contains "+us.size()+" users");
		List<String> us_users = new ArrayList<String>();
		for (int i=0; i<us.size();i++) 
			us_users.add(us.get(i).user_id);

		List<Double> users_priors_exp_losses;
		users_priors_exp_losses = new ArrayList<Double>(); // dynamic list of losses of the users.

		// find the first check-in for the first user in the list
		int k=0;
		while (!p_gdset.get_checkin(k).user_id.equals(us_users.get(0)))
			k++;
		String old_user_id = p_gdset.get_checkin(k).user_id;
		int old_user_checkins=1;
		// add the loss of the cell of the new position to the total loss of the user loss
		double old_user_prior_loss = get_exp_loss_for_cell(Double.parseDouble(p_gdset.get_checkin(k).lat),
				Double.parseDouble(p_gdset.get_checkin(k).lng), p_cell_side,p_lossObj);


		for (k=k+1;k<p_gdset.get_size();k++) 
			if (us_users.contains(p_gdset.get_checkin(k).user_id)) {
				// check if a new user is the same as before
				if (p_gdset.get_checkin(k).user_id.equals(old_user_id)) {
					old_user_checkins++;
					//update the user's loss;
					old_user_prior_loss = old_user_prior_loss + get_exp_loss_for_cell(Double.parseDouble(p_gdset.get_checkin(k).lat),
							Double.parseDouble(p_gdset.get_checkin(k).lng), p_cell_side,p_lossObj);
				}
				else {
					// process old user
					//System.out.println("for user "+old_user_id+ " processed "+ old_user_checkins+" checkins");
					// here we normalize the user's loss and add the result to the list
					users_priors_exp_losses.add( (double) old_user_prior_loss/old_user_checkins);
					// now we have a new user: update old user info
					old_user_id = p_gdset.get_checkin(k).user_id;
					old_user_checkins=1;
					// add the loss of the new position 
					old_user_prior_loss = get_exp_loss_for_cell(Double.parseDouble(p_gdset.get_checkin(k).lat),
							Double.parseDouble(p_gdset.get_checkin(k).lng), p_cell_side, p_lossObj);
				}
			}
		// process the last user
		//System.out.println("for user "+old_user_id+ " processed "+ old_user_checkins+" checkins");
			// here we normalize the user's loss and add the result to the list
			users_priors_exp_losses.add( (double) old_user_prior_loss/old_user_checkins);
		System.out.println("processed "+ users_priors_exp_losses.size());
		return users_priors_exp_losses;
	}
	
	
	// same as before but assumes that the input grid has the same resolution as the output one
	// i.e p_cell_side = mech.cell_side. 
	public List<Double> get_exp_losses_for_gdata_priors(GDataset p_gdset, Loss p_lossObj, int p_user_checkins_threshold) {
		return get_exp_losses_for_gdata_priors(p_gdset, mech.cell_side, p_lossObj, p_user_checkins_threshold);
	}

	
	// returns the conditional expected loss for a the center of the input cell that includes 
	// the given point. Every input cell has size p_cell_side, however every output cell has 
	// size mech.cell_side. This allows computing the loss of the input cell even it is 
	// defined on a grid with a different resolution in comparison to the output cells.   
	private double get_exp_loss_for_cell(double p_lat, double p_lng, double p_cell_side, Loss p_lossObj) {
		int in_x = get_cell_x_of_lng(p_lng, p_cell_side);
		int in_y = get_cell_y_of_lat(p_lat, p_cell_side);

		double loss =0;
		for (int out_y=0; out_y<mech.vsize; out_y++)
			for (int out_x=0; out_x<mech.hsize;out_x++) {
				// get horizontal, vertical distance of input and output from the corner 
				// of the south-west point
				double in_x_from_sw = (0.5+in_x)*p_cell_side; 
				double in_y_from_sw = (0.5+in_y)*p_cell_side; 
				double out_x_from_sw = (0.5+out_x)*mech.cell_side; 
				double out_y_from_sw = (0.5+out_y)*mech.cell_side; 
				
				double in_to_out_dist = Distance.get_euclid_dist(in_x_from_sw, in_y_from_sw, out_x_from_sw, out_y_from_sw);
				loss = loss + mech.getProb((int)Math.floor(in_x_from_sw/mech.cell_side), 
						                   (int)Math.floor(in_y_from_sw/mech.cell_side), 
						                   (int)Math.floor(out_x_from_sw/mech.cell_side), 
						                   (int)Math.floor(out_y_from_sw/mech.cell_side)) * 
						p_lossObj.get_loss_by_dist(in_to_out_dist);
				//lvect.put(out_y*mech.hsize+out_x, p_lossObj.get_loss_by_dist(in_to_out_dist));
			}
		// Don't use the following..too slow.
		//double cond_expected_loss = lvect.dot(mech.get_mech_matrix().getRow(in_y*mech.hsize+in_x));
		//return cond_expected_loss;
		return loss;
		
	}
	
	
	// compute the expected losses of the mechanism for the users in a given gdataset object 
	// containing a set of locations. It returns a list of these losses. 
	// the loss is computed only to the users having at least the given threshold of check-ins.
	// Note that if the dataset has one user, the list will have one loss element. 
	public List<Double> get_exp_losses_for_gdata_points(GDataset p_gdset, Loss p_lossObj, int p_user_checkins_threshold) {

		// get the list of users having above the thresholds. 
		List<UserStatistic> us = p_gdset.get_users_statistics(p_user_checkins_threshold);
		System.out.println("the list contains "+us.size()+" users");
		List<String> us_users = new ArrayList<String>();
		for (int i=0; i<us.size();i++) 
			us_users.add(us.get(i).user_id);

		List<Double> users_exp_losses;
		users_exp_losses = new ArrayList<Double>(); // dynamic list of losses of the users.

		// find the first check-in for the first user in the list
		int k=0;
		while (!p_gdset.get_checkin(k).user_id.equals(us_users.get(0)))
			k++;
		String old_user_id = p_gdset.get_checkin(k).user_id;
		int old_user_checkins=1;
		// add the loss of the new position to the total loss of the user loss
		double old_user_loss = get_exp_loss_for_point(Double.parseDouble(p_gdset.get_checkin(k).lat), 
				Double.parseDouble(p_gdset.get_checkin(k).lng), 
				p_lossObj);


		for (k=k+1;k<p_gdset.get_size();k++) 
			if (us_users.contains(p_gdset.get_checkin(k).user_id)) {
				// check if a new user is the same as before
				if (p_gdset.get_checkin(k).user_id.equals(old_user_id)) {
					old_user_checkins++;
					//update the user's loss;
					old_user_loss = old_user_loss + get_exp_loss_for_point(Double.parseDouble(p_gdset.get_checkin(k).lat), 
							Double.parseDouble(p_gdset.get_checkin(k).lng), 
							p_lossObj);
				}
				else {
					// process old user
					//System.out.println("for user "+old_user_id+ " processed "+ old_user_checkins+" checkins");
					// here we normalize the user's loss and add the result to the list
					users_exp_losses.add( (double) old_user_loss/old_user_checkins);
					// now we have a new user: update old user info
					old_user_id = p_gdset.get_checkin(k).user_id;
					old_user_checkins=1;
					// add the loss of the new position 
					old_user_loss = get_exp_loss_for_point(Double.parseDouble(p_gdset.get_checkin(k).lat), 
							Double.parseDouble(p_gdset.get_checkin(k).lng), 
							p_lossObj);
				}
			}
		// process the last user
		//System.out.println("for user "+old_user_id+ " processed "+ old_user_checkins+" checkins");
			// here we normalize the user's loss and add the result to the list
			users_exp_losses.add( (double) old_user_loss/old_user_checkins);
		System.out.println("processed "+ users_exp_losses.size());
		return users_exp_losses;
	}
	

	// returns the conditional expected loss for a specific input point (lat,lng)
	private double get_exp_loss_for_point(double p_lat, double p_lng, Loss p_lossObj) {
		
		// System.out.println("processing "+ p_lat +","+p_lng);
		// get cell indices of the input point with respect to the grid of the mechanism
		int in_x = get_cell_x_of_lng(p_lng,mech.cell_side);
		int in_y = get_cell_y_of_lat(p_lat,mech.cell_side);
		
		// compute a vector containing the loss for every cell in the grid
		// DoubleMatrix lvect = new DoubleMatrix(mech.hsize*mech.vsize);
		double loss =0;
		for (int out_y=0; out_y<mech.vsize; out_y++)
			for (int out_x=0; out_x<mech.hsize;out_x++) {
				double dy = (0.5+ out_y)*mech.cell_side;
				double dx = (0.5+ out_x)*mech.cell_side;
				double out_cell_center_lat = Distance.adjust_latitude(south, west, dy);
				double out_cell_center_lng = Distance.adjust_longitude(south, west, dx);
				double in_to_out_dist = Distance.get_distance_on_earth(p_lat, p_lng, 
						                       out_cell_center_lat, out_cell_center_lng); 
				loss = loss + mech.getProb(in_x, in_y, out_x, out_y) * 
						p_lossObj.get_loss_by_dist(in_to_out_dist);
				//lvect.put(out_y*mech.hsize+out_x, p_lossObj.get_loss_by_dist(in_to_out_dist));
			}
		// The following is too slow.
		//double cond_expected_loss = lvect.dot(mech.get_mech_matrix().getRow(in_y*mech.hsize+in_x));
		//return cond_expected_loss;
		return loss;
	}
	
	
/*	
	// This method calculates the expected loss for a specific point (lat,lng)
	// It uses the saturation level of the loss function to optimize the performance.  
	private double get_exp_loss_for_point_with_sat(double p_lat, double p_lng, Loss p_lossObj) {
		
		// get the cell of the given point
		int in_x = get_cell_x_of_lng(p_lng, mech.cell_side);
		int in_y = get_cell_y_of_lat(p_lat, mech.cell_side);
		
		double cond_expected_loss=0;
		double dx ; double dy;
		double out_cell_center_lat;  double out_cell_center_lng;
		double in_to_out_dist;
		
		// here we should optimize computation by looping only on outputs that 
		// are within saturation distance from the input point. and assign 
		// max loss for the rest. 
		int sat_cells = (int) Math.ceil(p_lossObj.get_saturation_dist()/mech.cell_side);
		
		int out_x_start = in_x-sat_cells;
		if (out_x_start<0) out_x_start =0;
		int out_x_end = in_x+sat_cells;
		if (out_x_end>=mech.hsize) out_x_end = mech.hsize-1;
		int out_y_start = in_y-sat_cells;
		if (out_y_start<0) out_y_start =0;
		int out_y_end = in_y+sat_cells;
		if (out_y_end>=mech.vsize) out_y_end = mech.vsize-1;
		
		double out_prob;
		double out_prob_sum =0;
		for (int out_x=out_x_start; out_x<=out_x_end; out_x++) 
			for (int out_y=out_y_start;out_y<=out_y_end;out_y++) {
				// compute the coordinates of the out cell center.
				dy = (0.5+ out_y)*mech.cell_side;
				dx = (0.5+ out_x)*mech.cell_side;
				out_cell_center_lat = Distance.adjust_latitude(south, west, dy);
				out_cell_center_lng = Distance.adjust_longitude(south, west, dx);
				in_to_out_dist = Distance.get_distance_on_earth(p_lat, p_lng, 
						                       out_cell_center_lat, out_cell_center_lng); 
				out_prob = mech.getProb(in_x,in_y,out_x,out_y);
				out_prob_sum = out_prob_sum + out_prob;
				cond_expected_loss = cond_expected_loss + out_prob *p_lossObj.get_loss_by_dist(in_to_out_dist);
			}
		cond_expected_loss = cond_expected_loss + (1-out_prob_sum)*p_lossObj.get_max_loss();
		return cond_expected_loss;
	}
*/	
	
	
	// get the cell of a point on a grid specified by p_cell_side
	private int get_cell_x_of_lng(double p_lng, double p_cell_side) {

		// get the cell of the given point
		double hsize = (int) mech.hsize*mech.cell_side/p_cell_side; // hsize according to p_cell_side
		double xdist = Distance.get_distance_on_earth(south, west, south, p_lng);
		int in_x =  (int) (xdist/p_cell_side);
		if (in_x>=hsize) {
			System.out.println("warning: input point is slightly out of grid: xdist = "+ xdist);
			in_x--;
		}
		return in_x;

	
	}
	
	private int get_cell_y_of_lat(double p_lat, double p_cell_side) {
		// get the cell of the given point on the grid specified by p_cell_side
		double vsize = (int) mech.vsize*mech.cell_side/p_cell_side; // vsize according to p_cell_side
		double ydist = Distance.get_distance_on_earth(south, west, p_lat, west);
		int in_y =  (int) (ydist/p_cell_side);
		if (in_y>=vsize) {
			System.out.println("warning: input point is out of grid: ydist = "+ ydist);
			in_y--;
		}
		return in_y;
	}

	// This method writes the current mechanism to a given file
 	public void write_mechanism_to_file(String p_mech_file) {
 		mech.write_mechanism_to_file(p_mech_file);
 	}

}
