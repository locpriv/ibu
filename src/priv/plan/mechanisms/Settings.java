package priv.plan.mechanisms;
/**
 *
 * @author javadb.com
 */
public class Settings {
	// remember to correct
	public static int max_hsize = 1000;   // max length of the grid on x dimension
	public static int max_vsize = 1000;   // max length of the grid on y dimension
	public static int prop_decimal_places = 10;	 // probability precision. Used for writing in files  
	public static int prob_spacing = 1;  // spaces between probabilities 


	public static int get_x_digits() {
		return (int) Math.ceil(Math.log10(max_hsize));
	}

	public static int get_y_digits() {
		return (int) Math.ceil(Math.log10(max_vsize));
	}

}

