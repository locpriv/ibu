package priv.plan.mechanisms;

import org.jblas.DoubleMatrix;

import priv.plan.loss.Loss;
import priv.plan.lpsolve.GLPK_LPSolver;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.PrivMetric;

// Optimal mechanism constructed on a grid.
// The rows/columns of the mechanism are in S order 

public class OptMechanism extends Mechanism {
	
      private PrivMetric privObj;
      private Loss lossObj;      // loss object
      private Prior prior;    // prior used for optimization
      private DoubleMatrix mechanism_matrix;  
      // stores the entries of the mechanism since every entry takes long time to be computed
    
    public OptMechanism(Prior p_prior, PrivMetric p_privObj, Loss p_lossObj) {
    	super(p_prior.hsize,p_prior.vsize,p_prior.cell_side);
        privObj = p_privObj;
        lossObj = p_lossObj;
        prior = p_prior;
    	mechanism_matrix=new DoubleMatrix(hsize*vsize,hsize*vsize);	

    	fill_mech_matrix();
    	
    	String info = "// Optimal mechanism for a prior \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize+"\n" 
    			+ "// Privacy type: "+ privObj.get_name() + "\n"
                + "// epsilon =" + privObj.eps +" for 1km \n"
                + "// Loss type = "+ lossObj.get_name();    	
    	add_info(info);
    }
    
    // fill the mechanism matrix elements by solving a linear program
    private void fill_mech_matrix() {
		GLPK_LPSolver s = new GLPK_LPSolver(prior,privObj,lossObj);
		s.construct_LP();
//      s.write_LP(prior_input_file +".lp");
		s.solve();
        add_info("// optimal Expected loss = "+ s.opt_exp_loss+"\n");
		mechanism_matrix = s.get_opt_mech_matrix();
		s.remove_LP();
    }
    
    // returns the mechanism matrix: override the super class method to improve performance 
    public DoubleMatrix get_mech_matrix() {
 		return mechanism_matrix;
    }
 
    // returns a mechanism matrix row probabilities: override the super class method to improve performance
    // used For EM. p_in is the out location in linear order, i.e. out_y*hsize +out_x. 
    public double[] get_mech_row_probabilities(int p_in) {
	   return mechanism_matrix.getRow(p_in).toArray();
    }

    // returns the mechanism matrix column: override the super class method to improve performance
    // p_out is the out location in linear order, i.e. out_y*hsize +out_x. 
    public DoubleMatrix get_mech_column(int p_out) {
    	return mechanism_matrix.getColumn(p_out);
    }
    
 	public double getProb(int p_in_x, int p_in_y, int p_out_x, int p_out_y) {
 		return mechanism_matrix.get(p_in_y*hsize+p_in_x, p_out_y*hsize+p_out_x);
 	}

}
