package priv.plan.mechanisms;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.jblas.DoubleMatrix;

// A mechanism (on a grid) read from a matrix stored in a file.
// Unlike AnnotatedFileMechanism no extra information (cell_side and h_size, v_size)
// are included for the mechanism in the file. These pieces of information are passed
// to the constructor. rows/columns in the file are in S order.

// Useful to interface with Kostas implementation.

public class MatrixFileMechanism extends Mechanism {
    
	private DoubleMatrix mechanism_matrix; // mechanism_matrix 

    // constructs a mechanism from a given file. 
	//Any line starting with // is skipped.
    public MatrixFileMechanism(String p_mech_input_file, int p_hsize, int p_vsize, double p_cell_side) {
    	super(p_hsize,p_vsize,p_cell_side);
    	BufferedReader bufferedReader = null;
    	
    	String line;      // to hold a line string from the input file
        try {    
        	bufferedReader = new BufferedReader(new FileReader(p_mech_input_file));
        	int pos_in_line=0; hsize=p_hsize; vsize=p_vsize;
        	String s; 

        	// now read the mechanism probabilities line by line and fill the mechanism matrix
            mechanism_matrix = new DoubleMatrix(hsize*vsize,hsize*vsize);
        	int row_count=0;                //will count number of rows for validation 
            
            int curr_in_y=0; int curr_in_x=0;   // the current line corresponds 
                                                      // to an input location with x,y
            int curr_out_y; int curr_out_x;    
            String prob_str;
            boolean prob_found = false;
            while ((line = bufferedReader.readLine()) != null && !line.startsWith("//")) {
            	pos_in_line=0;
            	curr_out_x=0; curr_out_y=0; 
            	prob_found = false;
            	while (pos_in_line<line.length()-1) {
            		s=line.substring(pos_in_line, pos_in_line+2);
            		if (s.matches("[0|1]\\.")) {
            			prob_found = true;
            			prob_str = line.substring(pos_in_line, 
            					                  pos_in_line+2+Settings.prop_decimal_places);
            			mechanism_matrix.put(curr_in_y*hsize+curr_in_x, 
            					curr_out_y*hsize+curr_out_x,Double.parseDouble(prob_str));
           			    if (curr_out_x< hsize-1) curr_out_x=curr_out_x+1;
           			    else { 
           			    	curr_out_x =0;
           			    	curr_out_y++;
           			    }
            		}
            		pos_in_line++;
            	}                      // here we reach end of the line: consider next y
            	if (prob_found) { 
 	                   // A complete row of probabilities has been finished
            		   // prob_found indicates that this row has at least one probability
            		if (curr_out_y!=vsize) {      // validate the obtained row
            			System.out.println("invalid mechanism:too few/many probabilities in one row");
            			System.exit(0);
            		}
            		row_count++;  // a valid row has been found: get the next input x,y
            		if (curr_in_x<hsize-1) curr_in_x++;
            		else {
            			curr_in_x=0;
            			curr_in_y++; 
            			}
            	}
            }
            if (row_count != hsize*vsize) {
    		    System.out.println("invalid mechanism:too few/many probability rows");
    		    System.exit(0);
            }
            
       } catch (FileNotFoundException ex) {
    	    System.out.println("mechanism input file is not found");
            ex.printStackTrace();
            System.exit(0);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedWriter
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    // returns the mechanism matrix: override the super class method to improve performance 
    public DoubleMatrix get_mech_matrix() {
 		return mechanism_matrix;
    }

    // returns a mechanism matrix row probabilities: override the super class method to improve performance
    // used For EM. p_in is the out location in linear order, i.e. out_y*hsize +out_x. 
   public double[] get_mech_row_probabilities(int p_in) {
	   return mechanism_matrix.getRow(p_in).toArray();
   }

    // returns the mechanism matrix column: override the super class method to improve performance
    // p_out is the out location in linear order, i.e. out_y*hsize +out_x. 
    public DoubleMatrix get_mech_column(int p_out) {
    	return mechanism_matrix.getColumn(p_out);
    }

    // returns the entry of the mechanism 
 	// for input in_x,in_y and output out_x,out_y
 	public double getProb(int p_in_x,int p_in_y,int p_out_x,int p_out_y) {
 		return mechanism_matrix.get(p_in_y*hsize+p_in_x, p_out_y*hsize+p_out_x);
 	}

}
