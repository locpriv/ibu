package priv.plan.mechanisms;

import org.jblas.DoubleMatrix;
import priv.plan.loss.Loss;

// construct the mechanism by generating points according to the pdf for every input and
// compute the probability of every cell as the proportion of the inclosed points relative
// to the total generated points. 


// Truncated planar Laplacian mechanism
// The rows and columns of the mechanism matrix represent the inputs and outputs
// of the mechanism, where rows/columns are in the S order
// i.e. the index of (x,y) is y*hsize+x.


public class TPLapMechanism extends Mechanism {
	
      public double eps;                    // used to construct Laplace matrix 
      private DoubleMatrix mechanism_matrix;  
      // stores the entries of the mechanism since every entry takes long time to be computed
    
  	  final int max_points = 5000;  // number of points generated for each input cell. 
  	  double inc = (double)1/max_points;
   
    // this constructor creates the TLap mechanism
    // specified by the size of the grid, the cell_side (in km), the type, and privacy parameter
    public TPLapMechanism(int p_hsize, int p_vsize, double p_cell_side, double p_eps) {
    	super(p_hsize,p_vsize,p_cell_side);
    	mechanism_matrix = new DoubleMatrix(p_hsize*p_vsize,p_hsize*p_vsize);	
        eps = p_eps;
    	fill_matrix_lap();
    	
    	String info = "// Truncated Laplacian mechanism  \n"
                + "// epsilon =" + eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
    	this.add_info(info);
    }
    
 	// The following constructs a mechanism that satisfies a given loss value for the uniform prior 
    // (with respect to the given loss object). This method is used to compare mechanisms with respect 
    // to statistical utility (using em algorithm). Important: we assume that loss(i,i)=0.
    public TPLapMechanism(int p_hsize, int p_vsize, double p_cell_side, Loss p_loss, double p_exp_loss) {
    	super(p_hsize,p_vsize,p_cell_side);
    	mechanism_matrix = new DoubleMatrix(p_hsize*p_vsize,p_hsize*p_vsize);	
 			double eps0=0.1; double eps1= 2/p_exp_loss;   // for p_exp_loss = 0.6, use 1.45, 1.0->0.85
 			double u;
 	 		do {
 	 			eps = (eps0+eps1)/2;
 	 	    	fill_matrix_lap();
 	 	    	// compute utility 
 	 			u = mechanism_matrix.mul(p_loss.get_loss_matrix(hsize, vsize, cell_side)).sum()/(hsize*vsize);
 				//u = this.get_exp_loss_for_prior(new Prior(hsize,vsize,cell_side),p_loss  );
 	 			if (p_exp_loss<u) eps0=eps; else eps1= eps;
 	 		}
 	 		while (Math.abs(p_exp_loss - u)>0.001);
	        String info = "// Truncated Laplacian mechanism  \n"
                + "// epsilon =" + eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
    	this.add_info(info);
    }
    

    
    // fill the mechanism matrix elements with Laplacian values
    private void fill_matrix_lap() {
        mechanism_matrix.fill(0);   // reset the matrix
    	for (int in_x=0; in_x<hsize;in_x++)
    		for (int in_y=0; in_y<vsize;in_y++) 
    			update_mech_matrix_for_input(in_x,in_y);
    }
    
 	// returns the entry of the Lap mechanism 
 	// for input x,y and output x,y
 	private void update_mech_matrix_for_input(int p_in_x,int p_in_y) {
 		double r;
 		double theta;
 		double dx,dy;
 		double out_x_d,out_y_d; // horizontal, vertical displacements from the corner of the grid.  
 		int out_x,out_y;
 		
 		// lambert function (-1) evaluation variables
 		double p;
 		double z;
 		double w;
 		double w1;
 		double l1,l2;
 		for (int i=0;i<max_points;i++) {  // sample max_points 
 			
 			// get a random noise vector
 			theta = 2*Math.PI*Math.random();  // get the angle
 			p = Math.random();
 			z = (p-1)/Math.E; 
 			l1= Math.log(-z); 
 			l2= Math.log(-Math.log(-z));
 			w = l1- l2+ l2/l1 + l2*(-2+l2)/(2*l1*l1) + 
 					l2*(6-9*l2+2*l2*l2)/(6*l1*l1*l1) + 
 					l2*(-12+36*l2-22*l2*l2+3*l2*l2*l2)/(12*l1*l1*l1*l1)+
 					l2*(60-300*l2+350*l2*l2-125*l2*l2*l2+12*l2*l2*l2*l2)/(60*l1*l1*l1*l1*l1);
 			// enhancing approximation of lambert function using Halley's iterative method,:
 			for (int j=0;j<1;j++) {
 				double num = w*Math.exp(w)-z;
 				double dom = Math.exp(w)*(w+1) - (w+2)*(w*Math.exp(w)-z)/(2*w+2); 
 				w1 = w - num/dom;
 				w=w1;
 			}
 			r = (double)(-1)*(w+1)/eps;  // get the magnitude in km. Should be converted to orders of cell size
 			                             // by dividing by cell_side 
 			
 			dx = r*Math.cos(theta)/cell_side;
 			dy = r*Math.sin(theta)/cell_side;
 			out_x_d = 0.5+ p_in_x + dx;
 			out_y_d = 0.5+ p_in_y + dy;
 			out_x = (int)Math.floor(out_x_d); 
 			out_y = (int)Math.floor(out_y_d);
 
 			// truncation
 			if (out_x<0) out_x=0;
 			if (out_x>hsize-1) out_x=hsize-1;
 			if (out_y<0) out_y=0;
 			if (out_y>vsize-1) out_y=vsize-1;
 			
 			// update the appropriate mechanism matrix element
 			double t = mechanism_matrix.get(p_in_y*hsize+p_in_x,out_y*hsize+out_x);
 			mechanism_matrix.put(p_in_y*hsize+p_in_x,out_y*hsize+out_x, t+inc) ;
 		}
 		
 		
 	}

 	// returns the mechanism matrix: override the super class method to improve performance 
    public DoubleMatrix get_mech_matrix() {
 		return mechanism_matrix;
    }
 	
    // returns a mechanism matrix row probabilities: override the super class method to improve performance
    // used For EM. p_in is the out location in linear order, i.e. out_y*hsize +out_x. 
    public double[] get_mech_row_probabilities(int p_in) {
 	   return mechanism_matrix.getRow(p_in).toArray();
    }

    // returns the mechanism matrix column: override the super class method to improve performance
    // p_out is the out location in linear order, i.e. out_y*hsize +out_x. 
    public DoubleMatrix get_mech_column(int p_out) {
    	return mechanism_matrix.getColumn(p_out);
    }

    public double getProb(int p_in_x, int p_in_y, int p_out_x, int p_out_y) {
 		return mechanism_matrix.get(p_in_y*hsize+p_in_x, p_out_y*hsize+p_out_x);
 	}

}
