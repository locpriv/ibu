package priv.plan.mechanisms;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.jblas.DoubleMatrix;
import org.jblas.Solve;
//import Jama.*;

import priv.plan.privMetrics.PrivMetric;

// Tight-constraints mechanism on a square grid (using automorphism symmetries)
// The mechanism matrix has the form 
// (double) mechanism[i_x][i_y][j_x][j_y] where the first 
// coordinates represent the row (mechanism input) and the other two 
// coordinates represent the column (mechanism output). 


public class TCSqMechanism extends Mechanism {
	
	  /* Internal class to describe a point on the grid assuming that the 
	   * origin (0,0) is the center of the grid. A unit distance is cell_side */
	  private class Point {
		  double x; double y;
		  public Point(double p_x, double p_y) {
			  x=p_x; y=p_y;
		  }
	  }

	  private PrivMetric privObj;
	  private DoubleMatrix phi_sym; //  phi_sym * mu_sym = 1
	  private DoubleMatrix mu_sym;   // mu_sym[c] is mechanism diagonal element 
	                          // for a location in symmetry class c
	  private boolean even_hsize;
	  // the above is used only in get_class_rep to return the exact coordinates
	  // of the center of a cell (representing a given class) relative to the 
	  // center of the grid. These coordinates depends on whether the hsize
	  // is odd or even. 
	  

	// this constructor creates the TC mechanism
    // specified by the size of the grid, the cell_side (in km), and privacy parameter
    public TCSqMechanism(int p_hsize, double p_cell_side, PrivMetric p_privObj) {
    	super(p_hsize,p_hsize,p_cell_side);
        if ((p_hsize%2)== 0 ) 
        	even_hsize=true; 
        else
        	even_hsize=false;
    	
    	privObj = p_privObj;
        construct_phi_sym();
        construct_mu_sym();
        
    	String info = "// Tight-constraints mechanism  \n"
    			+ "// Privacy type: "+ privObj.get_name() + "\n"
                + "// epsilon =" + privObj.eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
    	this.add_info(info);
    }
    
    //  get the index of the class that contains the given point
    //  p_x,p_y are relative to the center point of the grid 
    //  may be for example (0.5,0.5)
    private int get_sym_class(Point p) {
    	// get the point representing the class of p
    	double rep_x = Math.abs(p.x); double rep_y=Math.abs(p.y);
    	if (rep_y > rep_x) {   // swap the coordinates
    		double temp = rep_x; 
    		rep_x = rep_y;
    		rep_y = temp;
    	}
    	// get the ints of the given point, i.e. the horizontal and vertical indexes
    	// of the cell of the point (starting by 0) from the center of the grid.
    	int int_rep_x = (int) rep_x; int int_rep_y = (int) rep_y;
    	// class_ind = sum_{i=1}^{(rep_x+1)-1} i +(rep_y+1)  - 1     
    	// In the above expression 1s are added in rep_x+1, rep_y+1 to count 
    	// the encountered classes taking into account that rep_x, rep_y start
    	// from 0. We subtract 1 in the end of the expression to make the resulting 
    	// index start from 0. The above expression is simplified to the following. 

    	int class_ind = (int) (0.5*int_rep_x*(int_rep_x+1)+int_rep_y);
    	return class_ind;
    }

    
    /* get the point that represents the class having the given index */ 
    private Point get_sym_class_rep(int p_class_index) {
    	// let c be the class, and x,y be the ints of the required point coordinates. 
    	// Then we use the equation c=x(x+1)/2+y. 
    	// The basic idea is to assume that y=0 and get x by solving the
    	// equation: 1/2 x^2 +1/2 x -c = 0. Here x is the real part of the solution, 
    	// and y is obtained (from the above equation) as y = c - x(x+1)/2.
    	// The obtained x,y above are ints of the required point. Therefore 
    	// the coordinates of the return points must take into account whether 
    	// the grid is odd or even (add 0.5 if even).  
        
    	double x = -0.5 + Math.sqrt(0.25+2*p_class_index);
    	int retx = (int)x;
    	int rety = p_class_index - (int)(0.5*retx*(retx+1));
    	
    	double d_retx =retx; double d_rety=rety;
    	// handle the case when hsize is even
    	if (even_hsize==true) {
    		d_retx=retx+0.5; d_rety=rety+0.5;
    	}
    	return new Point(d_retx,d_rety);
    }
    
    
    
    // get the number of classes in the grid
    private int get_sym_classes_count() {

    	double dsize = hsize;
	    
    	// The required count may be obtained by getting the cell index of the 
    	// last point (having largest x,y coordinates) in the grid and add 1 
    	// (because the class index starts by 0). This point is exactly
	    // ( (dsize-1)/2, (dsize-1)/2 ) independently of the grid being odd or 
    	// even. In fact if hsize is odd, max_x=max_y=(hsize-1)/2, and if hsize 
    	// is even, max_x=max_y=hsize/2 -1/2. Therefore the class count is.
	    
    	double dret = 1 + get_sym_class(new Point(0.5*(dsize-1), 0.5*(dsize-1)));
    	
    	// A direct expression for dret can be obtained by substituting 
    	// get_sym_class in the above equation by its internal expression 
    	// x(x+1)/2+y where both x,y are (int) (dsize-1)/2. The latter 
    	// expression is exactly (dsize-1)/2 if dsize is odd, and is 
    	// ((dsize-1)-1)/2 is dsize is even. Therefore the required expression 
    	// is dret = (dsize+1)*(dsize+1)/8 + (dsize+1)/4; where dsize is replaced 
    	// by dsize-1 if dsize is even.  
	    
	    // handle the case when hsize is even 
	    // if (even_hsize==true) dsize = hsize-1;
	    // double dret = (dsize+1)*(dsize+1)/8 + (dsize+1)/4;
	    

	    return (int) dret;
}

    
    // construct the matrix phi_sym which is used later to get mu
    private void construct_phi_sym() {
    	int n = get_sym_classes_count();
    	phi_sym = new DoubleMatrix(n,n);
    	//phi_sym = new double[n][n]; 
    	for (int i=0;i<n;i++)
    		for (int j=0;j<n;j++)
    			phi_sym.put(i,j, get_sym_phi_entry(i,j));
    }
    
    // get the entry of sym_phi for two classes p_i, p_j
    private double get_sym_phi_entry(int p_i, int p_j) {
    	double sum=0;
    	Point i_rep_point, j_rep_point;
    	i_rep_point = get_sym_class_rep(p_i);
    	j_rep_point = get_sym_class_rep(p_j);
    	if (j_rep_point.x==0)     // j_rep_x = j_rep_y = 0
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
    				0, 0));
    	else if (j_rep_point.y==0) {  // j_rep_x>0 , j_rep_y=0
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    j_rep_point.x*cell_side, 0));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    -j_rep_point.x*cell_side, 0));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    0, j_rep_point.x*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    0, -j_rep_point.x*cell_side));
    	}
    	else if (j_rep_point.y==j_rep_point.x) { // j_rep_x>0 , j_rep_y=j_rep_x
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    j_rep_point.x*cell_side, j_rep_point.x*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    j_rep_point.x*cell_side, -j_rep_point.x*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    -j_rep_point.x*cell_side, j_rep_point.x*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    -j_rep_point.x*cell_side, -j_rep_point.x*cell_side));
    	}
    	else {        // j_rep_x>0 , 0< j_rep_y<j_rep_x
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    j_rep_point.x*cell_side, j_rep_point.y*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    j_rep_point.y*cell_side, j_rep_point.x*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    j_rep_point.x*cell_side, -j_rep_point.y*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    -j_rep_point.y*cell_side, j_rep_point.x*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    -j_rep_point.x*cell_side, j_rep_point.y*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    j_rep_point.y*cell_side, -j_rep_point.x*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    -j_rep_point.x*cell_side, -j_rep_point.y*cell_side));
    		sum = sum + Math.exp(-1*privObj.get_priv_distance(
    				i_rep_point.x*cell_side, i_rep_point.y*cell_side,
                    -j_rep_point.y*cell_side, -j_rep_point.x*cell_side));
    	}
    	return sum;
    }
    
    private void construct_mu_sym() {
    	int m = get_sym_classes_count();
    	//mu_sym = new double[m];

       // here we solve phi.mu=1 to obtain mu_sym
    	DoubleMatrix b = DoubleMatrix.ones(m);
    	mu_sym = Solve.solve(phi_sym, b);
    	
    	// check the resulting mu
    	if (mu_sym.min()<0) {
			System.out.println("TC-mechanism does not exist. vector entry is -ve.");
			System.exit(0);
		}

//    	for (int i=0;i< mu_sym.getRowDimension();i++)
//    		if (mu_sym.get(i,0)<0) {
//    			System.out.println("TC-mechanism does not exist. vector entry "+i+"is -ve.");
//    			System.exit(0);
//    		}
    }
    		

    // the coordinates of in and out points are in the original grid (relative to the corner) 		
    public double getProb(int p_in_x,int p_in_y,int p_out_x,int p_out_y) {
        
    	double ret;
    	double mid_x = (hsize-1)*0.5;
        double mid_y = mid_x;
        
        // get the coordinates of the input and output points relative to the center.
        double rel_in_x = p_in_x - mid_x;  double rel_in_y = p_in_y - mid_y;
        double rel_out_x = p_out_x - mid_x;  double rel_out_y = p_out_y - mid_y;
        
    	int class_ind = get_sym_class( new Point(rel_out_x,rel_out_y));
    	
    	// required probability is the product of the entry in mu that corresponds
    	// to the output point and the factor between output and input. 
    	ret = mu_sym.get(class_ind,0)*
    			Math.exp(-1*privObj.get_priv_distance(rel_in_x*cell_side, rel_in_y*cell_side, 
    					rel_out_x*cell_side, rel_out_y*cell_side));
    	return ret;
    }
    
// some methods for debugging    
    public int get_mu_size() {
    	return mu_sym.getLength();
    }
    
    public double get_mu_entry(int p_ind) {
    	return mu_sym.get(p_ind, 0);
    }
    
    public void write_phi_to_file(String p_file) {
    	int data_spacing = Settings.prob_spacing;
    	String phi_data ="";
    	BufferedWriter bufferedWriter = null;
        try {    
        	bufferedWriter = new BufferedWriter(new FileWriter(p_file));
            for (int class1=0;class1<get_sym_classes_count();class1++) {
            	for (int class2=0;class2<get_sym_classes_count();class2++) {
            		for (int k=0;k<data_spacing;k++) phi_data = phi_data+" ";
            		phi_data = phi_data + String.format("%01."+Settings.prop_decimal_places+"f",
     		               get_sym_phi_entry(class1,class2));
            	}
        		phi_data = phi_data + "\n"; // move to next line
        	}
//            bufferedWriter.write("epsilon = "+ eps+" per 1km, cell_side_in_km= "+ cell_side+"\n\n");
            bufferedWriter.write(phi_data);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
    
    
}
