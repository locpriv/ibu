package priv.plan.mechanisms;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import priv.plan.loss.Loss;
import priv.plan.priors.Prior;

/**
 *
 * @author javadb.com
 */
public class SMechanism extends Mechanism {
/*	  
 *  A scaled mechanism:
 *  A mechanism constructed by mapping the given mechanism (lr_mech) on a low-resolution 
 *  grid (lr_hsize, lr_vsize) of a region to the cells of another high-resolution grid 
 *  (hr_hsize, hr_vsize), i.e. with smaller cells, on the same region. 
 *  This mechanism runs the given mechanism on the low resolution grid then maps its output 
 *  to a small cell of the high-resolution grid using a prior (hr_prior) on it. 
 *  Notes: */	
	
	
	private Mechanism lr_mech;     // low resolution mechanism (with lr_hsize, lr_vsize, lr_cell_side)
	private double[][][][] scaling_remap; // ([lr_hsize][lr_vsize][hr_hsize][hr_vsize]) 
	                              // mapping from the low-resolution cells to high-resolution one.
	
	// private String[][] scaling_remap_det; 
	// a matrix (lr_hsize*lr_vsize) describing a deterministic remap from lo-res cells to hi-res cells. 
	// scaling_remap_det[x][y] stores the string x'y' representing the high-resolution cell x'y' to 
	// which the lo-res (x,y) is mapped. Constructed when the scaling is done based on a hi-res- prior
	
	private int scaling_ratio;   // scaling ratio from hi-res to lo-res (must be integer)
	
	private Prior hr_prior;      // high-resolution prior used to construct the hr_mechanism
	private Loss lossObj;        // necessary to optimize the remap
	
	double[][][][] hr_mechanism_matrix;  // ([hr_hsize][hr_vsize][hr_hsize][hr_vsize])
	//String remap_str="";    // describes the mapping from in_loc (in low-res grid) to out_loc (in high-res grid)
	double hr_exp_loss=0;    // the expected loss (with respect to hr_prior) of the direct mechanism (with remap) 
	
 	// constructs a high-res mechanism from a low-res mechanism p_mech and a remap p_remap. 
	public SMechanism(Mechanism p_lr_mech, double[][][][] p_scaling_remap) {
 		super(p_scaling_remap[0][0].length, p_scaling_remap[0][0][0].length,
 				p_lr_mech.cell_side/p_scaling_remap[0][0].length);
 		
 		if (hsize != p_scaling_remap.length || vsize != p_scaling_remap[0].length) {
 			System.out.println("unable to construct scaled machanism using a remap:");
 			System.out.println("resolution of the mechanism is incompatible with input resolution of the scaling remap");
 			System.exit(0);
 		}
 		double hs = (double) hsize/p_scaling_remap[0][0].length;
 		double vs = (double) vsize/p_scaling_remap[0][0][0].length;
 		
 		if (hs != vs) {
 			System.out.println("unable to construct scaled machanism using a remap:");
 			System.out.println("horizontal and vertical scaling ratios must be equal");
 			System.exit(0);
 		}
 			
 		if (hs != (int) hs) {
 			System.out.println("unable to construct scaled machanism using a remap:");
 			System.out.println("output resolution of the remap must be multiples of the resolution of the mechanism");
 			System.exit(0);
 		}
 		// end of validation

 		scaling_ratio = (int) hs;
 		lr_mech = p_lr_mech;
 		scaling_remap = p_scaling_remap;
 		
        //A header for the scaled mechanism
        String info = "// Scaled mechanism  \n"
                    + "// Size: hsize = "+ hsize +", vsize = "+ vsize;
        add_info(info);
 		compute_direct_mechanism_with_remap();
 	}
	
	// constructs a hi-res mechanism by mapping the outputs of p_lr_mech to the best hi-res cell
	// using the hi-res prior and the loss function.
	public SMechanism(Mechanism p_lr_mech, Prior p_hr_prior, Loss p_lossObj) {
 		super(p_hr_prior.hsize,p_hr_prior.vsize,p_hr_prior.cell_side);
 		lr_mech = p_lr_mech;
 		hr_prior = p_hr_prior;
 		lossObj = p_lossObj;
 		
 		double hs = (double) hsize/lr_mech.hsize;
 		double vs = (double) vsize/lr_mech.vsize;

 		if (hs != vs) {
 			System.out.println("unable to construct scaled machanism using a prior:");
 			System.out.println("horizontal and vertical scaling ratios must be equal");
 			System.exit(0);
 		}
 			
 		if (hs != (int) hs) {
 			System.out.println("unable to construct scaled machanism using a prior:");
 			System.out.println("the resolution of the prior must be multiples of the resolution of the mechanism");
 			System.exit(0);
 		}
 		// end of validation

 		scaling_ratio = (int) hs;
 		
        //A header for the scaled mechanism
        String info = "// Scaled mechanism  \n"
                    + "// Size: hsize = "+ hsize +", vsize = "+ vsize;
        add_info(info);
 		
 		compute_direct_mechanism_from_prior();
    	
// 		// writing the remap information
//        info = "// optimal remapping of the outputs of the low-res mechanism\n" 
//             + "//"+ remap_str+"\n\n"
//        // writing the expected loss before and after remap
//             + "// loss type: "+ lossObj.get_name() +"\n"
//        //     + "// expected loss of the original mechanism = "+ lr_exp_loss +"\n"
//             + "// expected loss for the high resolution prior = "+ hr_exp_loss;
//        add_info(info);

	}
	
  private void compute_direct_mechanism_with_remap() {
		hr_mechanism_matrix = new double[hsize][vsize][hsize][vsize];
		double sum;
		for (int lr_in_x=0; lr_in_x<lr_mech.hsize;lr_in_x++)  // for every low-res input cell of the original mechanism
			for (int lr_in_y=0; lr_in_y<lr_mech.vsize; lr_in_y++) 
		 		for (int hr_out_x=0; hr_out_x<hsize;hr_out_x++)  // for every hi-res out cell
		    		for (int hr_out_y=0; hr_out_y<vsize;hr_out_y++) {
		    			sum=0;
				 		for (int x=0; x<lr_mech.hsize;x++)   // loop over all low-res cells
				    		for (int y=0; y<lr_mech.vsize; y++) 
				    			sum = sum + lr_mech.getProb(lr_in_x, lr_in_y, x, y) * 
				    			   scaling_remap[x][y][hr_out_x][hr_out_y];
				 		// set all hi-res in the same input low-res cell to the same value
				 		for (int dx=0; dx<scaling_ratio;dx++)
					 		for (int dy=0; dy<scaling_ratio;dy++)
					 			hr_mechanism_matrix[lr_in_x*scaling_ratio+dx][lr_in_y*scaling_ratio+dy][hr_out_x][hr_out_y] = sum;
		    		}
  }
    

  // computes the direct mechanism and expected loss 
    // from the given one and the optimal remap
   private void compute_direct_mechanism_from_prior() {
	    double hr_out_loss;
	    double min_hr_out_loss;
    	double joint_prob;
    	int best_dx=0;int best_dy=0;

 		hr_mechanism_matrix = new double[hsize][vsize][hsize][vsize];
 		scaling_remap = new double[lr_mech.hsize][lr_mech.vsize][hsize][vsize];  // remaps from low-res grid to hi-res grid

 		for (int lr_out_x=0; lr_out_x<lr_mech.hsize;lr_out_x++)   // for every output of lr_mech on the low-res grid
    		for (int lr_out_y=0; lr_out_y<lr_mech.vsize;lr_out_y++) { 
    			// now see which high-res target cell in the same low-res cell (lr_out_x, lr_out_y) 
    			// can incur the minimum loss when it is reported instead of the enclosing lo-res cell.
    			// set the minimum loss for the out to a very large value.
    			min_hr_out_loss = lossObj.get_loss(0, 0, hsize*cell_side, vsize*cell_side)*10;
    			for (int dx=0; dx<scaling_ratio;dx++)
    				for (int dy=0; dy<scaling_ratio;dy++) {  // now every target hr cell in the same output lr cell
    					// compute the total loss for the current target
    		    			hr_out_loss = 0;
    		    			double inc_loss;
    		    			for (int hr_in_x=0; hr_in_x<hsize;hr_in_x++)
    		    				for (int hr_in_y=0; hr_in_y<vsize; hr_in_y++) {
    		    	    			joint_prob = hr_prior.getProb(hr_in_x,hr_in_y)*
    		   						     lr_mech.getProb(hr_in_x/scaling_ratio,hr_in_y/scaling_ratio, lr_out_x,lr_out_y);
    		    					inc_loss = lossObj.get_loss((hr_in_x+0.5)*cell_side, 
						                    (hr_in_y+0.5)*cell_side, 
						                    (lr_out_x*scaling_ratio+dx+0.5)*cell_side,
						                    (lr_out_y*scaling_ratio+dy+0.5)*cell_side);
    		    	    			hr_out_loss=hr_out_loss + joint_prob*inc_loss;
    		    				}
    		    			if (hr_out_loss<min_hr_out_loss) {  // has to be satisfied at least once
    		    				best_dx=dx; best_dy=dy;
    		    				min_hr_out_loss=hr_out_loss;
    		    			}
    					}
    			//  now add column (lr_out) in the low-res mechanism to column (lr+d) in the hi-res mech
    			//  in the direct mechanism and also add min_out_loss to expected loss
    			for (int hr_in_x=0; hr_in_x<hsize;hr_in_x++)
    				for (int hr_in_y=0; hr_in_y<vsize; hr_in_y++)
    					hr_mechanism_matrix[hr_in_x][hr_in_y][lr_out_x*scaling_ratio+best_dx][lr_out_y*scaling_ratio+best_dy]=
    							hr_mechanism_matrix[hr_in_x][hr_in_y][lr_out_x*scaling_ratio+best_dx][lr_out_y*scaling_ratio+best_dy] +
    							lr_mech.getProb(hr_in_x/scaling_ratio,hr_in_y/scaling_ratio,lr_out_x,lr_out_y);
    			hr_exp_loss = hr_exp_loss + min_hr_out_loss;
    			// adjust the scaling remap matrix
    			scaling_remap[lr_out_x][lr_out_y][lr_out_x*scaling_ratio+best_dx][lr_out_y*scaling_ratio+best_dy]=1;
    		}
    }
   
// writes the scaling remap to a file displaying for every lo-res cell (lr_x,lr_y) the 
// index of the internal hi-res cell (dx,dy) to which (lr_x,lr_y) is remapped. This is 
// performed only if the remap is deterministic.  
   public void write_scaling_remap_to_file(String p_scaling_remap_file) {
   	int data_spacing = Settings.prob_spacing;
   	String sc_remap_data ="";
    int x_digits =Settings.get_x_digits();   // the number of digits representing the x coordinate
	int y_digits =Settings.get_y_digits();   // the number of digits representing the y coordinate
   	BufferedWriter bufferedWriter = null;
       try {    
       	bufferedWriter = new BufferedWriter(new FileWriter(p_scaling_remap_file));
           for (int lr_y=0;lr_y<scaling_remap[0].length;lr_y++) {
           	for (int lr_x=0;lr_x<scaling_remap.length;lr_x++) {
           		for (int k=0;k<data_spacing;k++) sc_remap_data = sc_remap_data+" ";
           		// examine the hi-res cells inside (lr_x,lr_y) and check that only one element is 1 and
           		// get its relative index (dx,dy)
           		double prob;
           		double prob_sum=0;   // to check the remap is valid
           		for (int dx=0;dx<scaling_ratio;dx++)
           			for (int dy=0;dy<scaling_ratio;dy++) {
           				prob = scaling_remap[lr_x][lr_y][lr_x*scaling_ratio+dx][lr_y*scaling_ratio+dy];
           				prob_sum = prob_sum+prob;
           				if (prob==1)
           					sc_remap_data = sc_remap_data + String.format("%0"+x_digits+"d",dx)+
           			    						String.format("%0"+y_digits+"d",dy);
           				else if (0<prob && prob<1) {
           					System.out.println("unable to write the scaling remap: it is not deterministic");
           					System.exit(0);
           				}
           			}
           		if (prob_sum!=1) {
           			System.out.println("unable to write the scaling remap: is not valid");
           			System.exit(0);
           		}
           	}
       		sc_remap_data = sc_remap_data + "\n"; // move to next line
       	}
           bufferedWriter.write(sc_remap_data);
       } catch (IOException ex) {
           ex.printStackTrace();
       } finally {
           //Close the BufferedReader
           try {
               if (bufferedWriter != null) {
                   bufferedWriter.flush();
                   bufferedWriter.close();
               }
          } catch (IOException ex) {
               ex.printStackTrace();
           }
       }

   }
    
   
	public double getProb(int p_in_x, int p_in_y, int p_out_x, int p_out_y) {
 		return hr_mechanism_matrix[p_in_x][p_in_y][p_out_x][p_out_y];
 	}
    
}

