package priv.plan.mechanisms;

import org.jblas.DoubleMatrix;

/** A OneTimeBasicRappor mechanism on a grid of hsize*vsize. it is a mechanism from k to 2^k where k = hsize*vsize. 
 * The input location p (in linear order) is mapped to a k-bit vector, in which all bits are zero except the p-th 
 * bit set to 1. Then every bit is obfuscated independently by a 2-RR mechanism to yield a k-bits out_bitVector.
 * The parameter \eps describes the 2-RR mechanism that obfuscates every input bit. 
 * The mechanism is called one time because the obfuscation is done once (no longitudinal protection is done), called 
 * basic because it does not apply Bloom filters (every input corresponds to a unique one bit in the k-bit Vector B)
 * See [Erlingsson. et al 2014:RAPPOR]. 
 * The rows (inputs) of the mechanism matrix are ordered in linear order. 
 */
public class BasicOneTimeRappor {

    // basic mechanism attributes
    public int hsize,vsize;      // The grid for which the mechanism is designed
    public double cell_side;     // The length of the side of one square cell in the grid. 
    public double eps;
    public int k;

 // default constructor
    public BasicOneTimeRappor(int p_hsize, int p_vsize, double p_cell_side, double p_eps) {
    	hsize=p_hsize; 
    	vsize=p_vsize;
    	cell_side=p_cell_side;
    	eps = p_eps;
    	k = hsize*vsize;
    }  
    
 	
    /** 
     * get the probability of a k-size bitVector given an input location. The input location X is mapped to a bit vector bitVector(X)
     * which has the X-th bit set to 1 and other bits set to 0. Then the probability of an output bitVector Y is computed according 
     * to the bit values in Y.   
     * @param p_in_x
     * @param p_in_y
     * @param p_out_bitVector
     * @return
     */
    public double getProb(int p_in_x, int p_in_y, boolean[] p_out_bitVector) {
 		int in = p_in_y*hsize + p_in_x;
 		double prob;
 		// compute the probability due the in-th bit. Note that in-th bit in bitVector(in) is 1.
 		if (p_out_bitVector[in])
 			prob = Math.exp(0.5*eps)/(1+Math.exp(0.5*eps));
 		else prob = 1/(1+Math.exp(0.5*eps));
 		// compute the probabilities due other bits. Note that these bits in bitVector(in) ar 0.
 		for (int i= 0; i<in;i++)
 			if (p_out_bitVector[i])
 				prob = prob * 1/(1+Math.exp(0.5*eps));
 			else prob = prob * Math.exp(0.5*eps)/(1+Math.exp(0.5*eps));
 		for (int i= in+1; i<k;i++)
 			if (p_out_bitVector[i])
 				prob = prob * 1/(1+Math.exp(0.5*eps));
 			else prob = prob * Math.exp(0.5*eps)/(1+Math.exp(0.5*eps));
 		return prob;
 	}
    
/*    
    // returns the mechanism matrix: it is executed only if its subclass does not 
    // already have a mechanism matrix. If it has a matrix, the subclass should 
    // override this method
    public DoubleMatrix get_mech_matrix() {
    	DoubleMatrix mech_matrix = new DoubleMatrix(k, (int)Math.pow(2, k));

    	ui
    	for (int in_y=0; in_y<vsize;in_y++) 
			for (int in_x=0; in_x<hsize;in_x++)
           		for (int out_y=0;out_y<vsize;out_y++) 
           			for (int out_x=0;out_x<hsize;out_x++)
               			mech_matrix.put(in_y*hsize+in_x, out_y*hsize+out_x,getProb(in_x,in_y,out_x,out_y));
		return mech_matrix;
    }
*/
/*
    // returns a mechanism row distribution (probabilities) at p_in: 
    // it is executed only if its subclass does not 
    // already have a mechanism matrix. If the subclass does, it should 
    // override this method for performance. This method is used by EmpData class 
    // in the EM framework to generate obfuscated data. 
    // p_in is specified in linear order (y*hsize + x)
    public double[] get_mech_row_probabilities(int p_in) {
    	int in_y = p_in/hsize;
    	int in_x = p_in%hsize;
    	double[] row_probabilities = new double[hsize*vsize]; 
		for (int out_y=0; out_y<vsize;out_y++) 
			for (int out_x=0; out_x<hsize;out_x++)
				row_probabilities[out_y*hsize+out_x]= getProb(in_x,in_y,out_x,out_y);
		return row_probabilities;
    }
*/

    /** returns a mechanism column (p_outBitVector) as a column vector 
     * 
     * @param p_outBitVector
     * @return
     */
    public DoubleMatrix get_mech_column(boolean[] p_outBitVector) {
    	DoubleMatrix mech_col = new DoubleMatrix(k);
		for (int in_y=0; in_y<vsize;in_y++) 
			for (int in_x=0; in_x<hsize;in_x++)
               	mech_col.put(in_y*hsize+in_x,getProb(in_x,in_y,p_outBitVector));
		return mech_col;
    }

 	
 	
}
