package priv.plan.mechanisms;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.jblas.DoubleMatrix;


public class Remap {

    // basic attributes
    public int hsize,vsize;      // The grid for which the mechanism is designed
    private DoubleMatrix remap_mat;


 // default constructor
    public Remap(int p_hsize, int p_vsize, DoubleMatrix p_remap_matrix) {
    	if (!p_remap_matrix.isSquare()) {
    		System.out.println("invalid remap: the remap matrix is not square");
    		System.exit(0);
    	}
    	if (p_remap_matrix.length != p_hsize*p_vsize) {
    		System.out.println("invalid remap: the remap matix is incompatible with the specified dimensions");
    		System.exit(0);
    	}
    	hsize=p_hsize; 
    	vsize=p_vsize;
    	remap_mat = p_remap_matrix;
    }
    
    

 	// writes the remap matrix to a file. 
 	public void write_remap_to_file(String p_file) {
 		BufferedWriter bufferedWriter = null;
        try {    
            int data_spacing = Settings.prob_spacing;  // spaces between probabilities;
        	bufferedWriter = new BufferedWriter(new FileWriter(p_file));
        	for (int in_y=0; in_y<vsize;in_y++)
        		for (int in_x=0; in_x<hsize;in_x++) {  // for every input location
                   	for (int out_y=0;out_y<vsize;out_y++)
                   		for (int out_x=0;out_x<hsize;out_x++) {// for every output location
                   			bufferedWriter.write(String.format("%01."+Settings.prop_decimal_places+"f", 
                        			      getProb(in_x,in_y,out_x,out_y)));
                        	for (int k=0;k<data_spacing;k++) bufferedWriter.write(" ");
                   		}
                   	bufferedWriter.write("\n"); // move to a new row
        		}
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
        	try {
        		if (bufferedWriter != null) {
        			bufferedWriter.flush();
        			bufferedWriter.close();
        		}
        	} catch (IOException ex) {
        		ex.printStackTrace();
        	}
        }
 	}
 	
 	public DoubleMatrix get_remap_matrix() {
 		return remap_mat;
 	}
    
 	// get the probability of mapping (in_x, in_y) to (out_x,out_y)
 	public double getProb(int p_in_x, int p_in_y,int p_out_x,int p_out_y) {
 		return remap_mat.get(p_in_y*hsize+p_in_x, p_out_y*hsize+p_out_x);
 	}
 	
}
