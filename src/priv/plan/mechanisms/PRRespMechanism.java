package priv.plan.mechanisms;

import priv.plan.loss.Loss;
import priv.plan.privMetrics.PrivMetric;

public class PRRespMechanism extends Mechanism {
	
	private PrivMetric privObj;
    private double prob_diag;   // probability of yielding i given the secret is i  
    private double prob_non_diag;   // probability of yielding j given the secret is i, j noteq i  

    // Note that this mechanism is generic with respect to privacy metric
    // Therefore it accepts a privMetric parameter
    public PRRespMechanism(int p_hsize, int p_vsize, double p_cell_side, PrivMetric p_privObj) {
    	super(p_hsize,p_vsize,p_cell_side);
    	
    	privObj = p_privObj;
    	double t= Math.exp(p_privObj.eps * cell_side);
        prob_diag = t/(hsize*vsize-1+t);
        prob_non_diag = 1/(hsize*vsize-1+t);
    	
    	String info = "// Planar Randomised Response mechanism  \n"
    			+ "// Privacy type: "+ privObj.get_name() + "\n"
                + "// epsilon =" + privObj.eps +" for 1km \n"
                + "// Size: hsize = "+ hsize +", vsize = "+ vsize; 
    	this.add_info(info);
    }
    
 	// The following constructs a mechanism that satisfies a given loss value for the uniform prior 
    // (with respect to the given loss object). This method is used to compare mechanisms with respect 
    // to statistical utility (using em algorithm). Important: we assume that loss(i,i)=0.
    public PRRespMechanism(int p_hsize, int p_vsize, double p_cell_side, Loss p_loss, double p_exp_loss) {
 		super(p_hsize,p_vsize,p_cell_side);
 		int k = hsize*vsize;
 		prob_non_diag = (k*p_exp_loss)/p_loss.get_loss_matrix(hsize, vsize, cell_side).sum();
 		prob_diag = 1 - (k-1)*prob_non_diag;
 		if (prob_diag <= 0) {
 			System.out.println("Unable to construct RR mechanism: the given loss to too large");
 			System.exit(0);
 		}
 	}
 	public double getProb(int p_in_x, int p_in_y, int p_out_x, int p_out_y) {
 		if ((p_in_x==p_out_x)&&(p_in_y==p_out_y)) return prob_diag;
 		else return prob_non_diag;
 	}
 	
 	public double get_eps() {
 		return Math.log(prob_diag/prob_non_diag)/cell_side;
 	}

}
