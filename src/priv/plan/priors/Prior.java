package priv.plan.priors;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.Settings;

/** 
 * The prior describes a probability distribution over over a discrete points
 *  in the space. These points are specified by the grid that contains them 
 * (as cell centers) and the cell_side. This information fully describe these
 * points.
 */ 

public class Prior {
    private DoubleMatrix prior_vect;  // a matrix of one column
    public int hsize; 
    public int vsize;
    // the physical length in km for the side of one cell. Equivalently, the 
    // horizontal or the physical distance between the centers of two adjacent cells.  
    public double cell_side=0;
    private String prior_info ="";

    /** 
     * used to create a uniform prior with dimensions and  
     * physical distance represented by adjacent cells (locations). 
     */
    public Prior(int p_hsize, int p_vsize,double p_cell_side) {
    	hsize=p_hsize;
    	vsize=p_vsize;
    	cell_side = p_cell_side;
    	int k=hsize*vsize;
    	prior_vect = DoubleMatrix.ones(k).divi(k);
    }

    /** 
     * used to create a prior using a vector and planar dimensions  
     * physical distance represented by adjacent cells (locations). 
     */
    public Prior(DoubleMatrix p_vect, int p_hsize, int p_vsize,double p_cell_side) {
    	prior_vect = p_vect;
    	hsize=p_hsize;
    	vsize=p_vsize;
    	cell_side = p_cell_side;
    }

    
    /** 
     * used to create a prior object from an 2-dim array of x,y coordinates.
     * called from GDataset when priors are constructed
     * together with the cell_side (distance between adjacent cells). 
     * important: P(x,y) = p_prior_array[x][y]
     */
    public Prior(double[][] p_prior_array, double p_cell_side) {
    	hsize=p_prior_array.length;
    	vsize=p_prior_array[0].length;
    	prior_vect = new DoubleMatrix(hsize*vsize);
    	for (int x=0;x<hsize;x++)
    		for (int y=0;y<vsize;y++)
    			prior_vect.put(y*hsize+x, p_prior_array[x][y]);
    	cell_side = p_cell_side;
    }
    
    /** For creating a prior from a file. It should include the matrix and the cell size. 
     */
    public Prior(String p_file) {
    	
    	List<Double> prob_list = new ArrayList<Double>();
    	
    	BufferedReader bufferedReader = null;
    	String line;         // to hold a line string from the input file
        try {    
        	bufferedReader = new BufferedReader(new FileReader(p_file));
            int curr_y=0; int curr_x; int pos_in_line;
            String s;
            
            line=bufferedReader.readLine();
            while (line != null)
            	if (line.matches("") || line.startsWith("//"))
            		line=bufferedReader.readLine();   // skip the commented or empty line
            	else if (line.matches("cell_side_in_km=.*")) {
            			cell_side=Double.parseDouble(line.substring(16));  // read the cell side length
            			line=bufferedReader.readLine(); 
            			}
            	else {
            		pos_in_line=0;
            		curr_x=0;
            		while (pos_in_line<line.length()-1) {
            			s=line.substring(pos_in_line, pos_in_line+2);
            			if (s.matches("[0|1]\\.")) {
            				int s_end = pos_in_line+2+Settings.prop_decimal_places;
            				if (s_end>line.length()) s_end=line.length();
            				prob_list.add(Double.parseDouble(line.substring(pos_in_line, s_end)));
            				curr_x=curr_x+1;
            				}
            				pos_in_line= pos_in_line+1;
            			}
            			if (curr_x >0) {
            				curr_y=curr_y+1;   // increase the curr_y if we have we have 
            				// already found elements with coordinate curr_y 
            				hsize=curr_x;     // set the number of different x coordinates
            			}
            			// finished a row of probabilities
            			line=bufferedReader.readLine();
            		}
            vsize=curr_y;            // set the number of rows
            if (cell_side==0) {
            	System.out.println("cell side length is not specified in the prior file");
            	System.exit(0);
            }
            
            prior_vect = new DoubleMatrix(prob_list.size());
            for (int k=0;k<prob_list.size();k++)
            	prior_vect.put(k,0, prob_list.get(k));
            
        } catch (FileNotFoundException ex) {
           System.out.println("prior prior input file is not found"); 
    	   ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }

    /** 
     * Creates a prior object from a file having the probabilities in S order
     * i.e., the first probability in p_str corresponds to P(y=0,x=0)
     */
    public Prior(String p_file_name, int p_hsize, int p_vsize, double p_cell_side) {
    	prior_vect = new DoubleMatrix(p_hsize*p_vsize);
    	hsize=p_hsize;
    	vsize=p_vsize;
    	cell_side = p_cell_side;

    	String prior_str="";
    	BufferedReader bufferedReader = null;
    	try {    
    		bufferedReader = new BufferedReader(new FileReader(p_file_name));
    		prior_str=bufferedReader.readLine();
    	} catch (FileNotFoundException ex) {
    		System.out.println("prior prior input file is not found"); 
    		ex.printStackTrace();
    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		//Close the BufferedReader
    		try {
    			if (bufferedReader != null) {
    				bufferedReader.close();
    			}
    		} catch (IOException ex) {
    			ex.printStackTrace();
    		}
    	}
    	

    	int pos_in_str=0;  int pos_ahead=0; // start and end markers of a probability
    	int y=0; 
    	while (y<vsize) {
    		int x=0;
    		while (x<hsize) {
    			// find next probability in the string
    			while (pos_in_str<prior_str.length() && prior_str.charAt(pos_in_str)==' ')
    				pos_in_str = pos_in_str +1;  // advance the start marker until the start of word is found
    			if (pos_in_str>=prior_str.length()) {
    				System.out.println("Prior string is too short to find all probabilities");
    				System.exit(0);
    			}
    			pos_ahead=pos_in_str;            // set the end marker at the start of the word
    			while (pos_ahead<prior_str.length() && prior_str.charAt(pos_ahead)!=' ')
    				pos_ahead = pos_ahead+1; // advance the end marker until the space after the words is found
    			prior_vect.put(y*hsize+x,0, Double.parseDouble(prior_str.substring(pos_in_str, pos_ahead)));
    			pos_in_str=pos_ahead; // set the start marker at the first position after the word. 
    			//
    			x++;
    		}
    		y++;
    	}

    }

 	/** 
 	 * fill the prior matrix with random probabilities
 	 */
    public void fill_random_prior_matrix() {
    	double vrand; double sum=0;
    	for (int k=0; k<hsize*vsize; k++) {
    			vrand = Math.random();
    			prior_vect.put(k,0, vrand);
    			sum = sum + vrand;
    			}
    	// scale
    	for (int k=0; k<hsize*vsize; k++) {
    		double prob = prior_vect.get(k,0);
    		prior_vect.put(k,0, prob/sum);
    		}
    }
    
		
 	/** 
 	 * write the prior_matrix to the given file 
 	 * @param p_prior_file
 	 * @param p_with_info
 	 */
    // if p_with_info=true, the header information and the cell side length are written too.
    public void write_prior_to_file(String p_prior_file, boolean p_with_info) {

    	int data_spacing = Settings.prob_spacing;
    	String prior_data ="";
    	BufferedWriter bufferedWriter = null;
        try {    
        	bufferedWriter = new BufferedWriter(new FileWriter(p_prior_file));
            for (int y=0;y<vsize;y++) {
            	for (int x=0;x<hsize;x++) {
            		for (int s=0;s<data_spacing;s++) prior_data = prior_data+" ";
            		prior_data = prior_data + String.format("%01."+Settings.prop_decimal_places+"f",
     		               prior_vect.get(y*hsize+x,0));
            	}
        		prior_data = prior_data + "\n"; // move to next line
        	}
            if (p_with_info) {
            	bufferedWriter.write(prior_info+"\n");
            	bufferedWriter.write("#cell_side_in_km= "+ cell_side+"\n\n");
            }
            bufferedWriter.write(prior_data);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }

    /** 
     * returns the prior data in a single string format as required by Kostas
     * first row is written first, etc.
     */
    public String get_prior_as_string () {
    	int data_spacing = Settings.prob_spacing;
    	String prior_data ="";
    	for (int y=0;y<vsize;y++) 
    		for (int x=0;x<hsize;x++) {
        		for (int k=0;k<data_spacing;k++) prior_data = prior_data+" ";
        		prior_data = prior_data + String.format("%01."+Settings.prop_decimal_places+"f",
  		               prior_vect.get(y*hsize+x,0));
    		}
    	return prior_data.substring(data_spacing);  // remove the initial space;
    }
    
 	// write the prior as one vector to the given file to use Kostas program
    public void write_prior_vector_to_file(String p_prior_file) {

    	BufferedWriter bufferedWriter = null;
        try {    
        	bufferedWriter = new BufferedWriter(new FileWriter(p_prior_file));
        	int data_spacing = Settings.prob_spacing;
        	for (int y=0;y<vsize;y++) 
        		for (int x=0;x<hsize;x++) {
            		for (int k=0;k<data_spacing;k++) bufferedWriter.write(" ");
            		bufferedWriter.write(String.format("%01."+Settings.prop_decimal_places+"f",
      		               prior_vect.get(y*hsize+x,0)));
        		}
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            //Close the BufferedReader
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
           } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    
    /** 
     * get the probability of cell x,y
     * @param p_x
     * @param p_y
     * @return probability
     */
    public double getProb(int p_x, int p_y) {
    	return prior_vect.get(p_y*hsize+p_x,0);
    }

    /**
     * returns the column vector representation of the prior
     * @return
     */
    public DoubleMatrix get_prior_vector() {
  	   return prior_vect;
    }
 	
    
    public void add_info(String p_extra_info) {
 		prior_info = prior_info + "\n" + p_extra_info;
 	}
 	
 	public String get_info() {
 		return prior_info;
 	}



}
