package priv.plan.privMetrics;

import priv.plan.dist.Distance;

// This class specifies the necessary function (get_privacy_between) that computes 
// the privacy factor based on the coordinates of given cells.
public class ManPriv extends PrivMetric {

	public ManPriv(double p_eps) {
		super(p_eps);
	}

	// gets the privacy factor between points i=(i_x, i_y) and j=(j_x,j_y)
//	public double get_distance_between(double i_x, double i_y, double j_x, double j_y){
//        double dist = Math.abs(i_x-j_x)+ Math.abs(i_y-j_y);   
//		return dist;
//	}
	
	public double get_priv_distance(double i_x, double i_y, double j_x, double j_y) {
		return eps*Distance.get_manhat_dist(i_x, i_y, j_x, j_y);
	}
	
	public String get_name(){
		return PrivMetric.PRIV_MAN;
	}
}
