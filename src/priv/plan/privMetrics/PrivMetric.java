package priv.plan.privMetrics;

// This class specifies the necessary function (get_privacy_between) that computes 
// the privacy factor based on the coordinates of given cells.
public abstract class PrivMetric {
	
    public static final String PRIV_MAN = "PRIV_MAN";
    public static final String PRIV_MAX = "PRIV_MAX";
	public static final String PRIV_GEO = "PRIV_GEO";
	
	public double eps;  // the privacy per 1 km
	
	public PrivMetric(double p_eps) {
		eps = p_eps;
	}
	
//	public abstract double get_distance_between(double i_x, double i_y, double j_x, double j_y);

public abstract double get_priv_distance(double i_x, double i_y, double j_x, double j_y);


	// gets the privacy factor between points i=(i_x, i_y) and j=(j_x,j_y)
//	public double get_privacy_between(double i_x, double i_y, double j_x, double j_y) {
//        double dist = get_distance_between(i_x,i_y,j_x,j_y);
//		return (double) Math.exp(eps*dist);
//	}
	public abstract String get_name();
}
