package priv.plan.privMetrics;


// the distance (metric) between two points is the max coordinate difference
public class MaxPriv extends PrivMetric {

	public MaxPriv(double p_eps) {
		super(p_eps);
	}
	
	public double get_priv_distance(double i_x, double i_y, double j_x, double j_y) {
		return eps*Math.max(Math.abs(i_x-j_x), Math.abs(i_y-j_y));
	}
	
	public String get_name(){
		return PrivMetric.PRIV_MAX;
	}
}
