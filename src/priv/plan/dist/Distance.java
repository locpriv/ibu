package priv.plan.dist;

/**
 *
 * @author javadb.com
 */
public class Distance {
      // This class contains the necessary functions to compute the distances 
	  // between two locations
	
	
	public static double get_euclid_dist(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
	}
		
	public static double get_manhat_dist(double i_x, double i_y, double j_x, double j_y){
        return Math.abs(i_x-j_x)+ Math.abs(i_y-j_y);
    }
	    // returns the radial representation of an angle in degrees
	    private static double get_rad(double x) {
	    	  return (x*Math.PI/180);
	    	  }

        // returns the distance in km between to points p1, p2 on earth using their 
	    // geographic coordinates (lat/lng)
	    public static double get_distance_on_earth(double p1_lat, double p1_lng, double p2_lat, double p2_lng) {
	    	  double R = 6378.137;              // Earth mean radius in km
	    	  double dLat = get_rad(p2_lat - p1_lat);
	    	  double dLong = get_rad(p2_lng - p1_lng);
	    	  double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	    	    Math.cos(get_rad(p1_lat)) * Math.cos(get_rad(p2_lat)) *
	    	    Math.sin(dLong / 2) * Math.sin(dLong / 2);
	    	  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    	  double d = R * c;
	    	  return d; // returns the distance in km
	    	}
	    
	    // adds a south-to-north distance p_dy (in km) and return the new latitude
	    public static double adjust_latitude (double p_lat, double p_lng, double p_dy) {
	    	double R = 6378.137;              // Earth mean radius in km
			return p_lat + (p_dy/R)*(180/Math.PI);
	    }

	    // adds a west-to-east distance p_dx (in km) and return the new longitude
	    public static double adjust_longitude (double p_lat, double p_lng, double p_dx) {
	    	double R = 6378.137;              // Earth mean radius in km
			return p_lng + (p_dx/R)*(180/Math.PI)/Math.cos(p_lat*Math.PI/180);	
	    }
		

    
}

