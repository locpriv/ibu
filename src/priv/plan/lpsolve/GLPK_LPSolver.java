package priv.plan.lpsolve;

import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.GlpkException;
import org.gnu.glpk.SWIGTYPE_p_double;
import org.gnu.glpk.SWIGTYPE_p_int;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_smcp;
import org.jblas.DoubleMatrix;

import priv.plan.loss.Loss;
import priv.plan.mechanisms.Settings;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.PrivMetric;

/**
 *
 * @author javadb.com
 */
public class GLPK_LPSolver {
	// this program write the LP to find the optimal mechanism
	// for nodes arranged in a grid. The probability of reporting
	// node 00 when the input node is 01 is denoted by x0100. 
	static final double MAX_PRIV = 1e+4;
    
	private Prior prior;     // source file of the prior: contains also the cell_size in km
	private PrivMetric privObj;   // e.g. geo_privacy or man_privacy
	private Loss lossObj; 
	
	private int hsize;
	private int vsize;
	private double cell_side;
	
    glp_prob mylp;
    
    public double opt_exp_loss;
	
	public GLPK_LPSolver(Prior p_prior, PrivMetric p_privObj, Loss p_lossObj) {
        // validate the prior
		if (p_prior.hsize == 0 || p_prior.vsize ==0 || p_prior.cell_side==0) {
        	System.out.println("error: empty or invalid prior");
        	System.exit(0);
        	}
        
		prior = p_prior;
		privObj = p_privObj;
		lossObj = p_lossObj;
		hsize = p_prior.hsize;
		vsize = p_prior.vsize;
		cell_side = p_prior.cell_side;
	}
	
	public void construct_LP() {

		try {
			// Create LP problem
			mylp = GLPK.glp_create_prob();
			GLPK.glp_create_index(mylp);

			//            System.out.println("Problem created");
			GLPK.glp_set_prob_name(mylp, "optMech");

			// Define columns (variables)
			GLPK.glp_add_cols(mylp, hsize*hsize*vsize*vsize);

			System.out.println("No. of variables = "+ GLPK.glp_get_num_cols(mylp));

			// The following are to specify names for the variables
			int x_digits =Settings.get_x_digits();   // the number of digits representing the x coordinate
			int y_digits =Settings.get_y_digits();   // the number of digits representing the y coordinate

			// Define objective
			System.out.println("adding the expected loss");
			GLPK.glp_set_obj_name(mylp, "eloss");
			GLPK.glp_set_obj_dir(mylp, GLPKConstants.GLP_MIN);

			int var_ind_obj =0;
			String var_name_obj ="";
			double loss;   // to hold the loss associated to a prior element and a mechanism entry
			double prob;   // to hold the double value of a prior element
			for (int i_x=0; i_x<hsize; i_x++) 
				for (int i_y=0; i_y<vsize; i_y++)  
					for (int k_x=0; k_x<hsize; k_x++)
						for (int k_y=0; k_y<vsize; k_y++) {
							prob = prior.getProb(i_x,i_y);  

							//  add a term for utility
							var_ind_obj = var_ind_obj +1;
							var_name_obj = "x"+String.format("%0"+x_digits+"d",i_x)+ 
									String.format("%0"+y_digits+"d",i_y)+ 
									"_"+ String.format("%0"+x_digits+"d",k_x)+
									String.format("%0"+y_digits+"d",k_y);
							//                      System.out.println(var_ind + "     "+ var_name);
							GLPK.glp_set_col_name(mylp, var_ind_obj, var_name_obj);
							GLPK.glp_set_col_kind(mylp, var_ind_obj, GLPKConstants.GLP_CV);
							GLPK.glp_set_col_bnds(mylp, var_ind_obj, GLPKConstants.GLP_DB, 0, 1);

							//                     System.out.println(GLPK.glp_get_col_name(lp, var_ind)+"         "+ var_ind);

							loss = lossObj.get_loss(i_x*cell_side,i_y*cell_side,
									             k_x*cell_side,k_y*cell_side);
							GLPK.glp_set_obj_coef(mylp, var_ind_obj, prob*loss);
						}
			/*         
          // for debug: printing objective  
          String objStr = GLPK.glp_get_obj_name(lp)+ "= ";
      	  for (int i=1; i<= GLPK.glp_get_num_cols(lp);i++) {
      		  objStr = objStr + GLPK.glp_get_obj_coef(lp,i)+"*"+GLPK.glp_get_col_name(lp, i)+" + ";
      	  }
      	  System.out.println(objStr);
			 */            

			System.out.println("adding inequalities");

			// first array include the ids of the variables involved in the constraint
			// second array include the coefficients of these variables in the constraint
			// the elements of these arrays will be added at positions 1,2,...
			// the 0th position should not be used, but has to be in the array
			// so if we will have 2 terms in the constraint (row) we have to create
			// the array of size 3 (pos 0,1,2)
			SWIGTYPE_p_int ind_con = GLPK.new_intArray(3);
			SWIGTYPE_p_double val_con = GLPK.new_doubleArray(3);

			/*
            // for debug:
            SWIGTYPE_p_int ret_ind_con=GLPK.new_intArray(4); // big number
	        SWIGTYPE_p_double ret_val_con=GLPK.new_doubleArray(4);
			 */	        


			// constructing the privacy constraints
			String left_var_name; 
			int left_var_ind;
			String right_var_name; 
			int right_var_ind;

			for (int i_x=0;i_x<hsize;i_x++)  // for every input node
				for (int i_y=0;i_y<vsize;i_y++)
					for (int j_x=0;j_x<hsize;j_x++)  // another input node
						for (int j_y=0;j_y<vsize;j_y++) 
							for (int k_x=0;k_x<hsize;k_x++)   //for every output node
								for (int k_y=0;k_y<vsize;k_y++)
									if (!(j_x == i_x && j_y == i_y))
										if ((privObj.get_name() == PrivMetric.PRIV_MAN && privObj.get_priv_distance(i_x*cell_side,i_y*cell_side,j_x*cell_side,j_y*cell_side)==privObj.eps*cell_side) 
												|| (privObj.get_name()== PrivMetric.PRIV_GEO)){
											// the above condition in the case of manhattan privacy 
											// sets the constraints for only adjacent nodes. 
											// The remaining constraints are induced.
											double priv_factor = Math.exp(privObj.get_priv_distance(i_x*cell_side, i_y*cell_side, 
													               j_x*cell_side, j_y*cell_side)); 
											if (priv_factor < MAX_PRIV)  {  // priv_factor = MAX_PRIV;  
											left_var_name = "x" + 
													String.format("%0"+x_digits+"d",i_x)+
													String.format("%0"+y_digits+"d",i_y)+ 
													"_" + String.format("%0"+x_digits+"d",k_x)+
													String.format("%0"+y_digits+"d",k_y);
											left_var_ind = GLPK.glp_find_col(mylp, left_var_name);

											right_var_name = "x" +
													String.format("%0"+x_digits+"d",j_x)+
													String.format("%0"+y_digits+"d",j_y)+
													"_" + String.format("%0"+x_digits+"d",k_x)+
													String.format("%0"+y_digits+"d",k_y);
											right_var_ind = GLPK.glp_find_col(mylp, right_var_name);

											int row_ind = GLPK.glp_add_rows(mylp, 1);  // add new constraint;
											//System.out.println("row index "+ row_ind);

											// the expression in the constraint is bounded from above by 0 
											GLPK.glp_set_row_bnds(mylp, row_ind, GLPKConstants.GLP_UP, 0, 0);
											// add the terms on in the constraints at positions 1,2,...
											GLPK.intArray_setitem(ind_con, 1, left_var_ind);      // 1st variable
											GLPK.doubleArray_setitem(val_con, 1, 1);              // its coefficient
											GLPK.intArray_setitem(ind_con, 2, right_var_ind);     // 2nd variable
											GLPK.doubleArray_setitem(val_con, 2, -1*priv_factor); // coefficient
											// add the constraint to the problem
											// the 3rd argument is the number of non-zero terms in the expression
											GLPK.glp_set_mat_row(mylp, row_ind, 2, ind_con, val_con);  

											/* 
            								// Debug: print constraint infor
            								int len = GLPK.glp_get_mat_row(mylp, row_ind, ret_ind_con, ret_val_con);
            								String conStr ="";
            								for (int i=1; i<=len; i++)
            								conStr = conStr + GLPK.doubleArray_getitem(ret_val_con, i) +"*"+
            								GLPK.glp_get_col_name(mylp, GLPK.intArray_getitem(ret_ind_con, i))+"+";
            								System.out.println(conStr);
											 */
											}
										}
			// delete the couple of arrays used for every constraint above.
			GLPK.delete_intArray(ind_con);
			GLPK.delete_doubleArray(val_con);
			/*           
            GLPK.delete_intArray(ret_ind_con);
            GLPK.delete_doubleArray(ret_val_con);

			 */

			// constructing equalities constraints (have to include the 0th position) 
			SWIGTYPE_p_int ind_eq = GLPK.new_intArray(hsize*vsize+1);  // an element for every output
			SWIGTYPE_p_double val_eq = GLPK.new_doubleArray(hsize*vsize+1);

			/*            
            // for debug:
            SWIGTYPE_p_int ret_ind_eq = GLPK.new_intArray(hsize*vsize+1);  // an element for every output
            SWIGTYPE_p_double ret_val_eq = GLPK.new_doubleArray(hsize*vsize+1);
			 */            

			int var_ind_eq;
			String var_name_eq;
			System.out.println("adding equalities");
			for (int i_x=0;i_x<hsize;i_x++)
				for (int i_y=0;i_y<vsize;i_y++) {      // for every input
					int row_ind = GLPK.glp_add_rows(mylp, 1);  // add new constraint;
					GLPK.glp_set_row_bnds(mylp, row_ind, GLPKConstants.GLP_FX, 1, 1); // specify it is equality
					int constarint_term =0;
					for (int k_x=0;k_x<hsize;k_x++)
						for (int k_y=0;k_y<vsize;k_y++) {  // for every output
							var_name_eq = "x"+
									String.format("%0"+x_digits+"d",i_x)+
									String.format("%0"+y_digits+"d",i_y)+
									"_"+ String.format("%0"+x_digits+"d",k_x)+
									String.format("%0"+y_digits+"d",k_y);
							var_ind_eq = GLPK.glp_find_col(mylp, var_name_eq);
							constarint_term = constarint_term +1;
							GLPK.intArray_setitem(ind_eq, constarint_term, var_ind_eq);   // variables
							GLPK.doubleArray_setitem(val_eq, constarint_term, 1);      // coefficients 
						}
					/*
            	 // debug: ensure that constraint int array include all the required variables
            	 // before adding it to the matrix
            	 double var_coef_eq;
            	 for (int j=1; j<=hsize*vsize;j++) {
            		 var_ind_eq=GLPK.intArray_getitem(ind_eq, j);
            		 var_name_eq=GLPK.glp_get_col_name(mylp, var_ind_eq);
            		 var_coef_eq = GLPK.doubleArray_getitem(val_eq, j);
            		 System.out.println("index: "+ var_ind_eq + "  name: "+ var_name_eq+" coef: "+var_coef_eq);
            	 }
					 */
					GLPK.glp_set_mat_row(mylp, row_ind, hsize*vsize, ind_eq, val_eq);  // set the values of the new constraint

					/*	
            	 // print constraint info
					int len = GLPK.glp_get_mat_row(lp, row_ind, ret_ind_eq, ret_val_eq);
					String conStr ="";
					for (int i=1; i<=len; i++)
					conStr = conStr + GLPK.doubleArray_getitem(ret_val_eq, i) +"*"+
					GLPK.glp_get_col_name(lp, GLPK.intArray_getitem(ret_ind_eq, i))+"+";
					System.out.println(conStr);
					 */


				}            
			//System.out.println("finished writing equalities");
			// delete the couple of arrays used for every constraint above.
			GLPK.delete_intArray(ind_eq);
			GLPK.delete_doubleArray(val_eq);
			/*
            GLPK.delete_intArray(ret_ind_eq);
            GLPK.delete_doubleArray(ret_val_eq);
			 */            

		} catch (GlpkException ex) {
			ex.printStackTrace();
		}
	}
	
	public void write_LP(String p_LP_file) {
		try {
			GLPK.glp_write_lp(mylp, null, p_LP_file);
		} catch (GlpkException ex) {
			ex.printStackTrace();
		}
	}
	
	
	// solves the linear program and return 0 if a solution is found
	public int solve() {
		// Solve model
		System.out.println("Solving..");
		int ret=1;
		try {
			glp_smcp parm = new glp_smcp();
			GLPK.glp_init_smcp(parm);
			
//			GLPKJNI.glp_init_smcp(GLPK.GLP_DUALP, parm);
			GLPK.glp_scale_prob(mylp, GLPK.GLP_SF_AUTO |GLPK.GLP_SF_SKIP);
			ret = GLPK.glp_simplex(mylp, parm);
			GLPK.glp_unscale_prob(mylp);
		} catch (GlpkException ex) {
			ex.printStackTrace();
		}
		
		if (ret==0) 
			opt_exp_loss=GLPK.glp_get_obj_val(mylp);
		else
			System.out.println("The problem could not be solved");

		return ret;
	}
	
	
	public void remove_LP() {
		try {
			// Free memory
			GLPK.glp_delete_prob(mylp);
			} catch (GlpkException ex) {
            ex.printStackTrace();
            }
    }
	
	// write the solution (optimal mechanism) to the specified file 
	public DoubleMatrix get_opt_mech_matrix() {
		DoubleMatrix mech_matrix = new DoubleMatrix(hsize*vsize,hsize*vsize); 
		int x_digits =Settings.get_x_digits();   // the number of digits representing the x coordinate
		int y_digits =Settings.get_y_digits();   // the number of digits representing the y coordinate

		String var_name;       // LP variable name
		int var_ind;           // LP variable index
		double prob;
		for (int i_x=0; i_x<hsize;i_x++)
			for (int i_y=0; i_y<vsize;i_y++) { // for every input
				String currInput=String.format("%0"+x_digits+"d",i_x)+ String.format("%0"+y_digits+"d",i_y);
				for (int k_x=0; k_x<hsize;k_x++)
					for (int k_y=0; k_y<vsize;k_y++) { // for every output
						var_name = "x"+ currInput +
								"_"+ String.format("%0"+x_digits+"d",k_x)+
								String.format("%0"+y_digits+"d",k_y);
						var_ind = GLPK.glp_find_col(mylp, var_name);
						prob = GLPK.glp_get_col_prim(mylp, var_ind);
						mech_matrix.put(i_y*hsize+i_x, k_y*hsize+k_x, prob);
					}
			}
		
		return mech_matrix;
	}
    
}

