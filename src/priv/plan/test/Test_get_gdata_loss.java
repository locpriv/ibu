package priv.plan.test;

import priv.plan.gdset.GDataset;
import priv.plan.loss.*;
import priv.plan.mechanisms.AnnotatedFileMechanism;
import priv.plan.mechanisms.GMechanism;

public class Test_get_gdata_loss {

	public static void main(String[] args) {
		// South: 34.0225,   North: 34.0944
		// West:  -118.3004,    East:  -118.2136

		double south = 34.0225; 
		double west = -118.3004;
		double north = 34.0944;
		double east = -118.2136;
		
		String gdata_file = "los_angeles_data_64.txt";
		GDataset ds = new GDataset(gdata_file,south,north,west,east,1,".*");
		String mechanism_file = "grid_privType_GEO_priv3.0_LossLIN_GEO_los_angeles_prior.lp.m";
		double sat_dist = ds.get_max_distance();
		LinGeoLoss lossObj = new LinGeoLoss(sat_dist,sat_dist);
		AnnotatedFileMechanism fm = new AnnotatedFileMechanism(mechanism_file);
		GMechanism gm = new GMechanism(fm, south,west);
		double loss = gm.get_exp_losses_for_gdata_points(ds, lossObj,1).get(1);
        System.out.println("average loss = "+ loss + "km");
        
/*        
		gdata_file = "london_user_552_data.txt";
		loss = gm.get_mechanism_exp_loss_for_gdata(gdata_file, Settings.LOSS_LIN_GEO);
        System.out.println("for "+gdata_file+ ", average loss = "+ loss + "km");
        
        
		gdata_file = "london_user_661_data.txt";
		loss = gm.get_mechanism_exp_loss_for_gdata(gdata_file, Settings.LOSS_LIN_GEO);
        System.out.println("for "+gdata_file+ ", average loss = "+ loss + "km");
        
        
		gdata_file = "london_user_756_data.txt";
		loss = gm.get_mechanism_exp_loss_for_gdata(gdata_file, Settings.LOSS_LIN_GEO);
        System.out.println("for "+gdata_file+ ", average loss = "+ loss + "km");
        
        
		gdata_file = "london_user_984_data.txt";
		loss = gm.get_mechanism_exp_loss_for_gdata(gdata_file, Settings.LOSS_LIN_GEO);
        System.out.println("for "+gdata_file+ ", average loss = "+ loss + "km");
*/        
	}

}
