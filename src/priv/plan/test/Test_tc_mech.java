package priv.plan.test;

import priv.plan.mechanisms.*;
import priv.plan.privMetrics.GeoPriv;

public class Test_tc_mech {
	
	//static int vsize =80; 
	static double cell_side =1;
	static GeoPriv privObj= new GeoPriv(Math.log(2));
	static double time_no_sym,time_sym,mem_no_sym,mem_sym;

	public static void main(String[] args) {
		
		// This program compares between two methods of constructing the tc-mechanism.
		
		// The first method in the following loads the necessary libraries for matrix 
		// manipulation and others, which is about 75.2KB. This overhead is included only 
		// in the memory consumption of the first method. The second method does not need
		// to load this library again, hence its indicated consumption is its real memory 
		// consumption, i.e. without any overheads. 
		
		// Therefore the actual memory consumption for the first is:  the indicated - 75.2KB
		// and the actual memory consumption of the second is: the indicated
		
		// Note also the memory consumption is approximately the size of phi + size of mu
		// thus, [(mu_size)^2+(mu_size)]*sizeof(double). The size of double = 8 bytes.
		
		// We observe that when we do not use the loop, the memory consumptions are precise. 
		
		//String time_data_no_sym=""; String time_data_sym="";
		String mem_data_no_sym=""; String mem_data_sym="";
		
		Test_tc_mech.create_tc_mechanism_no_sym(1);

		for (int k=29;k<=29;k=k+2) {
			System.out.println("hsize = "+ k);
		create_tc_mechanism_no_sym(k);
//		create_tc_mechanism_sym(k);
//		time_data_no_sym = time_data_no_sym + "["+ k +","+ time_no_sym+"],";
//		time_data_sym = time_data_sym + "["+ k +","+ time_sym+"],";
		mem_data_no_sym = mem_data_no_sym + "["+ k +","+ mem_no_sym+"],";
		mem_data_sym = mem_data_sym + "["+ k +","+ mem_sym+"],";
		}
		
//		System.out.println("\n time consumption with no symmetries");
//		System.out.println(time_data_no_sym);
//		System.out.println("\n time consumption using symmetries");
//		System.out.println(time_data_sym);

		System.out.println("\n memory consumption with no symmetries");
		System.out.println(mem_data_no_sym);
		System.out.println("\n memory consumption using symmetries");
		System.out.println(mem_data_sym);
/******** comparison with the truncated geometric mechanism *******/		
		//TPGeomMechanism mg = new TPGeomMechanism(vsize,vsize,cell_side,eps);
		//mg.write_diagonal_to_file("diagonal_geom.txt");

/******** check the validity of the mechanism  ********/
/*
		double sum;
		for (int in_x=0;in_x<m.hsize;in_x++)
			for (int in_y=0;in_y<m.vsize;in_y++)
			{
				sum =0;
				for (int out_x=0; out_x<m.hsize;out_x++)
					for (int out_y=0;out_y<m.vsize;out_y++)
						sum = sum +m.getProb(in_x,in_x,out_x,out_y);
				System.out.println("total probability for ("+in_x+","+in_y +")= "+ sum);
			}
*/
	}
	
		
	private static void create_tc_mechanism_no_sym(int p_size) {
		long starttime, endtime, start_used_mem,end_used_mem;
		Runtime runtime = Runtime.getRuntime();
		
		//*********** tc-mechansim1 consider symmetries on rectangle********		
		System.out.println("");

		System.out.println("Considering a tc-mechanism without rectangle symmetries");
		System.gc();
		start_used_mem = runtime.totalMemory()-runtime.freeMemory();
		starttime = System.nanoTime();
		TCRectMechanism mt1 = new TCRectMechanism(p_size,p_size,cell_side,privObj);
		endtime = System.nanoTime();
		System.gc();
		end_used_mem = runtime.totalMemory()-runtime.freeMemory();
		
		time_no_sym = (endtime - starttime)*1e-9;  // in seconds
		mem_no_sym = (end_used_mem- start_used_mem)*Math.pow(2, -10); // in KBytes

		//double lib_size = 75.2;
		
		int mu_size=mt1.get_mu_size();  
		
		//System.out.println("size of the vector = "+ mu_size);
		System.out.println("Time elapsed = "+ time_no_sym + " seconds" );
		System.out.println("Estimated memory usage = "+ (mu_size*mu_size+mu_size)*8* Math.pow(2, -10));
		System.out.println("Memory used = " + mem_no_sym + " Kbytes");

//		mt1.write_diagonal_to_file("diagonal1_tc.txt");
//		mt1.write_mechanism_to_file("mech1.m");
		System.gc();
	}
	
	private static void create_tc_mechanism_sym(int p_size) {
		long starttime, endtime, start_used_mem,end_used_mem;
		Runtime runtime = Runtime.getRuntime();
		// tc-mechanism using symmetries 		
		System.out.println("");
		System.out.println("Considering a tc-mechanism using symmetries");
		System.gc();
		start_used_mem = runtime.totalMemory()-runtime.freeMemory();
		starttime = System.nanoTime();
		TCSqMechanism mt2 = new TCSqMechanism(p_size,cell_side,privObj);
         // m.write_mechanism_to_file("tc_mech_test.m");
		// mu vector
		endtime = System.nanoTime();
		System.gc();
		end_used_mem = runtime.totalMemory()-runtime.freeMemory();
		
		time_sym = (endtime - starttime)*1e-9;  // in seconds
		mem_sym = (end_used_mem- start_used_mem)*Math.pow(2, -10); // in KBytes

		//double lib_size = 75.2;
		int mu_size=mt2.get_mu_size();
		//System.out.println("size of the vector = "+ mu_size);
		System.out.println("Time elapsed = "+ time_sym + " seconds" );
		System.out.println("Estimated memory usage = "+ (mu_size*mu_size+mu_size)*8* Math.pow(2, -10));
		System.out.println("Actual Memory used = " + mem_sym+ " Kbytes");

		boolean v=true; int neg_i=-1;
		for (int i=0;i<mt2.get_mu_size();i++) {
			//System.out.println(mt.get_mu_entry(i)+"   ");
			if (mt2.get_mu_entry(i)<0) {
				v=false; neg_i=i;
			}
		}
		if (v==false) System.out.println("entry "+neg_i+" is negative: "+ mt2.get_mu_entry(neg_i));
        System.out.println(" ");	
		if (v) System.out.println("mechanism exists");
		else System.out.println("mechanism does not exist");
		
//		mt2.write_diagonal_to_file("diagonal2_tc.txt");
//		mt2.write_mechanism_to_file("mech2.m");
//		mt.write_phi_to_file("phi.txt");
		System.gc();
	}


}
