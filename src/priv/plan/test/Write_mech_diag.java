package priv.plan.test;

import priv.plan.mechanisms.*;

public class Write_mech_diag {
	
	// Tests constructing a mechanism from a file, write its diagonal to another file
	public static void main(String[] args) {
		
     AnnotatedFileMechanism m = new AnnotatedFileMechanism("gw_opt_san_francisco_6_14_2km_1.7.mech");
     m.write_diagonal_to_file("opt_17.txt");
     
/*     
     LinGeoLoss lossObj = new LinGeoLoss(10,10);
     Prior p = new Prior("test_prior1.pr");
     double loss = m.get_expected_loss_for_prior(p, lossObj);
     System.out.println(loss);
     */
	}

}
