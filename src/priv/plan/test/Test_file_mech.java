package priv.plan.test;

import priv.plan.loss.*;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;

public class Test_file_mech {
	
	// Tests constructing a mechanism from a file, and computes its expected loss
	// with respect to a given prior and a loss function

	public static void main(String[] args) {
		//double prob=0;
		//Prior p = new Prior("manhattan_prior.pr");
		//for (int i=0; i<p.hsize; i++)
		//	for (int j=0; j< p.vsize;j++) 
		//		prob = prob + p.prior_matrix[i][j];
		//System.out.println(prob);
		GeoPriv privObj = new GeoPriv(Math.log(2));
		Prior p = new Prior("test_prior1.pr");
		TPGeomMechanism gm = new TPGeomMechanism(p.hsize,p.vsize,p.cell_side,privObj.eps);
	    gm.write_mechanism_to_file("gmech.mech");
		
     AnnotatedFileMechanism m1 = new AnnotatedFileMechanism("gmech.mech");
     m1.write_mechanism_matrix_to_file("geom_matrix.mech");
     MatrixFileMechanism m2 = new MatrixFileMechanism("geom_matrix.mech", m1.hsize,m1.vsize,m1.cell_side);
     
     m2.write_mechanism_to_file("geom_mechanism_copy.mech");
     
     
/*     
     LinGeoLoss lossObj = new LinGeoLoss(10,10);
     Prior p = new Prior("test_prior1.pr");
     double loss = m.get_expected_loss_for_prior(p, lossObj);
     System.out.println(loss);
     */
	}

}
