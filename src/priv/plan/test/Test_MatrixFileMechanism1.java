package priv.plan.test;

import priv.plan.loss.*;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;

public class Test_MatrixFileMechanism1 {
	
	// construct a prior 
	// constructs a mechanism from a file in kostas format
	// i.e rows and columns are in S order
	// remap the mechanism. 
	
	// Tests constructing a mechanism from a file, and computes its expected loss
	// with respect to a given prior and a loss function

	public static void main(String[] args) {
		Prior p = new Prior(4,4,1);
     
	GeoPriv privObj = 	new GeoPriv(Math.log(1.4)/0.1); 
	TCRectMechanism tc = new TCRectMechanism(p.hsize,p.vsize,p.cell_side, privObj);
	 //KostasMechanism m = new KostasMechanism("tlap_cells_8400_1.4.mech",p.hsize,p.vsize,p.cell_side);
     BinLoss lossObj = new BinLoss(0,1);
     
     RMechanism mr = new RMechanism(tc,p,lossObj);
     // i.e. processing every 10 locations in one thread.
     
     //double loss = m.get_expected_loss_for_prior(p, lossObj);
     //System.out.println(loss);
	}

}
