package priv.plan.test;

import java.time.LocalDateTime;

import priv.plan.gdset.GDataset;
import priv.plan.gdset.GSettings;
import priv.plan.loss.LinGeoLoss;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;

public class Test_loss_old_vs_new {
	
	
	// Construct a prior on 8400 cells, a tgeom mechanism, and 
	// then evaluate the loss of this mechanism using old and new 
	// methods to compare the performace

	//  A region in San Francisco (12km*28km) agreed with Kostas
	//South: 37.5395 North: 37.7910
	//West: -122.5153 East : -122.3789
	//north-to-south: 28.000 km 
	//east-to-west: 12.001 km 
	//Grid: hsize = 6 , vsize = 14 
	//cell side :2 km		
	static double south= 37.5395;
	static double north= 37.7910;
	static double west = -122.5153;
	static double east = -122.3789;
	
	static double cell_side_h = 0.2;  // making a grid of 60*140 cells

	
	static GDataset ds;
//	static Prior gprior;

	public static void main(String[] args) {

		// get priors at different resolutions
		System.out.println("Constructing dataset...");
		ds= new GDataset(GSettings.data_file, south, north, west, east, cell_side_h, ".*");

//		System.out.println("constructing loss file");
		double saturation_dist = ds.get_max_distance();  
		LinGeoLoss lossObj;
		lossObj = new LinGeoLoss(saturation_dist, saturation_dist);
//		lossObj.write_kostas_loss_to_file1("loss_8400.los", 60, 140, cell_side_h);

		
		// construct high-res prior on 8400 cells
		System.out.println("constructing prior");
		Prior gprior_h = ds.get_prior(cell_side_h);
		//System.out.println("constructing prior file");
		//gprior_h.write_prior_vector_to_file("gprior_8400.pr");

		// use l = ln(t), t = 1.4 .. 2.9  // eps = l/0.1 
        // System.out.println("constructing mechanisms..");
		double t =1.4;
		
		GeoPriv privObj = new GeoPriv(Math.log(t)/0.1);  // when multiplied by cell_side 0.2, we get reasonable l
		String l_str = String.format("%02."+1+"f", Math.exp(privObj.eps*0.1)); //just to verify
		
//		int grid_size_h = (int) ((12/cell_side_h)*(28/cell_side_h));
		
//		System.out.println("constructing lab for" + l_str);
//		TPLapMechanism4 tlap_mech;
//		String tlap_file_name = "tlap_cells_"+grid_size_h+"_"+l_str+".mech";
//		tlap_mech = new TPLapMechanism4((int)(12/cell_side_h),(int)(28/cell_side_h),cell_side_h,privObj.eps);
//		tlap_mech.write_mechanism_matrix_to_file(tlap_file_name);

//		System.out.println("constructing tgeom for" + l_str);
//		TPGeomMechanism tgeom_mech;
//		String tgeom_file_name = "tgeom_cells_"+grid_size_h+"_"+l_str+".mech";
//		tgeom_mech = new TPGeomMechanism((int)(12/cell_side_h),(int)(28/cell_side_h),cell_side_h,privObj.eps);
//		tgeom_mech.write_mechanism_matrix_to_file(tgeom_file_name);

		System.out.println("constructing tc for" + l_str);
		TCRectMechanism tc_mech;
//		String tc_file_name = "tc_cells_"+grid_size_h+"_"+l_str+".mech";
		tc_mech = new TCRectMechanism((int)(12/cell_side_h),(int)(28/cell_side_h),cell_side_h,privObj);
//		tc_mech.write_mechanism_matrix_to_file(tc_file_name);
		
		
		System.out.println("evaluating loss for the tgeom old way..."+LocalDateTime.now());
		double loss = tc_mech.get_exp_loss_for_prior(gprior_h, lossObj);
		System.out.println("loss = "+ loss+ "at time"+ LocalDateTime.now());
		
		// used to compare with the linear algebra method to evaluate loss.
		//System.out.println("evaluating loss for the tgeom new way..."+LocalDateTime.now());
		//loss = tc_mech.get_expected_loss_for_prior(gprior_h, lossObj);
		//System.out.println("loss = "+ loss+ "at time"+ LocalDateTime.now());
		
	}

		
	
	}
	
