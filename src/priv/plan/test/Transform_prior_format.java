package priv.plan.test;

import priv.plan.priors.Prior;

public class Transform_prior_format {
	
	// This program constructs a prior from a string, and then write it 
	// to a file in matrix form.
	// Used to the paper "practical mechanism 2016" to write the 
	// global priors of regions in the readable  matrix format 
	public static void main(String[] args) {

		String prior_str = 
				"0.0024 0.0018 0.0007 0.0006 0.0092 0.0016 0.0008 0.0035 0.0004 0.0002 0.0012 0.0061 0.0183 0.0139 0.0017 0.0062 0.0102 0.0031 0.0044 0.0245 0.0389 0.0164 0.0121 0.0080 0.0083 0.0035 0.0125 0.0175 0.0278 0.0233 0.0132 0.0111 0.0051 0.0090 0.0040 0.0151 0.0220 0.0265 0.0893 0.0897 0.0007 0.0018 0.0039 0.0130 0.0224 0.0509 0.1378 0.0247 0.0001 0.0019 0.0059 0.0221 0.0117 0.0203 0.0400 0.0218 0.0038 0.0010 0.0026 0.0041 0.0127 0.0245 0.0082 0.0001";
		int hsize=8; int vsize=8; double cell_side=1.0;
		Prior p = new Prior(prior_str,hsize,vsize,cell_side);
		p.write_prior_to_file("san_francisco_prior.pr",true);
	}

}
