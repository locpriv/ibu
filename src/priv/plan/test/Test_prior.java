package priv.plan.test;

import priv.plan.priors.Prior;

public class Test_prior {
	
	// This program constructs a prior from a file, and then write it 
	// again in a new file, and also in a string format to test the 
	// two methods of representation. 

	public static void main(String[] args) {

		Prior p1 = new Prior("test_prior1.pr");
	    p1.write_prior_vector_to_file("test_prior_kostas.pr");
	    String prior_str = p1.get_prior_as_string();
	    System.out.println(prior_str);
//		Prior p2 = new Prior(prior_str);
//		p2.write_prior_to_file("test_prior1_copy.pr");
		
		// test_prior1, test_prior2, and the string in the console should be the same
	}

}
