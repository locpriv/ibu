package priv.plan.test;

import priv.plan.loss.LinGeoLoss;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;


// create truncated Laplace mechanism for various epsilons and evaluate the loss for a 
// uniform prior. 
public class Write_Lap_mech64 {

	public static void main(String[] args) {
		
		Prior p = new Prior(8,8,1.0);
		LinGeoLoss lossObj = new LinGeoLoss(16,16);

		TPLapMechanism_integ m1 = new TPLapMechanism_integ(8,8,1.0,0.2);
		//m1.write_mechanism_to_file("lap"+0.2+".mech");
		System.out.println(m1.get_exp_loss_for_prior(p, lossObj));
		
		TPLapMechanism_integ m2 = new TPLapMechanism_integ(8,8,1.0,0.5);
		//m2.write_mechanism_to_file("lap"+0.5+".mech");
		System.out.println(m2.get_exp_loss_for_prior(p, lossObj));

		TPLapMechanism_integ m3 = new TPLapMechanism_integ(8,8,1.0,1.0);
		//m3.write_mechanism_to_file("lap"+1.0+".mech");
		System.out.println(m3.get_exp_loss_for_prior(p, lossObj));

		TPLapMechanism_integ m4 = new TPLapMechanism_integ(8,8,1.0,1.5);
		//m4.write_mechanism_to_file("lap"+1.5+".mech");
		System.out.println(m4.get_exp_loss_for_prior(p, lossObj));

		TPLapMechanism_integ m5 = new TPLapMechanism_integ(8,8,1.0,2.0);
		//m5.write_mechanism_to_file("lap"+2.0+".mech");
		System.out.println(m5.get_exp_loss_for_prior(p, lossObj));


	}

}
