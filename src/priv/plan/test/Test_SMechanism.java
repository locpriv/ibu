package priv.plan.test;
import priv.plan.gdset.GDataset;
import priv.plan.gdset.GSettings;
import priv.plan.loss.LinGeoLoss;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;

public class Test_SMechanism {

	public static void main(String[] args) {
		
		String region_name= "san_francisco";  // region name: used for file names
		double south= 37.7402;
		double north= 37.8121;
		double west = -122.4760;
		double east = -122.3850;

		double cell_side = 1;  // corresponding to 8*8 cells grid
		
		double eps=0.2;
		
		System.out.println("constructing the dataset and low and hi resolution grids");
		GDataset ds = new GDataset(GSettings.data_file,south,north,west,east,cell_side,".*");
		Prior p64 = ds.get_prior();
		Prior p256 = ds.get_prior(0.5);
		
		double sat_dist = ds.get_max_distance();
		LinGeoLoss lossObj = new LinGeoLoss(sat_dist,sat_dist);
		
		// optimal mechanism on 8*8 grid
		System.out.println("loading the optimal mechanism for low-resolution grid...");
		AnnotatedFileMechanism optm= new AnnotatedFileMechanism(region_name+"_64_"+eps+".m");
		System.out.println("constructing scaled mechanism for hi-resolution grid...");
		SMechanism sm = new SMechanism(optm,p256,lossObj);
		sm.write_mechanism_to_file("scaled_san_francisco_256.m");
		
		
		System.out.println("constructing truncated planar geometric mechanism for high resolution grid...");
		TPGeomMechanism tpg = new TPGeomMechanism(p256.hsize,p256.vsize,p256.cell_side,eps);
		System.out.println("remapping truncated planar geometric mechanism...");
		RMechanism tpgr = new RMechanism(tpg, p256, lossObj);

		
		

		System.out.println("getting utility of remapped geometric mechanism...");
		System.out.println("loss of remapped geometric for prior = "+ tpgr.get_exp_loss_for_prior(p256, lossObj));
		System.out.println("loss of all users using the remapped geometric mechanism");
		GMechanism gm1 = new GMechanism(tpgr,south,west);
		System.out.println(gm1.get_exp_losses_for_gdata_points(ds, lossObj, 15));
		
		System.out.println("");

		System.out.println("getting utility of scaled mechanism...");
		System.out.println("loss of scaled mechanism for prior = "+ sm.get_exp_loss_for_prior(p256, lossObj));
		System.out.println("loss of all users using the scaled mechanism");
		GMechanism gm2 = new GMechanism(sm,south,west);
		System.out.println(gm2.get_exp_losses_for_gdata_points(ds, lossObj, 15));

//		System.out.println("writing scaling remap...");
//		sm.write_scaling_remap_to_file(region_name+"_sc_rem_64_256.rem");

	}
}
