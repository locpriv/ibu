package priv.plan.test;

import priv.plan.priors.Prior;

public class Test_VectorPrior {
	
   // It reads a prior from a file in vector (S) format. That is, prior is 
   // is in one line in which locations are orderd y0x0,y0x1,....
	
	public static void main(String[] args) {
		double prob=0;
		Prior p = new Prior("gprior_8400.pr",60,140,0.2);
		for (int i=0; i<p.hsize; i++)
			for (int j=0; j< p.vsize;j++) 
				prob = prob + p.getProb(i,j);
		System.out.println(prob);
     
	}

}
