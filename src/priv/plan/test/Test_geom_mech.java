package priv.plan.test;

import priv.plan.loss.*;
import priv.plan.mechanisms.*;

// This tests the truncated planar geometric mechanism 
public class Test_geom_mech {

	public static void main(String[] args) {

	     int hsize= 2;
	     int vsize =2;
	     double cell_side=1.0;
	     double eps = 1.0;
	     
//	     LinGeoLoss lossObj = new LinGeoLoss(10,10);
//	     lossObj.write_kostas_loss_to_file("linloss.los", hsize, vsize, cell_side);

		
     TPGeomMechanism m = new TPGeomMechanism(hsize,vsize,cell_side,eps);
     
     //System.out.println("0000= "+ m.getProb(0,0,0,0));
     //System.out.println("0303= "+ m.getProb(0,2,0,2));
     //System.out.println("3030= "+ m.getProb(3,0,3,0));
     //System.out.println("3232= "+ m.getProb(3,2,3,2));

     //System.out.println("1111= "+ m.getProb(1,1,1,1) );
     
     
     
//     m.write_mechanism_to_file("mech_1.mech");
//     m.write_kostas_mechanism_to_file("mech_k.mech");
     // read the mecanism stored in a file as a matrix with rows/columns are in S order
     // i.e. in Kostas format
//     KostasMechanism mk = new KostasMechanism("mech_k.mech",hsize,vsize,cell_side);
//     mk.write_mechanism_to_file("mech_2.mech");
     
     // now compare between mech1, and mech2 files. Should be the same.
     
     
     // tests that total probability is 1
     double prob =0;
    	     for (int out_x = 0; out_x< m.hsize;out_x++)
    	    	 for (int out_y=0; out_y<m.vsize;out_y++)
    		 prob = prob + m.getProb(0,0,out_x,out_y);
     System.out.println("total probability = " + prob);
     
     prob =0;
     for (int out_x = 0; out_x< m.hsize;out_x++)
    	 for (int out_y=0; out_y<m.vsize;out_y++)
	 prob = prob + m.getProb(0,1,out_x,out_y);
     System.out.println("total probability = " + prob);

     prob =0;
     for (int out_x = 0; out_x< m.hsize;out_x++)
    	 for (int out_y=0; out_y<m.vsize;out_y++)
	 prob = prob + m.getProb(1,0,out_x,out_y);
     System.out.println("total probability = " + prob);
     prob =0;
     for (int out_x = 0; out_x< m.hsize;out_x++)
    	 for (int out_y=0; out_y<m.vsize;out_y++)
	 prob = prob + m.getProb(1,1,out_x,out_y);
     System.out.println("total probability = " + prob);
     
     m.write_mechanism_to_file("geo_mech.mech");
	}

}
