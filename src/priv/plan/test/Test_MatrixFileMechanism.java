package priv.plan.test;

import priv.plan.loss.*;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;

public class Test_MatrixFileMechanism {
	
	// construct a prior from a file in Kostas format, 
	// constructs a mechanism from a file in kostas format
	// i.e rows and columns are in S order
	// remap the mechanism. 
	
	// Tests constructing a mechanism from a file, and computes its expected loss
	// with respect to a given prior and a loss function

	public static void main(String[] args) {
		double prob=0;
		Prior p = new Prior("gprior_8400.pr",60,140,0.2);
		//for (int i=0; i<p.hsize; i++)
		//	for (int j=0; j< p.vsize;j++) 
		//		prob = prob + p.prior_matrix[i][j];
		//System.out.println(prob);
     
	GeoPriv privObj = 	new GeoPriv(Math.log(1.4)/0.1); 
	TCRectMechanism tc = new TCRectMechanism(p.hsize,p.vsize,p.cell_side, privObj);
	 //KostasMechanism m = new KostasMechanism("tlap_cells_8400_1.4.mech",p.hsize,p.vsize,p.cell_side);
     BinLoss lossObj = new BinLoss(0,1);
     
     RMechanism mr = new RMechanism(tc,p,lossObj);
     
     //double loss = m.get_expected_loss_for_prior(p, lossObj);
     //System.out.println(loss);
	}

}
