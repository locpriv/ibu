package priv.plan.test;

import priv.plan.loss.*;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.*;


public class Test_opt_mech {

	// computes the optimal mechanism using the class OptMechanism
	public static void main(String[] args) {
		
		String region_name = "san_francisco";
		double eps=0.2;

		   String prior_input_file = region_name+"_4.pr";

		// create a prior object 
			System.out.println("Reading the prior file: "+ prior_input_file);
			Prior prior = new Prior(prior_input_file); 

			double sat_dist = prior.cell_side*Math.sqrt(prior.hsize*prior.hsize+prior.vsize*prior.vsize);
			LinGeoLoss lossObj = new LinGeoLoss(sat_dist,sat_dist); // loss function

			
			GeoPriv privObj = new GeoPriv(eps) ;    // privacy per 1km. 
		   // Thus for two adjacent cells, privacy = e^(eps * cell_side). 
		   
		   OptMechanism opt_m = new OptMechanism(prior, privObj, lossObj);
		   opt_m.write_mechanism_to_file(region_name+"_4_"+eps+".m");

	}
}
