package priv.plan.test;

import priv.plan.mechanisms.*;
import priv.plan.privMetrics.ManPriv;


// verifies that ProdTGeomMechanism is exactly a tight-constraints mechanism 
// with respect to Manhattan privacy 
public class Test_tc_mech_man {

	public static void main(String[] args) {

		   int hsize = 10; int vsize = 10; double cell_side = 1;
		   ManPriv privObj = new ManPriv(Math.log(2));   // eps= log(2) per km;

		   ProdTGeomMechanism m1 = new ProdTGeomMechanism(hsize,vsize, cell_side,privObj.eps);
		   m1.write_diagonal_to_file("m1_diag.m");
		   
		   TCSqMechanism m2 = new TCSqMechanism(hsize,cell_side,privObj);
		   m2.write_diagonal_to_file("m2_diag.m");
	}
}
