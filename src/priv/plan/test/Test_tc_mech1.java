package priv.plan.test;

import priv.plan.mechanisms.*;
import priv.plan.privMetrics.GeoPriv;
import priv.plan.privMetrics.ManPriv;

public class Test_tc_mech1 {
	
	//static int vsize =80; 
	static double cell_side =1;
	static GeoPriv privObj= new GeoPriv(Math.log(2));

	public static void main(String[] args) {
		
		//TCSqMechanism mt1 = new TCSqMechanism(7,cell_side,privObj);
		//mt1.write_mechanism_to_file("TCSq.mech");
		//mt1.write_diagonal_to_file("TCSq.txt");
		//System.out.println("number of classes for square grid = "+ mt1.get_mu_size());
		//TCRectMechanism mt2 = new TCRectMechanism(40,50,cell_side,privObj);
		//mt2.write_mechanism_to_file("TCRect.mech");
		//mt2.write_diagonal_to_file("TCRect.txt");
		//System.out.println("number of classes for rectangle grid = "+ mt2.get_mu_size());
		TCRectMechanism mt2 = new TCRectMechanism(60,140,cell_side,privObj);
		// if we use TCRectMechanism_jblas instead, we get error: org.jblast INFO Deleting...
//		mt2.write_mechanism_matrix_to_file("test_matrix1.txt");
		
//		TPGeomMechanism mt2 = new TPGeomMechanism(80,100,cell_side,privObj.eps);

/******** check the validity of the mechanism  ********/

		double sum;
		double prob;

		for (int in_x=0;in_x<mt2.hsize;in_x++)
			for (int in_y=0;in_y<mt2.vsize;in_y++)
			{
				sum =0;
				for (int out_x=0; out_x<mt2.hsize;out_x++)
					for (int out_y=0;out_y<mt2.vsize;out_y++) {
						prob  = mt2.getProb(in_x,in_y,out_x,out_y);
						sum = sum + prob;
					}
				System.out.println("total probability for ("+in_x+","+in_y +")= "+ sum);
			}
	
	}

}
