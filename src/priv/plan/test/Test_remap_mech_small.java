package priv.plan.test;
import priv.plan.loss.*;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.*;

public class Test_remap_mech_small {
	
	// correct mechanism but wrong expected loss
	// This program uses a prior to remap different mechanisms and print them, in order
	// to between their probabilities. 

	public static void main(String[] args) {
		
		
		GeoPriv privObj = new GeoPriv(Math.log(2));
		//BinLoss lossObj = new BinLoss(0,1);
		LinGeoLoss lossObj = new LinGeoLoss(1,200);   // saturation distance at 100km 
		Prior p = new Prior(4,3,1);
		p.fill_random_prior_matrix();
//		p.write_prior_to_file("tc_prior.pr");
		//p.write_prior_to_file("test_prior1.pr");
//		Prior p = new Prior("tc_prior.pr");
		
		TCRectMechanism tc = new TCRectMechanism(p.hsize,p.vsize,p.cell_side,privObj);
//		tc.write_mechanism_to_file("tc.mech");
//		AnnotatedFileMechanism tc = new AnnotatedFileMechanism("tc.mech");
		RMechanism tc_rem1 = new RMechanism(tc,p,lossObj);
//		tc_rem1.write_mechanism_to_file("tc_rem1.mech");
		System.out.println("loss of the tc mechanism (with old remap) = "+ tc_rem1.get_exp_loss_for_prior(p, lossObj));
		RMechanism tc_rem2 = new RMechanism(tc,p,lossObj);
		tc_rem2.write_mechanism_to_file("tc_rem2.mech");
		System.out.println("loss of the tc mechanism (with new remap) = "+ tc_rem2.get_exp_loss_for_prior(p, lossObj));
		
//		TPLapMechanism4 lm = new TPLapMechanism4(p.hsize,p.vsize,p.cell_side,privObj.eps);
//		System.out.println("loss of the laplace mechanism (old) = "+ lm.get_expected_loss_for_prior(p, lossObj));
//		System.out.println("loss of the laplace mechanism (new) = "+ lm.get_expected_loss_for_prior1(p, lossObj));
//		RMechanism rm1 = new RMechanism(lm,p,loss_type);
//		rm1.write_mechanism_to_file("lmech_remapped.m");

		
//		TPGeomMechanism gm = new TPGeomMechanism(p.hsize,p.vsize,p.cell_side,privObj.eps);
//		gm.write_mechanism_to_file("gmech.mech");
//		RMechanism_old rgm1 = new RMechanism_old(gm,p,lossObj);
//		rgm1.write_mechanism_to_file("gmech_remapped1.mech");
		
//		RMechanism rgm2 = new RMechanism(gm,p,lossObj);
//		rgm2.write_mechanism_to_file("gmech_remapped2.mech");
//		System.out.println("expected loss for geometric after remapping (old) = "+ rgm2.get_expected_loss_for_prior_old(p, lossObj));
//		System.out.println("expected loss for geometric after remapping (new) = "+ rgm2.get_exp_loss_for_prior(p, lossObj));
//		//rgm2.write_mechanism_matrix_to_file("gmech_remapped2_matrix.mech");

		
		//		TCSqMechanism tm = new TCSqMechanism(p.hsize,p.cell_side,privObj);
//		RMechanism rm3= new RMechanism(tm,p,lossObj);
//		rm3.write_mechanism_to_file("tcmech_remapped.m");

	}

}
