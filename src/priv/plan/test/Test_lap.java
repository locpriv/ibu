package priv.plan.test;

import priv.plan.loss.BinLoss;
import priv.plan.loss.LinGeoLoss;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;


public class Test_lap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int hsize=5, vsize=5; double cell_side=0.1;
		
		double eps = 8;
		
		GeoPriv privObj = new GeoPriv(eps);

		Prior up = new Prior(hsize, vsize, cell_side);
		
//		LinGeoLoss lossObj = new LinGeoLoss(10*hsize*cell_side, 10*hsize*cell_side); 
		BinLoss lossObj = new BinLoss(0,1); 
		
//		TCSqMechanism tc = new TCSqMechanism(hsize,cell_side,privObj);
//		System.exit(0);

//		System.out.println("constructing Lap mechanism");
//		TPLapMechanism lap = new TPLapMechanism(hsize,vsize,cell_side,eps);

//		System.out.println("constructing Lap2 mechanism");
//		TPLapMechanism2 lap2 = new TPLapMechanism2(hsize,vsize,cell_side,eps);

		
		System.out.println("constructing Lap2 mechanism");
		TPLapMechanism_integ lap2 = new TPLapMechanism_integ(hsize,vsize,cell_side,eps);
		System.out.println("constructing Lap4 mechanism");
		TPLapMechanism lap4 = new TPLapMechanism(hsize,vsize,cell_side,eps);

//	   System.out.println("expected loss for lap: "+ lap.get_expected_loss_for_prior(up, lossObj));
       System.out.println("expected loss for lap with integration: "+ (lap2.get_exp_loss_for_prior(up, lossObj)));
       System.out.println("expected loss for lap with sampling: "+ (lap4.get_exp_loss_for_prior(up, lossObj)));
		
/*		
		System.out.println("prob(0,0,0,0) = "+ lap.getProb(0, 0, 0, 0));
		System.out.println("prob(0,0,0,0) = "+ lap1.getProb(0, 0, 0, 0));
		System.out.println("\n");

		System.out.println("prob(2,2,2,2) = "+ lap.getProb(2, 2, 2, 2));
		System.out.println("prob(2,2,2,2) = "+ lap1.getProb(2, 2, 2, 2));
		System.out.println("\n");

		System.out.println("prob(0,0,0,1) = "+ lap.getProb(0, 0, 0, 1));
		System.out.println("prob(0,0,0,1) = "+ lap1.getProb(0, 0, 0, 1));
		System.out.println("\n");
		
		System.out.println("prob(2,2,2,1) = "+ lap.getProb(2, 2, 2, 1));
		System.out.println("prob(2,2,2,1) = "+ lap1.getProb(2, 2, 2, 1));
		System.out.println("\n");
		
		System.out.println("prob(0,0,0,2) = "+ lap.getProb(0, 0, 0, 2));
		System.out.println("prob(0,0,0,2) = "+ lap1.getProb(0, 0, 0, 2));
		System.out.println("\n");

		System.out.println("prob(2,2,2,0) = "+ lap.getProb(2, 2, 2, 0));
		System.out.println("prob(2,2,2,0) = "+ lap1.getProb(2, 2, 2, 0));
		System.out.println("\n");

		
		System.out.println("prob(0,0,1,0) = "+ lap.getProb(0, 0, 1, 0));
		System.out.println("prob(0,0,1,0) = "+ lap1.getProb(0, 0, 1, 0));
		System.out.println("\n");


		System.out.println("prob(2,2,1,2) = "+ lap.getProb(2, 2, 1, 2));
		System.out.println("prob(2,2,1,2) = "+ lap1.getProb(2, 2, 1, 2));
		System.out.println("\n");

		System.out.println("prob(0,0,1,1) = "+ lap.getProb(0, 0, 1, 1));
		System.out.println("prob(0,0,1,1) = "+ lap1.getProb(0, 0, 1, 1));
		System.out.println("\n");

		System.out.println("prob(2,2,1,1) = "+ lap.getProb(2, 2, 1, 1));
		System.out.println("prob(2,2,1,1) = "+ lap1.getProb(2, 2, 1, 1));
		System.out.println("\n");

*/	
		
/*				
		double sum;
		double sum4;
		double prob;
		for (int in_x=0;in_x<lap4.hsize;in_x++)
			for (int in_y=0;in_y<lap4.vsize;in_y++)
			{
				sum =0;
				sum4 =0;
				for (int out_x=0; out_x<lap4.hsize;out_x++)
					for (int out_y=0;out_y<lap4.vsize;out_y++) {
//						prob  = lap.getProb(in_x,in_y,out_x,out_y);
//						sum = sum + prob;
						prob  = lap4.getProb(in_x,in_y,out_x,out_y);
						sum4 = sum4 + prob;
					}
//				System.out.println("total probability for lap ("+in_x+","+in_y +")= "+ sum);
				System.out.println("total probability for lap1 ("+in_x+","+in_y +")= "+ sum4);
			}
*/

	}

}
