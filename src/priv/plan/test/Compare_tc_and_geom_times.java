package priv.plan.test;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.*;

public class Compare_tc_and_geom_times {
	
	// This program creates an instance of geometric mechanism, and 
	// another one for the tc-mechanism to compare the time of constructing 
	// them. 

	public static void main(String[] args) {
		
		GeoPriv privObj = new GeoPriv(10*Math.log(2));
//		String loss_type = Loss.LOSS_LIN_GEO;
		Prior gp = new Prior("los_angeles_prior.pr");
		
		System.out.println("Constructing truncated planar geometric mechanism...");
		TPGeomMechanism gm = new TPGeomMechanism(gp.hsize,gp.vsize,gp.cell_side,privObj.eps);
//		System.out.println("Remapping truncated planar geometric mechanism...");
//		RMechanism rgm = new RMechanism(gm,gp,loss_type);
//		rgm.write_mechanism_to_file("gmech_remapped.m");
		

		System.out.println("Constructing TC-constraints mechanism...");
		TCSqMechanism tm = new TCSqMechanism(gp.hsize,gp.cell_side,privObj);
//		System.out.println("Remapping TC-constraints mechanism...");
//		RMechanism rtm= new RMechanism(tm,gp,loss_type);
//		rtm.write_mechanism_to_file("tcmech_remapped.m");
		
//		TPLapMechanism lm = new TPLapMechanism(gp.hsize,gp.vsize,gp.cell_side,eps);
//		lm.write_mechanism_to_file("lap_mech_eps2_64.m");
//		RMechanism rlm = new RMechanism(lm,gp,loss_type);
//		rm1.write_mechanism_to_file("lmech_remapped.m");
        
		
/*		
		Prior up1 = new Prior("los_angeles_user_2_prior.pr");
		Prior up2 = new Prior("los_angeles_user_531_prior.pr");
		Prior up3 = new Prior("los_angeles_user_777_prior.pr");
		Prior up4 = new Prior("los_angeles_user_1745_prior.pr");
		
//		System.out.println("expected loss of the Laplacian mechanism for user 1: "+ 
//		         rlm.get_expected_loss_for_prior(up1, loss_type));
		System.out.println("expected loss of the geometric mechanism for user 1: "+ 
		         rgm.get_expected_loss_for_prior(up1, loss_type));
		System.out.println("expected loss of the TC-mechanism for user 1: "+ 
		         rtm.get_expected_loss_for_prior(up1, loss_type)+"\n");

//		System.out.println("expected loss of the Laplacian mechanism for user 2: "+ 
//		         rlm.get_expected_loss_for_prior(up2, loss_type));
		System.out.println("expected loss of the geometric mechanism for user 2: "+ 
		         rgm.get_expected_loss_for_prior(up2, loss_type));
		System.out.println("expected loss of the TC-mechanism for user 2: "+ 
		         rtm.get_expected_loss_for_prior(up2, loss_type)+"\n");

//		System.out.println("expected loss of the Laplacian mechanism for user 3: "+ 
//		         rlm.get_expected_loss_for_prior(up3, loss_type));
		System.out.println("expected loss of the geometric mechanism for user 3: "+ 
		         rgm.get_expected_loss_for_prior(up3, loss_type));
		System.out.println("expected loss of the TC-mechanism for user 3: "+ 
		         rtm.get_expected_loss_for_prior(up3, loss_type)+"\n");

//		System.out.println("expected loss of the Laplacian mechanism for user 4: "+ 
//		         rlm.get_expected_loss_for_prior(up4, loss_type));
		System.out.println("expected loss of the geometric mechanism for user 4: "+ 
		         rgm.get_expected_loss_for_prior(up4, loss_type));
		System.out.println("expected loss of the TC-mechanism for user 4: "+ 
		         rtm.get_expected_loss_for_prior(up4, loss_type)+"\n");
*/}

}
