package priv.plan.test;

import java.time.LocalDateTime;

import priv.plan.gdset.GDataset;
import priv.plan.gdset.GSettings;
import priv.plan.loss.LinGeoLoss;
import priv.plan.mechanisms.*;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;

public class Test_remap_loss_on_60_140 {
	
	
	// Construct a prior on large grid 8400 cells, a tgeom mechanism, and 
	// then test the performance of remapping, and evaluating the loss of 
	// this mechanism with respect to a prior on this grid.

	//  A region in San Francisco (12km*28km) agreed with Kostas
	//South: 37.5395 North: 37.7910
	//West: -122.5153 East : -122.3789
	//north-to-south: 28.000 km 
	//east-to-west: 12.001 km 
	//Grid: hsize = 6 , vsize = 14 
	//cell side :2 km		
	static double south= 37.5395;
	static double north= 37.7910;
	static double west = -122.5153;
	static double east = -122.3789;
	
	static double cell_side_h = 0.2;  // making a grid of 60*140 cells

	
	static GDataset ds;
//	static Prior gprior;

	public static void main(String[] args) {

		// get priors at different resolutions
		System.out.println("Constructing dataset...");
		ds= new GDataset(GSettings.data_file, south, north, west, east, cell_side_h, ".*");

//		System.out.println("constructing loss file");
		double saturation_dist = ds.get_max_distance();  
		LinGeoLoss lossObj;
		lossObj = new LinGeoLoss(saturation_dist, saturation_dist);
//		lossObj.write_kostas_loss_to_file1("loss_8400.los", 60, 140, cell_side_h);

		
		// construct high-res prior on 8400 cells
		System.out.println("constructing prior");
		Prior gprior_h = ds.get_prior(cell_side_h);
		//System.out.println("constructing prior file");
		//gprior_h.write_prior_vector_to_file("gprior_8400.pr");

		// use l = ln(t), t = 1.4 .. 2.9  // eps = l/0.1 
        // System.out.println("constructing mechanisms..");
		double t =1.4;
		
		GeoPriv privObj = new GeoPriv(Math.log(t)/0.1);  // when multiplied by cell_side 0.2, we get reasonable l
		String l_str = String.format("%02."+1+"f", Math.exp(privObj.eps*0.1)); //just to verify
		
//		int grid_size_h = (int) ((12/cell_side_h)*(28/cell_side_h));
		
//		System.out.println("constructing lab for" + l_str);
//		TPLapMechanism4 tlap_mech;
//		String tlap_file_name = "tlap_cells_"+grid_size_h+"_"+l_str+".mech";
//		tlap_mech = new TPLapMechanism4((int)(12/cell_side_h),(int)(28/cell_side_h),cell_side_h,privObj.eps);
//		tlap_mech.write_mechanism_matrix_to_file(tlap_file_name);

		System.out.println("constructing tgeom for" + l_str);
		TPGeomMechanism tgeom;
//		String tgeom_file_name = "tgeom_cells_"+grid_size_h+"_"+l_str+".mech";
		tgeom = new TPGeomMechanism((int)(12/cell_side_h),(int)(28/cell_side_h),cell_side_h,privObj.eps);
//		tgeom_mech.write_mechanism_matrix_to_file(tgeom_file_name);

//		System.out.println("constructing tc for" + l_str);
//		TCRectMechanism tc_mech;
//		String tc_file_name = "tc_cells_"+grid_size_h+"_"+l_str+".mech";
//		tc_mech = new TCRectMechanism((int)(12/cell_side_h),(int)(28/cell_side_h),cell_side_h,privObj);
//		tc_mech.write_mechanism_matrix_to_file(tc_file_name);
		
		
		System.out.println("evaluating loss for the tgeom..."+LocalDateTime.now());
		System.out.println(tgeom.get_exp_loss_for_prior(gprior_h, lossObj) + " at time"+ LocalDateTime.now());
		//System.out.println("Remapping the tgeom..."+LocalDateTime.now());
		//RMechanism tgeom_rem = new RMechanism(tgeom_mech,gprior_h, lossObj);
		//System.out.println("evaluating loss for the prior..."+LocalDateTime.now());
		//System.out.println(tgeom_rem.get_expected_loss_for_prior(gprior_h, lossObj) + "  at time"+ LocalDateTime.now());
		GMechanism gm = new GMechanism(tgeom,south,west);
		System.out.println("evaluating losses of users..."+LocalDateTime.now());
		System.out.println(gm.get_exp_losses_for_gdata_points(ds, lossObj, 600));
		
		// used to compare with the linear algebra method to evaluate loss.
		//System.out.println("evaluating loss for the tgeom new way..."+LocalDateTime.now());
		//loss = tc_mech.get_expected_loss_for_prior(gprior_h, lossObj);
		//System.out.println("loss = "+ loss+ "at time"+ LocalDateTime.now());
		
	}

		
	
	}
	
