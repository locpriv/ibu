package stat.spaces;

import java.util.ArrayList;
import java.util.List;
import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.*;
import stat.distributions.Sampling;


/** a collection of BitVectorEmpDatum records, together with hsize, vsize to describe 
 * the correspondence to original locations.  
 * 
 */
public class BitVectorEmpData {
	public List<BitVectorEmpDatum> data; // a list of bitVectors/frequency in the output grid Z, 
	                             // every bit in a bitVector corresponds to a location in its linear order 
	                             // and has nonzero frequency
	//public int total_observations; 
	public int vectSize;   // the number of bits in every observed vector (k in the notation of Kairouz)

	//public int hsize,vsize;  // specification of the grid on which EmpData is evaluated 
	
	/** 
	 * Construct BitVectorEmpData object by obfuscating a given GridEmpData using a give Rappor mechanism. 
	 * @param p_rmech
	 * @param p_origEmpGridData
	 */
	public BitVectorEmpData(GridEmpData p_origEmpGridData, BasicOneTimeRappor p_rmech) {
		// validate the mechanism with p_origEmpGridData
		if ((p_origEmpGridData.hsize!= p_rmech.hsize)||(p_origEmpGridData.vsize != p_rmech.vsize)) {
			System.out.println("The original emprical data grid is incompatible with the grid of the given Rappor mechanism");
			System.exit(0);
		}
		vectSize = p_origEmpGridData.hsize*p_origEmpGridData.vsize; 
		data = new ArrayList<BitVectorEmpDatum>();
		int totalLocs = p_origEmpGridData.data.size();
		// for every datum (loc, freq) get freq random bitVectors, and add them to data 
		for (int l=0; l<totalLocs;l++) {
			//System.out.println("Handling location "+ l + " out of "+ totalLocs+" .. Obfuscating "+ p_origEmpGridData.data.get(l).frequency + " observations");
			for (int u=0; u<p_origEmpGridData.data.get(l).frequency;u++) {
				boolean[] obfBitVector = obfuscateLoc(p_origEmpGridData.data.get(l).location, p_rmech.eps);  
				// if obfBitVector exists in data update frequency, otherwise add it to the list with frequency 1
				int i = 0;
				while (i<data.size()&& !bitVectorsAreEqual(obfBitVector, data.get(i).bitVector)) i++;
				if (i<data.size()) 
					data.get(i).frequency++;
				else {
					data.add(new BitVectorEmpDatum(obfBitVector,1));
					//System.out.println("detected a new vector.. total different vectors = "+data.size());
				}
			}
			
		}
	}
	
	private boolean bitVectorsAreEqual (boolean[] p_bitVector1, boolean[] p_bitVector2) {
		if ((vectSize != p_bitVector1.length)||(vectSize != p_bitVector2.length)) {
			System.out.println("The given bitVectors are incompatible"); 
			System.exit(0);
		}
		int j=0;
		while (j<vectSize && p_bitVector1[j]==p_bitVector2[j]) 
			j++;
		if (j==vectSize) return true;
		else return false;
	}
	
	/**
	 * returns an obfuscated bit vector based on a given p_loc (in linear order) and p_eps. 
	 * @param p_loc
	 * @param p_eps
	 * @return
	 */
	private boolean[] obfuscateLoc(int p_loc, double p_eps) {
		boolean[] bitVector = new boolean[vectSize];
		double[] distribution = new double[2];
		// Let Xj be bit j in the vector representing original input
		// and Yj be bit j in the output vector after obfuscation
		distribution[0] = Math.exp(0.5*p_eps)/(1+Math.exp(0.5*p_eps));  // probability of Yj=Xj  
		distribution[1] = 1/(1+Math.exp(0.5*p_eps)); // probability of Yj!=Xj

		int[] bitRetention = Sampling.sample(distribution, vectSize); // 0 indicates no flip, 1 indicates flip
		for (int j=0; j<vectSize;j++) 
			if (bitRetention[j]==0)
				bitVector[j]=false;  // keep 0 as 0
			else bitVector[j]=true;  // flip 0 to 1;
		if (bitRetention[p_loc]==0)  // handling the bit at p_loc which is originally 1. 
			bitVector[p_loc]=true;   // keep 1 as 1
		else bitVector[p_loc]=false; // flip 1 to 0
		
		return bitVector;
	}

	/** 
	 * This method takes a Rappor mechanism p_rmech and returns its columns that correspond 
	 * to the elements of the data. i-th column from left corresponds to the i-th datum (bitVector, frequency) in data. 
	 * Used for RAPPOR EM estimation  
	 */
	public DoubleMatrix get_probability_matrix(BasicOneTimeRappor p_rmech) {
		//int columns = data.size();
		//System.out.println("adding column 0 out of "+ columns + " columns");
		DoubleMatrix pr_z_giv_x = p_rmech.get_mech_column(data.get(0).bitVector);
		for (int i=1;i< data.size();i++) {
			//System.out.println("adding column "+i+" out of "+ columns + " columns");
			pr_z_giv_x = DoubleMatrix.concatHorizontally(pr_z_giv_x, p_rmech.get_mech_column(data.get(i).bitVector));
		}
		return pr_z_giv_x;
	}
	

	/** returns the empirical distribution on observed outcomes (bitVectors) as a row vector. 
	 * the j-th element corresponds to the j-th datum (bitVector, frequency) in data.
	 * P.S. Since it is constructed from data, it contains only nonzero entries.
	 * It is used in EM estimation method since it requires no zeros in the empirical distribution.
	 * @return the empirical distribution of the BitVectorEmpData object
	 */
	public DoubleMatrix getDistributionOnObservables() {
		// create a column vector, and transpose
		DoubleMatrix emp_pr_z = new DoubleMatrix(data.size()).transpose(); 
		for (int i=0; i< data.size();i++) {
			emp_pr_z.put(i, data.get(i).frequency); 
		}
		// Normalize
		double sum = emp_pr_z.sum(); 
		emp_pr_z.divi(sum); 
		return emp_pr_z;
	}
	
	
	/**
	 * @return the empirical probability (from the observed data) that bit j is set in a random observed vector.
	 * that is entry j is the number of observed vectors with bit j equal to 1 divided by the total number of 
	 * observed vectors in the current bitVectorEmpData
	 */
	public DoubleMatrix get_emp_bit_probabilities() {
		DoubleMatrix ret = DoubleMatrix.zeros(vectSize);   // a column vector
		for (int i=0; i< data.size(); i++)
			for (int j=0; j<vectSize; j++)
				if (data.get(i).bitVector[j]) 
					ret.put(j, ret.get(j)+ data.get(i).frequency);
		int total_observations = get_total_observations();
		return ret.divi(total_observations).transpose();
	}
	
	/** 
	 * @return the sum of all frequencies in data
	 */
	public int get_total_observations() {
		int ret =0;
		for (int i=0; i< data.size(); i++)
			ret = ret + data.get(i).frequency;
		return ret;
	}
	
	
}
