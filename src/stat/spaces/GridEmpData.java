package stat.spaces;

import java.util.ArrayList;
import java.util.List;
import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.*;
import stat.distributions.Sampling;


/** a collection of EmpDatum records, together with hsize, vsize to describe the location attribute in every EmpDatum. 
 * 
 * @author ehab
 *
 */
public class GridEmpData {
	public List<GridEmpDatum> data; // a list of location/frequency in the output grid Z, 
	                             // every location is identified by its linear order 
	                             // and has nonzero frequency
	public int total_observations; // the sum of all frequencies in data

	public int hsize,vsize;  // specification of the grid on which EmpData is evaluated 

	/**
	 * EmpData object is specified by grid data hsize,vsize and an array of frequencies. Array elements 
	 * correspond to the grid cell in linear order
	 * @param p_hsize
	 * @param p_vsize
	 * @param p_freq_array
	 */
	// including zero-frequency locations, which will be discarded in the final attribute data. 
	public GridEmpData(int p_hsize, int p_vsize, int[] p_freq_array) {
		hsize = p_hsize;
		vsize = p_vsize;
		construct_data_from_freqArray(p_freq_array);
	}

	/** 
	 * Create a GridEmpData by obfuscating the given empirical data using the given mechanism.
	 * @param p_empdata 
	 * @param p_mech
	 */
	public GridEmpData(GridEmpData p_empdata, Mechanism p_mech) {
		// validate the mechanism with p_empdata
		if ((p_empdata.hsize!= p_mech.hsize)||(p_empdata.vsize != p_mech.vsize)) {
			System.out.println("The given emprical data grid is incompatible with the grid of the given mechanism");
			System.exit(0);
		}
		hsize = p_empdata.hsize; vsize = p_empdata.vsize; 

		// for every datum (loc, freq) get freq random locations using the mechanism distribution at loc
		int new_freq_array[] = new int[hsize*vsize];
		for (int j=0; j< p_empdata.data.size();j++) {
			// get the mechanism distribution at data.get(j).location
			double[] mech_row_probabilities = p_mech.get_mech_row_probabilities(p_empdata.data.get(j).location);
			// get an array obf_locs[] of data.get(j).frequency locations sampled from the above distribution. 
			int[] obf_locs = Sampling.sample(mech_row_probabilities, p_empdata.data.get(j).frequency);  
			// for each item in obf_locs[] update new_freq_array[] by adding 1. 
			for (int i=0;i<obf_locs.length; i++)
				new_freq_array[obf_locs[i]] += 1;
		}

		// construct a new EmpData object using the array obf_loc[] and return it. 
		construct_data_from_freqArray(new_freq_array);
	}

	private void construct_data_from_freqArray(int[] p_freq_array) {
		total_observations =0;
		data = new ArrayList<GridEmpDatum>(); // dynamic array of EmpDatum
		for (int loc=0;loc<p_freq_array.length;loc++)
			if (p_freq_array[loc]>0) {
				GridEmpDatum datum = new GridEmpDatum(loc,p_freq_array[loc]);
				data.add(datum);
				total_observations = total_observations + p_freq_array[loc];
			}
	}

	/** 
	 * This method takes a mechanism p_mech and returns its columns that correspond 
	 * to the elements of the data. j-th column corresponds to the j-th datum (location, 
	 * frequency) in data.  
	 */
	public DoubleMatrix get_probability_matrix(DoubleMatrix p_mech_matrix) {
		int[] empdata_locs = new int[data.size()];  // an array of observed locations in the empirical data 
		for (int j=0;j< data.size();j++)
			empdata_locs[j] = data.get(j).location;
		DoubleMatrix pr_z_giv_x = p_mech_matrix.getColumns(empdata_locs);
		return pr_z_giv_x;
	}
	
	/** returns the empirical distribution on observed outcomes (locations) as a row vector. 
	 * the j-th element corresponds to the j-th datum (location, frequency) in data.
	 * P.S. Since it is constructed from data, it contains no zero entries.
	 * It is used in EM estimation method since it requires no zeros in the empirical distribution.
	 * @return the empirical distribution of the empdata object
	 */
	public DoubleMatrix getDistributionOnObservables() {
		// create a column vector, and transpose
		DoubleMatrix emp_pr_z = new DoubleMatrix(data.size()).transpose(); 
		for (int j=0; j< data.size();j++) {
			emp_pr_z.put(j, data.get(j).frequency); 
		}
		// Normalize
		double sum = emp_pr_z.sum(); 
		emp_pr_z.divi(sum); 
		return emp_pr_z;
	}
	
	
	
	
}
