package stat.spaces;

/** 
 * a record that includes an obfuscated location in the space Z of observables, and the frequency of observing this location
 * @author ehab
 *
 */
public class GridEmpDatum {
	int location;  // location in the output grid Z, identified by its linear order
	int frequency; // number of times this datum is observed
	
	public GridEmpDatum(int p_location, int p_frequency) {
		location = p_location;
		frequency = p_frequency;
	}
}
