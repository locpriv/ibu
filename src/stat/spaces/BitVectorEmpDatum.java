package stat.spaces;

/** 
 * a record that includes an obfuscated bitVector in space Z of observables (BitVectors ), and the frequency of observing this observable
 * @author ehab
 *
 */
public class BitVectorEmpDatum {
	public boolean[] bitVector;  // observed bitVector
	public int frequency; // number of times this datum is observed
	
	public BitVectorEmpDatum(boolean[] p_bitVector, int p_frequency) {
		bitVector = p_bitVector;
		frequency = p_frequency;
	}
}
