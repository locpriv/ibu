package stat.spaces;

import org.jblas.DoubleMatrix;
import priv.plan.mechanisms.*;

/** 
 * Describes an abstract space X of secrets, as a general subset of a grid of locations
  * The grid is specified by hsize,vsize, cell_side. The subset may be the 
  * entire grid as in (GridSecretsSpace) or a discrete set of cells, or 
  * a convex hull of locations. Every case of these is implemented by a 
  * subclass of SecretsSpace.
  */ 
public abstract class SecretsSpace {
	// The following are essential information about the grid that contains the space
	// This grid is used to produce the heat map of the distribution resulting from 
	// the em algorithm.
	public int hsize;
	public int vsize;
//	public double cell_side;
	
	// default constructor
	public SecretsSpace(int p_hsize, int p_vsize) {
		hsize = p_hsize; vsize = p_vsize;
	}

	
	/** 
	 * get a row vector describing a uniform prior on the set of secrets.
	 * the mapping between the entries of the vector and the grid cells is 
	 * maintained by the specific class.
	 */
	abstract public DoubleMatrix get_uniform_distribution();
	
	
	/** 
	 * takes a mechanism p_mech and returns its rows that 
	 * correspond to the elements of the secrets space. Note that the 
	 * i-th row corresponds to the i-th element in the distribution vector, 
	 * and the mapping between these elements an the location on the grid 
	 * depends on the specific class.
	 */
	abstract public DoubleMatrix get_mechanism_matrix(Mechanism p_mech);
	
}
