package stat.spaces;

import org.jblas.DoubleMatrix;
import priv.plan.mechanisms.*;

/** 
 * This class describes the most flexible SecretsSpace. Here the space X of secrets is a grid of locations specified by hsize,vsize, cell_side. 
 * Note that cell_side is important here to compute the Earth Mover's Distance (EMD) on between distributions on the space, if needed.
 */ 
public class GridSecretsSpace extends SecretsSpace {
	
	public double cell_side;
	public GridSecretsSpace(int p_hsize, int p_vsize, double p_cell_side) {
		super(p_hsize,p_vsize);
		cell_side = p_cell_side;
	}

    // Mapping between grid cells and distribution entries is specific to 
	// the current class. Here (GridSecretsSpace)
	// cell(x,y) <=> vector[y*hsize+x]. This mapping is important to 
	// produce the heat map on the grid from the vector resulting from 
	// em method.

	/** 
	 * returns a row vector, a uniform distribution over the secrets
	 */
	public DoubleMatrix get_uniform_distribution() {
		return DoubleMatrix.ones(hsize*vsize).divi(hsize*vsize).transpose();
	}

	/** 
	 * returns a row vector, the distribution on the secrets as a row vector. 
	 * takes a GridEmpData object and produces from it a distribution on the secrets. 
	 * Note that it may contain zero elements. The distribution respects the semantics 
	 * of the space secrets: i-th item is location on the hsize*vsize grid represented by 
	 * its linear order. This method is used to obtain the original distribution on secrets 
	 * from empirical data. It is also used in the matrix inversion method
	 **/

	public DoubleMatrix get_distribution_from_emp_data(GridEmpData p_empdata) {
		 DoubleMatrix ret = DoubleMatrix.zeros(hsize*vsize);  // create a column vector
		 for (int i=0; i< p_empdata.data.size();i++)
			 ret.put(p_empdata.data.get(i).location, p_empdata.data.get(i).frequency);
			// Normalize
			double sum = ret.sum(); 
			ret.divi(sum);
			return ret.transpose();
		}

	/** 
	 * returns a row vector, the distribution on the secrets as a row vector. 
	 * takes a BitVectorEmpData object (resulting from a Rappor mechanism) and produces 
	 * from it a distribution on the secrets. Note that it may contain zero elements. 
	 * This method is used to obtain the empirical distribution on secrets from a bitVector 
	 * obfuscated data.  
	 **/
	public DoubleMatrix get_distribution_from_bitVectEmpData(BitVectorEmpData p_bitVectorEmpData) {
		DoubleMatrix ret = DoubleMatrix.zeros(hsize*vsize);   // a column vector
		int numObservedVectors = p_bitVectorEmpData.data.size();
		for (int i=0; i< numObservedVectors; i++)
			for (int j=0; j<hsize*vsize; j++)
				if (p_bitVectorEmpData.data.get(i).bitVector[j]) 
					ret.put(j, ret.get(j)+p_bitVectorEmpData.data.get(i).frequency);
		double sum = ret.sum();
		return ret.divi(sum).transpose();
	}
	
	
	/** 
	 * In the current class we return all the rows of the mechanism, i.e. the entire 
	 * mechanism matrix.
	 */
	public DoubleMatrix get_mechanism_matrix(Mechanism p_mech) {
		// ensure that the mechanism covers the secretsSpace
		if ((hsize != p_mech.hsize)||(vsize != p_mech.vsize)) {
			System.out.println("the space of secrets is incompatible with the mechanism grid");
			System.exit(0);
		}
		return p_mech.get_mech_matrix();
	}

}
