package stat.estimators;

import priv.plan.mechanisms.*;
import stat.spaces.BitVectorEmpData;
import stat.spaces.GridSecretsSpace;
import org.jblas.DoubleMatrix;

/**
 * The purpose of this class is to compute an estimator for the distribution on a given space of secrets, 
 * assuming a Rappor mechanism has be used for obfuscation. This estimator is the implementation of the 
 * method in [Kairouz, et al, 2016:discrete distribution estimation under local privacy], with applying 
 * normalization to correct resulting negative values. 
 **/ 
public class Estimator_BOTRappor_INVN {
	
	private GridSecretsSpace xspace;   // space of secrets X, which we require to estimate its distribution.
	private DoubleMatrix emp_bit_probabilities;   
	// element j is the number of times that bit j is equal to 1 in the observed empirical bitVectors (Tj/n) in [Kairouz 2016] 
	private double eps;

	
/**
 * Construct an estimator for the distribution on a given space of secrets, assuming a Rappor mechanism 
 * has be used for obfuscation. This estimator is the implementation of the method in 
 * [Kairouz, et al, 2016:discrete distribution estimation under local privacy], with applying 
 * normalization to correct resulting negative values.  
 * @param p_xspace : the space of secrets
 * @param p_rmech : the obfuscation Rappor mechanism
 * @param p_bitVectorEmpData: the observed empirical data. 
 */
	public Estimator_BOTRappor_INVN (GridSecretsSpace p_xspace, BasicOneTimeRappor p_rmech, BitVectorEmpData p_bitVectorEmpData) {
		
		xspace = p_xspace;
		// validate that the mechanism is compatible with p_empdata
		if ((p_xspace.hsize!= p_rmech.hsize)||(p_xspace.vsize != p_rmech.vsize)) {
			System.out.println("The grid space is incompatible with the given Rappor mechanism");
			System.exit(0);
		}
		eps = p_rmech.eps;

		// the element is just Tj/n where Tj is the number of bitVectors that have bit j equal to 1.
		// get for every bit Yj the empirical probability that Yj=1.
		emp_bit_probabilities = p_bitVectorEmpData.get_emp_bit_probabilities();
	}


	
	/**
     * This computes an estimator on the elements of the secrets space (xspace), using 
     * the method in [Kairouz, et al, 2016:discrete distribution estimation under local privacy], 
     * which also uses the inverse matrix of the k-RR mechanism.  
     * If the resulting vector has -ve components, it is corrected to a valid distrtibution using 
     * normalization.  
     * @return a distribution on the secrets space, as a row vector
     */
	public DoubleMatrix getEstimator () {
		double c1 = Math.exp(0.5*eps)+1;
		double c2 = Math.exp(0.5*eps)-1;
		DoubleMatrix pr_x = emp_bit_probabilities.mul(c1/c2).sub(1/c2);
		System.out.println("estimated = "+ pr_x);
		// normalization
		pr_x.muli(pr_x.ge(0));    // set -ve entries to zeros
		double sum = pr_x.sum();
		pr_x.divi(sum);
		//System.out.println("euclidean distance to the -ve distribution = "+ pr_x.distance2(pr_x1));
		return pr_x;

    }


}
