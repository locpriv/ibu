package stat.estimators;

import priv.plan.mechanisms.*;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;
import java.util.Arrays;

import org.jblas.DoubleMatrix;
import org.jblas.Solve;

/**
 * The purpose of this class is to evaluate the maximum likelihood distribution on a given space of secrets (xspace), 
 * using the inverse matrix with projection method from the paper: 
 * "Projection onto the probability simplex: An efficient algorithm with a simple proof, and an application" by Weiran Wang et al.
 * This class uses empirical data which describes a collection of locations and the (nonzero) 
 * frequency of every location, and the mechanism that produced this empirical data. Note that we assume here that a 
 * fixed mechanism is used. 
 */
public class Estimator_INVP {
	
	private GridSecretsSpace xspace;   // space of secrets X, which we require to estimate its distribution.
	private DoubleMatrix pr_z_giv_x;   // Reduced G matrix:rows represent elements of X, columns correspond to elements of Z
	private DoubleMatrix emp_pr_z;     // a row vector, empirical distribution on Z

	
/**
 * Construct an estimator for computing the max-likelihood distribution on a given space of secrets, an obfuscation
 * mechanism, and empirical data (including zero frequencies) resulting by obfuscation. 
 * @param p_xspace : the space of secrets
 * @param p_mech : the obfuscation mechanism
 * @param p_empdata: the observed empirical data. 
 */
	public Estimator_INVP (GridSecretsSpace p_xspace, Mechanism p_mech, GridEmpData p_empdata) {
		
		xspace = p_xspace;
		// get the mechanism matrix
		// This call validates also that the elements of xspace are the same as the mechanism grid. 
		pr_z_giv_x = xspace.get_mechanism_matrix(p_mech); 

		
		// validate also that the mechanism is compatible with p_empdata
		if ((p_empdata.hsize!= p_mech.hsize)||(p_empdata.vsize != p_mech.vsize)) {
			System.out.println("The emprical data grid is incompatible with the mechanism grid");
			System.exit(0);
		}

		// the empirical distribution: every element of emp_pr_z including zero entries.
		emp_pr_z = xspace.get_distribution_from_emp_data(p_empdata);
		//emp_pr_z = p_empdata.getCompleteEmpDistribution();
		//total_observations = p_empdata.total_observations;  // used to evaluate the likelihood function
	}

	public Estimator_INVP (GridSecretsSpace p_xspace, Mechanism p_mech, DoubleMatrix p_emp_distribution) {
		
		xspace = p_xspace;
		// get the mechanism matrix
		// This call validates also that the elements of xspace are the same as the mechanism grid. 
		pr_z_giv_x = xspace.get_mechanism_matrix(p_mech); 

		
		// validate also that the mechanism is compatible with p_empdata
		if ((p_emp_distribution.length != p_mech.hsize*p_mech.vsize)) {
			System.out.println("The emprical distribution is incompatible with the mechanism grid");
			System.exit(0);
		}

		// the empirical distribution: every element of emp_pr_z including zero entries.
		emp_pr_z = p_emp_distribution;
	}

	
	/**
     * Using matrix inversion method it computes an estimator on the elements of the secrets space (xspace), using 
     * the full empirical data distribution emp_pr_z, and the mechanism matrix (pr_z_giv_x). If the resulting vector 
     * has -ve components, it is corrected to a valid distrtibution using normalization.  
     * @return a distribution on the secrets space, as a row vector
     */
	public DoubleMatrix getMaximizer () {
		System.out.println("empirical = " + emp_pr_z);
		DoubleMatrix inv = Solve.solve(pr_z_giv_x, DoubleMatrix.eye(pr_z_giv_x.rows));
		DoubleMatrix pr_x = emp_pr_z.mmul(inv);
		System.out.println("estimated = "+ pr_x);
		System.out.println("prob sum = "+ pr_x.sum());
		//System.out.println("Size =" + pr_x.length);

		// projection
		double[] y = pr_x.toArray();
		Arrays.sort(y); // sorts the array in a ascending order
		double lambda =0;
		double sum_u =0;
		for (int j=1; j<=y.length; j++) {
			double u = y[y.length-j]; 
			sum_u = sum_u + u;
			if (u + (1- sum_u)/j >0) 
				lambda = (1- sum_u)/j; // this turns to be lambda in the end
		}
		pr_x.addi(lambda);
		pr_x.muli(pr_x.ge(0));    // set -ve entries to zeros
/*
		System.out.println("After projection:\n"+pr_x);
		System.out.println("Total probability = "+pr_x. sum());
		System.out.println("Size =" + pr_x.length);
		for (int i=0;i<pr_x.length;i++) 
			if (pr_x.get(i)<0)
				System.out.println("-ve element at "+i +":"+pr_x.get(i));
*/
		//System.out.println("euclidean distance to the -ve distribution = "+ pr_x.distance2(pr_x1));
		return pr_x;
    }


}
