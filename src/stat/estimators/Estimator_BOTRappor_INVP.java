package stat.estimators;

import priv.plan.mechanisms.*;
import stat.spaces.BitVectorEmpData;
import stat.spaces.GridSecretsSpace;

import java.util.Arrays;

import org.jblas.DoubleMatrix;

/**
 * The purpose of this class is to compute an estimator for the distribution on a given space of secrets, 
 * assuming a Rappor mechanism has be used for obfuscation. This estimator is the implementation of the 
 * method in [Kairouz, et al, 2016:discrete distribution estimation under local privacy], with applying 
 * projection to correct resulting negative values. 
 **/ 
public class Estimator_BOTRappor_INVP {
	
	private GridSecretsSpace xspace;   // space of secrets X, which we require to estimate its distribution.
	private DoubleMatrix emp_bit_probabilities;   
	// element j is the number of times that bit j is equal to 1 in the observed empirical bitVectors (Tj/n) in [Kairouz 2016] 
	private double eps;

	
/**
 * Construct an estimator for the distribution on a given space of secrets, assuming a Rappor mechanism 
 * has be used for obfuscation. This estimator is the implementation of the method in 
 * [Kairouz, et al, 2016:discrete distribution estimation under local privacy], with applying 
 * projection to correct resulting negative values.  
 * @param p_xspace : the space of secrets
 * @param p_rmech : the obfuscation Rappor mechanism
 * @param p_bitVectorEmpData: the observed empirical data. 
 */
	public Estimator_BOTRappor_INVP (GridSecretsSpace p_xspace, BasicOneTimeRappor p_rmech, BitVectorEmpData p_bitVectorEmpData) {
		
		xspace = p_xspace;
		// validate that the mechanism is compatible with p_empdata
		if ((p_xspace.hsize!= p_rmech.hsize)||(p_xspace.vsize != p_rmech.vsize)) {
			System.out.println("The grid space is incompatible with the given Rappor mechanism");
			System.exit(0);
		}
		eps = p_rmech.eps;

		// the element is just Tj/n where Tj is the number of bitVectors that have bit j equal to 1.
		// get for every bit Yj the empirical probability that Yj=1.
		emp_bit_probabilities = p_bitVectorEmpData.get_emp_bit_probabilities();
	}


	
	/**
     * This computes an estimator on the elements of the secrets space (xspace), using 
     * the method in [Kairouz, et al, 2016:discrete distribution estimation under local privacy], 
     * which also uses the inverse matrix of the k-RR mechanism.  
     * If the resulting vector has -ve components, it is corrected to a valid distribution using 
     * projection.  
     * @return a distribution on the secrets space, as a row vector
     */
	public DoubleMatrix getEstimator () {
		double c1 = Math.exp(0.5*eps)+1;
		double c2 = Math.exp(0.5*eps)-1;
		DoubleMatrix pr_x = emp_bit_probabilities.mul(c1/c2).sub(1/c2);
		System.out.println("estimated = "+ pr_x);

		// projection
		double[] y = pr_x.toArray();
		Arrays.sort(y); // sorts the array in a ascending order
		double lambda =0;
		double sum_u =0;
		for (int j=1; j<=y.length; j++) {
			double u = y[y.length-j]; 
			sum_u = sum_u + u;
			if (u + (1- sum_u)/j >0) 
				lambda = (1- sum_u)/j; // this turns to be lambda in the end
		}
		pr_x.addi(lambda);
		pr_x.muli(pr_x.ge(0));    // set -ve entries to zeros
		return pr_x;

    }

}
