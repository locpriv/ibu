package stat.estimators;

import priv.plan.mechanisms.*;
import stat.distributions.Distance;
import stat.distributions.Likelihood;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import org.jblas.DoubleMatrix;
/**
 * The purpose of this class is to evaluate the maximum likelihood distribution on a given space of secrets (xspace), 
 * using empirical data which describes a collection of locations and the (nonzero) frequency of every location, and 
 * the mechanism that produced this empirical data. Note that we assume here that a fixed mechanism is used. 
 */
public class Estimator_EM {
	
	private GridSecretsSpace xspace;   // space of secrets X, which we require to estimate its distribution.
	private DoubleMatrix pr_z_giv_x;   // Reduced G matrix:rows represent elements of X, columns correspond to elements of Z
	private DoubleMatrix emp_pr_z;     // a row vector, empirical distribution on Z
	private int total_observations;    // total number of observations that produced emp_pr_z
	
	private DoubleMatrix original_distribution_on_secrets; // a row vector to be compared to obtained distributions

	
/**
 * Construct an estimator for computing the max-likelihood distribution on a given space of secrets, an obfuscation
 * mechanism, and empirical data (with nonzero frequencies) resulting by obfuscation. 
 * @param p_xspace : the space of secrets
 * @param p_mech : the obfuscation mechanism
 * @param p_empdata: the observed empirical data. 
 */
	public Estimator_EM (GridSecretsSpace p_xspace, Mechanism p_mech, GridEmpData p_empdata) {
		
		xspace = p_xspace;
		// get the mechanism matrix rows restricted to the elements of xspace
		// This call validates also that the elements of xspace are covered by the mechanism grid. 
		DoubleMatrix xspace_mech_matrix = xspace.get_mechanism_matrix(p_mech); 

		
		// validate also that the mechanism is compatible with p_empdata
		if ((p_empdata.hsize!= p_mech.hsize)||(p_empdata.vsize != p_mech.vsize)) {
			System.out.println("The emprical data grid is incompatible with the mechanism grid");
			System.exit(0);
		}
		
		// get the mechanism matrix columns restricted to p_empdata, i.e. a slim version of the matrix G,
		// that results from reducing identical columns of G to one column that corresponds to a location in p_empdata. 
		// note we pass a DoubleMatrix (for performance) instead of a mechanism. This is why we do validation above.
		pr_z_giv_x = p_empdata.get_probability_matrix(xspace_mech_matrix);


		// the empirical distribution: every element of emp_pr_z has a nonzero value.
		emp_pr_z = p_empdata.getDistributionOnObservables();
		total_observations = p_empdata.total_observations;  // used to evaluate the likelihood function
	}
	
	private double getDistToOrigDistribution(DoubleMatrix p_pr_x, String p_distType) {
		
		if (p_distType.equals(Distance.EMD))
			return Distance.getEMDDist(original_distribution_on_secrets, p_pr_x, xspace);
		else if (p_distType.equals(Distance.TV))
			return Distance.getTotalVariationDist(original_distribution_on_secrets, p_pr_x);
		else if (p_distType.equals(Distance.KL))
			return Distance.getSymKLDiff(original_distribution_on_secrets, p_pr_x);
		else {
			System.out.println("EM error: invalide distance type");
			return -1;
		}
	}
    
	/**
     * computes the maximum-likelihood estimate on the elements of the secrets space (xspace), using 
     * the available empirical data distribution emp_pr_z, and the reduced G matrix (pr_z_giv_x). This 
     * computation starts with a uniform distribution on xspace. This method is used for debugging as 
     * to see the behaviour of the EM algorithm on a large number of iterations.
     * @param p_iterations: number of iterations to run
     * @param p_cycle: a number of iterations. After every cycle, the distance and likelihood are recorded.  
     * @param p_logLikelihoodFileName : file to store the progress of the likelihood with the iterations.
     * @param p_DistanceFileName : to store the progress of the progress of the distance to original distribution.
     * @param p_DifferenceFileName: to store differences (TV) between successive distributions in iterations (slope)
     * @return a distribution on the secrets space, as a row vector
     */
	public DoubleMatrix getMaximizer(int p_iterations, int p_cycle, String p_distType, 
			String p_logLikelihoodFileName, String p_DistanceFileName, String p_DifferenceFileName) {

    	BufferedWriter logLikelihoodPlotBufferedWriter = null;
    	BufferedWriter distancePlotBufferedWriter = null;
       	BufferedWriter diffPlotBufferedWriter = null;
		
		DoubleMatrix pr_x=null;   // the distribution that will be refined until convergence
    	try {
    		// Create files for tracking logLikelihood and distance to original distribution
    		logLikelihoodPlotBufferedWriter = new BufferedWriter(new FileWriter(p_logLikelihoodFileName));
    		if (original_distribution_on_secrets != null)
    			distancePlotBufferedWriter = new BufferedWriter(new FileWriter(p_DistanceFileName));
    		diffPlotBufferedWriter = new BufferedWriter(new FileWriter(p_DifferenceFileName));

    		// Initialize pr_x (a row vector):
    		// pr_x must not contain zeros: we use uniform distribution. 
    		// Also pr_x must not yield probability 0 for any element in 
    		// the empirical data, i.e. {pr_x.mmul(pr_z_giv_x).min()!=0}. This assures that complete data likelihood Q is defined at pr_x 
    		pr_x = xspace.get_uniform_distribution();  // a row vector

    		DoubleMatrix pr_z;        // current distribution on outputs
    		DoubleMatrix pr_x_and_z;  // current joint distribution of x,z
    		DoubleMatrix pr_x_giv_z;  // posterior distribution of x given z

    		// write initial log data to log files
    		logLikelihoodPlotBufferedWriter.write(0 +" "+ Likelihood.get_log_likelihood(pr_x, pr_z_giv_x, emp_pr_z, total_observations) + "\n");
    		// if original empirical data is available, the method logs the difference to the current distribution.  
    		if (original_distribution_on_secrets != null)
		        distancePlotBufferedWriter.write(0 +" "+ this.getDistToOrigDistribution(pr_x, p_distType)+ "\n");  

    		// Iterate the following steps for a given number of iterations or until likelihood difference 
    		// is small (evaluating likelihoods is not too expensive)
    		DoubleMatrix old_pr_x = pr_x;
    		for (int k=1;k<=p_iterations;k++)
    		{
    			// Compute a row vector containing the probability of 
    			// every z in the empirical data (only observed ones, not every z in Z)
    			// with respect to the current distribution, i.e. pr_z = pr_x.G
    			pr_z = pr_x.mmul(pr_z_giv_x);

    			// Construct a joint probability matrix where each entry is p(x,z)
    			// multiply (point-wise) every column of pr_z_giv_x by pr_x in a column form, i.e. transposed.   
    			pr_x_and_z = pr_z_giv_x.mulColumnVector(pr_x.transpose());

    			// Construct a conditional matrix of the form P(x|z), with z indexing rows and x indexing columns
    			pr_x_giv_z = (pr_x_and_z.divRowVector(pr_z)).transpose();

    			// update the current distribution
    			pr_x = emp_pr_z.mmul(pr_x_giv_z);

    			// write the iteration results to log files
    			logLikelihoodPlotBufferedWriter.write(k +" "+ Likelihood.get_log_likelihood(pr_x, pr_z_giv_x, emp_pr_z, total_observations) + "\n");
    			if (k % p_cycle == 0 && original_distribution_on_secrets != null) {
    				distancePlotBufferedWriter.write(k +" "+ this.getDistToOrigDistribution(pr_x,p_distType)+ "\n"); 
    				System.out.println("Processing iteration "+k+" ...");
    				double diff =  Distance.getTotalVariationDist(pr_x, old_pr_x);
    				diffPlotBufferedWriter.write(k +" "+ diff + "\n");
    				old_pr_x = pr_x;
    			}
    		}

    	} catch (IOException ex) {
    		ex.printStackTrace();
    	} finally {
    		//Close the BufferedWrites
    		try {
    			if (logLikelihoodPlotBufferedWriter != null) {
    				logLikelihoodPlotBufferedWriter.flush();
    				logLikelihoodPlotBufferedWriter.close();
    			}
    			if (distancePlotBufferedWriter != null) {
    				distancePlotBufferedWriter.flush();
    				distancePlotBufferedWriter.close();
    			}
    			if (diffPlotBufferedWriter != null) {
    				diffPlotBufferedWriter.flush();
    				diffPlotBufferedWriter.close();
    			}
    		} catch (IOException ex) {
    			ex.printStackTrace();
    		}
    	}

    	return pr_x;

    }
	
	
	/**
     * computes the maximum-likelihood estimate on the elements of the secrets space (xspace), using 
     * the available empirical data distribution emp_pr_z, and the reduced G matrix (pr_z_giv_x). This 
     * computation starts with a uniform distribution on xspace. 
     * @param p_iterations
     * @return a distribution on the secrets space, as a row vector
     */
	public DoubleMatrix getMaximizer(int p_iterations) {
		
		DoubleMatrix pr_x=null;   // the distribution that will be refined until convergence
    		pr_x = xspace.get_uniform_distribution();  // a row vector

    		DoubleMatrix pr_z;        // current distribution on outputs
    		DoubleMatrix pr_x_and_z;  // current joint distribution of x,z
    		DoubleMatrix pr_x_giv_z;  // posterior distribution of x given z

    		for (int k=1;k<=p_iterations;k++)
    		{
    			pr_z = pr_x.mmul(pr_z_giv_x);
    			pr_x_and_z = pr_z_giv_x.mulColumnVector(pr_x.transpose());
    			pr_x_giv_z = (pr_x_and_z.divRowVector(pr_z)).transpose();
    			pr_x = emp_pr_z.mmul(pr_x_giv_z);
    		}
    	return pr_x;
    }
	
	/**
	 * Iterates until p_min_diff between iterations is within p_min_diff 
	 * @param p_min_diff
	 * @return
	 */
	public DoubleMatrix getMaximizer(double p_min_diff, int p_max_iterations) {

		DoubleMatrix old_pr_x; 
		DoubleMatrix pr_x=null;   // the distribution that will be refined until convergence
		double err;
		pr_x = xspace.get_uniform_distribution();  // a row vector
		old_pr_x = pr_x;

		DoubleMatrix pr_z;        // current distribution on outputs
		DoubleMatrix pr_x_and_z;  // current joint distribution of x,z
		DoubleMatrix pr_x_giv_z;  // posterior distribution of x given z
		int i=0;
		do 
		{
			i++;  // count the iterations
			pr_z = pr_x.mmul(pr_z_giv_x);
			pr_x_and_z = pr_z_giv_x.mulColumnVector(pr_x.transpose());
			pr_x_giv_z = (pr_x_and_z.divRowVector(pr_z)).transpose();
			pr_x = emp_pr_z.mmul(pr_x_giv_z);
			err = Distance.getTotalVariationDist(pr_x, old_pr_x);
			old_pr_x = pr_x;
		} while (err > p_min_diff && i< p_max_iterations);
		System.out.println("EM procedure terminated at iteration "+ i +" with error "+ err);
		return pr_x;
	}
	
	/**
	 * Starts with p_init_pr_x and iterates until p_min_diff (between successive iterations) is within p_min_diff 
	 * @param p_init_pr_x
	 * @param p_min_diff
	 * @return
	 */
	public DoubleMatrix getMaximizer(DoubleMatrix p_init_pr_x, double p_min_diff, int p_max_iterations) {

		DoubleMatrix old_pr_x; 
		DoubleMatrix pr_x=null;   // the distribution that will be refined until convergence
		double err;
		pr_x = p_init_pr_x;  // a row vector
		old_pr_x = pr_x;

		DoubleMatrix pr_z;        // current distribution on outputs
		DoubleMatrix pr_x_and_z;  // current joint distribution of x,z
		DoubleMatrix pr_x_giv_z;  // posterior distribution of x given z
		int i=0;
		do 
		{
			i++;  // count the iterations
			pr_z = pr_x.mmul(pr_z_giv_x);
			pr_x_and_z = pr_z_giv_x.mulColumnVector(pr_x.transpose());
			pr_x_giv_z = (pr_x_and_z.divRowVector(pr_z)).transpose();
			pr_x = emp_pr_z.mmul(pr_x_giv_z);
			err = Distance.getTotalVariationDist(pr_x, old_pr_x);
			old_pr_x = pr_x;
		} while (err > p_min_diff && i< p_max_iterations);
		System.out.println("EM procedure terminated at iteration "+ i +" with error "+ err);
		return pr_x;
	}

	/** set the original distribution if needed to track the distance to the 
	 * distribution at every iteration.
	 * @param p_orig_dist
	 */
	public void setOriginalDistributionOnSecrets(DoubleMatrix p_orig_dist) {
		original_distribution_on_secrets = p_orig_dist;
	}

}
