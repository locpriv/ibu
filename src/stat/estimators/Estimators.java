package stat.estimators;

import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.Mechanism;
import priv.plan.mechanisms.BasicOneTimeRappor;
import priv.plan.mechanisms.PRRespMechanism;
import priv.plan.priors.Prior;
import stat.distributions.Distance;
import stat.spaces.BitVectorEmpData;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;

public class Estimators {
	public static final String EM = "EM";
	public static final String INVN = "INVN";  // INV with normalization
	public static final String INVP = "INVP";  // INV with projection
	public static final String KRR_INVN = "KRR_INVN"; // INVN using equations of [Kairouz,2016]
	public static final String KRR_INVP = "KRR_INVP"; // INVP using equations of [Kairouz,2016]
	public static final String BOTRAPPOR_EM = "BOTRAPPOR_EM";
	public static final String BOTRAPPOR_INVN = "BOTRAPPOR_INVN";
	public static final String BOTRAPPOR_INVP = "BOTRAPPOR_INVP";
	
	static int em_max_iterations = 50000; 
	
	/** 
	 * Estimates the real distribution on the given xspace using the given noisy_empdata and mechanism and the estimator type
	 * to use. The resulting distribution is stored in a file using p_results_base_folder and the p_file_tag. 
	 * p_original_empdata is used to evaluate the (TV) distance from the original distribution to the estimated one. This 
	 * distance value is stored in the resulting distribution file.  
	 * @param p_xspace
	 * @param p_mech
	 * @param p_noisy_empdata
	 * @param p_estimatorType
	 * @param p_original_empdata
	 * @param p_results_base_folder
	 * @param p_file_tag
	 */
	public static void estimate_distribution_em(GridSecretsSpace p_xspace, Mechanism p_mech, GridEmpData p_noisy_empdata, 
			double p_em_error, GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating using "+ EM + " ....");
		System.out.println("number of different locations in noisy data = " + p_noisy_empdata.data.size());

		// Constructing EM starting distribution
		//	DoubleMatrix init_pr_x = DoubleMatrix.ones(100).transpose();
		//	init_pr_x.put(50, 1.5);
		//	double sum = init_pr_x.sum();
		//	init_pr_x.divi(sum);
			
			Estimator_EM em = new Estimator_EM(p_xspace, p_mech, p_noisy_empdata);
			pr_x = em.getMaximizer(p_em_error, em_max_iterations);
			//  use the following to produce distance and likelihood plots with iterations
			//      em.setOriginalDistributionOnSecrets(xspace.get_distribution_from_emp_data(original_empdata));
			//		pr_x = em.getMaximizer(em_iterations, em_cycle, distType,logLikelihood_filename_prefix+"_"+p_file_tag, 
			//                                            distance_filename_prefix+"_"+p_file_tag, 
			//                                            difference_filename_prefix+"_"+p_file_tag);

		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);

		System.out.println("Statustical distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+ EM +"_"+p_file_tag, true);
	}

	public static void estimate_distribution_invn(GridSecretsSpace p_xspace, Mechanism p_mech, GridEmpData p_noisy_empdata, 
			GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating using "+INVN+" ....");
			Estimator_INVN invn = new Estimator_INVN(p_xspace, p_mech, p_noisy_empdata);
			pr_x = invn.getMaximizer();
		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		
		System.out.println("Distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+INVN+"_"+p_file_tag, true);
	}
	
	public static void estimate_distribution_invp(GridSecretsSpace p_xspace, Mechanism p_mech, GridEmpData p_noisy_empdata, 
			GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating using "+INVP+"....");
		Estimator_INVP invp = new Estimator_INVP(p_xspace, p_mech, p_noisy_empdata);
			pr_x = invp.getMaximizer();

		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		
		
		System.out.println("Distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+INVP+"_"+p_file_tag, true);
	}
	
	/** 
	 * This estimates the original distribution using the matrix inverse closed-form equations
	 * described in [Kairouz, 2016], with normalization.
	 * @param p_xspace
	 * @param p_mech
	 * @param p_noisy_empdata
	 * @param p_original_empdata
	 * @param p_distType
	 * @param p_results_base_folder
	 * @param p_file_tag
	 */
	public static void estimate_distribution_krr_invn(GridSecretsSpace p_xspace, PRRespMechanism p_mech, GridEmpData p_noisy_empdata, 
			GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating using "+KRR_INVN+" ....");
			Estimator_KRR_INVN invn = new Estimator_KRR_INVN(p_xspace, p_mech, p_noisy_empdata);
			pr_x = invn.getMaximizer();
		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		
		System.out.println("Distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+KRR_INVN+"_"+p_file_tag, true);
	}
	
	/**
	 * This estimates the original distribution using the matrix inverse closed-form equations
	 * described in [Kairouz, 2016], with projection.
	 * @param p_xspace
	 * @param p_mech
	 * @param p_noisy_empdata
	 * @param p_original_empdata
	 * @param p_distType
	 * @param p_results_base_folder
	 * @param p_file_tag
	 */
	public static void estimate_distribution_krr_invp(GridSecretsSpace p_xspace, PRRespMechanism p_mech, GridEmpData p_noisy_empdata, 
			GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating using "+KRR_INVP+"....");
		Estimator_KRR_INVP invp = new Estimator_KRR_INVP(p_xspace, p_mech, p_noisy_empdata);
			pr_x = invp.getMaximizer();

		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		
		
		System.out.println("Distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+KRR_INVP+"_"+p_file_tag, true);
	}
	

	public static void estimate_distribution_rappor_em(GridSecretsSpace p_xspace, BasicOneTimeRappor p_rmech, BitVectorEmpData p_noisy_empdata, 
			double p_em_error, GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating with Rappor mechanism using "+ BOTRAPPOR_EM + " ....");
		System.out.println("number of different locations in noisy data = " + p_noisy_empdata.data.size());

		// Constructing EM starting distribution
		//	DoubleMatrix init_pr_x = DoubleMatrix.ones(100).transpose();
		//	init_pr_x.put(50, 1.5);
		//	double sum = init_pr_x.sum();
		//	init_pr_x.divi(sum);
			
			Estimator_BOTRappor_EM em = new Estimator_BOTRappor_EM(p_xspace, p_rmech, p_noisy_empdata);
			pr_x = em.getMaximizer(p_em_error, em_max_iterations);
			//  use the following to produce distance and likelihood plots with iterations
			//      em.setOriginalDistributionOnSecrets(xspace.get_distribution_from_emp_data(original_empdata));
			//		pr_x = em.getMaximizer(em_iterations, em_cycle, distType,logLikelihood_filename_prefix+"_"+p_file_tag, 
			//                                            distance_filename_prefix+"_"+p_file_tag, 
			//                                            difference_filename_prefix+"_"+p_file_tag);

		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);

		System.out.println("Statustical distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+ BOTRAPPOR_EM +"_"+p_file_tag, true);

	}

	

	/**
	 * estimate the distibution on xspace using a Rappor mechanism and bitVector empirical data. 
	 * Distance to the original distribution is evaluated using the given distType. 
	 * @param p_xspace
	 * @param p_rmech
	 * @param p_noisy_empdata
	 * @param p_original_empdata
	 * @param p_distType
	 * @param p_results_base_folder
	 * @param p_file_tag
	 */
	public static void estimate_distribution_rappor_invn(GridSecretsSpace p_xspace, BasicOneTimeRappor p_rmech, BitVectorEmpData p_noisy_empdata, 
			GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating with Rappor using "+BOTRAPPOR_INVN+" ....");
			Estimator_BOTRappor_INVN invn = new Estimator_BOTRappor_INVN(p_xspace, p_rmech, p_noisy_empdata);
			pr_x = invn.getEstimator();


		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		
		System.out.println("Distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+BOTRAPPOR_INVN+"_"+p_file_tag, true);

	}
	
	
	public static void estimate_distribution_rappor_invp(GridSecretsSpace p_xspace, BasicOneTimeRappor p_rmech, BitVectorEmpData p_noisy_empdata, 
			GridEmpData p_original_empdata, String p_distType, String p_results_base_folder, String p_file_tag) {
		// estimate and produce plot data
		DoubleMatrix pr_x=null;
		System.out.println("\nestimating using "+BOTRAPPOR_INVP+"....");
		Estimator_BOTRappor_INVP invp = new Estimator_BOTRappor_INVP(p_xspace, p_rmech, p_noisy_empdata);
		
		// original distribution is passed for debugging.. remove later
		pr_x = invp.getEstimator();

		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the estimated distribution");
		double statDistance=0;
		if (p_distType.equals(Distance.TV)) 
			statDistance = Distance.getTotalVariationDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			statDistance= Distance.getEMDDist(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x, p_xspace);
		else if (p_distType.equals(Distance.KL)) 
			statDistance= Distance.getSymKLDiff(p_xspace.get_distribution_from_emp_data(p_original_empdata), pr_x);
		
		
		System.out.println("Distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(pr_x.transpose(), p_xspace.hsize,p_xspace.vsize, p_xspace.cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(p_results_base_folder+"distribution_estimated_"+BOTRAPPOR_INVP+"_"+p_file_tag, true);

	}


}
