package stat.estimators;

import priv.plan.mechanisms.*;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;
import org.jblas.DoubleMatrix;

/**
 * The purpose of this class is to evaluate the maximum likelihood estimator for KRR mechanism on a given 
 * space of secrets (xspace), using the inverse matrix using the closed form Equation (6) in [Kairouz, et al 2016] 
 * with normalization.
 * This class uses empirical data which describes a collection of locations and the (nonzero) 
 * frequency of every location, and the mechanism that produced this empirical data. Note that we assume here that a 
 * kRR mechanism is used. 
 */
public class Estimator_KRR_INVN {
	
	private GridSecretsSpace xspace;   // space of secrets X, which we require to estimate its distribution.
	private DoubleMatrix emp_pr_z;     // a row vector, empirical distribution on Z
	
	double eps;

	
/**
 * Construct an estimator for computing the max-likelihood distribution on a given space of secrets, an obfuscation
 * mechanism, and empirical data (including zero frequencies) resulting by obfuscation. 
 * @param p_xspace : the space of secrets
 * @param p_mech : the obfuscation mechanism
 * @param p_empdata: the observed empirical data. 
 */
	public Estimator_KRR_INVN (GridSecretsSpace p_xspace, PRRespMechanism p_mech, GridEmpData p_empdata) {
		
		eps = p_mech.get_eps();
		xspace = p_xspace;
		
		// validate also that the mechanism is compatible with p_empdata
		if ((p_empdata.hsize!= p_mech.hsize)||(p_empdata.vsize != p_mech.vsize)) {
			System.out.println("The emprical data grid is incompatible with the mechanism grid");
			System.exit(0);
		}

		// the empirical distribution: every element of emp_pr_z including zero entries.
		emp_pr_z = xspace.get_distribution_from_emp_data(p_empdata);
		//emp_pr_z = p_empdata.getCompleteEmpDistribution();
		//total_observations = p_empdata.total_observations;  // used to evaluate the likelihood function
	}

	public Estimator_KRR_INVN (GridSecretsSpace p_xspace, PRRespMechanism p_mech, DoubleMatrix p_emp_distribution) {
		
		xspace = p_xspace;
		eps = p_mech.get_eps();

		
		// validate also that the mechanism is compatible with p_empdata
		if ((p_emp_distribution.length != p_mech.hsize*p_mech.vsize)) {
			System.out.println("The emprical distribution is incompatible with the mechanism grid");
			System.exit(0);
		}

		// the empirical distribution: every element of emp_pr_z including zero entries.
		emp_pr_z = p_emp_distribution;
	}

	
	/**
     * Using matrix inversion method it computes an estimator on the elements of the secrets space (xspace), using 
     * the full empirical data distribution emp_pr_z, and the inverse matrix of kRR (See Eq 6 in Kairouz, 2016]. 
     * If the resulting vector has -ve components, it is corrected to a valid distribution using normalization.  
     * @return a distribution on the secrets space, as a row vector
     */
	public DoubleMatrix getMaximizer () {
		//DoubleMatrix inv = Solve.solve(pr_z_giv_x, DoubleMatrix.eye(pr_z_giv_x.rows));
		//DoubleMatrix pr_x = emp_pr_z.mmul(inv);
		int k= xspace.hsize*xspace.vsize;
		double c1 = (Math.exp(eps)+k-1)/(Math.exp(eps)-1); 
		double c2 = 1.0/(Math.exp(eps)-1);
		DoubleMatrix pr_x = (emp_pr_z.mmul(c1)).sub(c2);
        // normalization
		pr_x.muli(pr_x.ge(0));    // set -ve entries to zeros
		double sum = pr_x.sum();
		pr_x.divi(sum);
		//System.out.println("euclidean distance to the -ve distribution = "+ pr_x.distance2(pr_x1));
		return pr_x;
    }


}
