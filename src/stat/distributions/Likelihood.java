package stat.distributions;

import org.jblas.DoubleMatrix;

public class Likelihood {
	
	/** 
	 * Computes the log-probability of empdata given a distribution over secrets p_pr_x
	 * and probabilty matrix pr_z_giv_x, and empirical distribution p_emp_pr_z, and the total number of observations 
	 * @param p_pr_x : a given distribution on the secret space
	 * @param p_pr_z_giv_x
	 * @param p_emp_pr_z
	 * @param p_totalObservations
	 * @return log-likelihood of p_pr_x
	 */
	public static double get_log_likelihood(DoubleMatrix p_pr_x, DoubleMatrix p_pr_z_giv_x,DoubleMatrix p_emp_pr_z , int p_totalObservations) {
		double ret=0;
		for (int j=0;j<p_emp_pr_z.length;j++)
			ret = ret + p_emp_pr_z.get(j)* Math.log(p_pr_x.dot( p_pr_z_giv_x.getColumn(j) ) );
		return ret* p_totalObservations;
		//return ret;
		// return Math.exp(ret);
		// return Math.exp(ret* p_totalObservations); // return the probability rather than log-Probability
	}

	
}
