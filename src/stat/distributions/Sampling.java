package stat.distributions;

import java.util.Arrays;

public class Sampling {
	
	/** 
	 * returns an array of q integers sampled from a probability distribution p_probabilities
	 * @param p_probabilities
	 * @param q
	 * @return
	 */
	public static int [] sample(double [] p_probabilities, int q) {
        double[] cdf = new double[p_probabilities.length];
        cdf[0] = p_probabilities[0]; 
        for (int i = 1; i < cdf.length; i++)
            cdf[i] = cdf[i - 1] + p_probabilities[i];  // note that cdf is sorted (ascending)

        int [] samples = new int[q];
        for (int i = 0; i < q; i++) {
            samples[i] = Arrays.binarySearch(cdf, Math.random());
            // if the given value is not found in cdf, binary search returns -i-1 
            // where i is the insertion point (the position where 
            // the given value should be inserted to keep the array cdf sorted.  
            if (samples[i]<0) samples[i] = Math.abs(samples[i]+1);
            if (samples[i]==cdf.length) samples[i]= samples[i]-1; 
        }

        return samples;
    }
		
	
	/**
	 * samples an integer between 0 and n using binomial distributions with parameters n,p
	 * @param n
	 * @param p
	 * @return
	 */
	public static int getBinomial(int p_maxnum, double p) {
		int x = 0;
		for(int i = 0; i < p_maxnum; i++) {
			if(Math.random() < p)
				x++;
		}
		return x;
	}

	/**
	 * samples an integer between 0 and n using the uniform. 
	 * @param p_min
	 * @param p_max
	 * @return
	 */
	public static int getUniform(int p_min, int p_max) {
		return (int)(Math.random() * ((p_max - p_min) + 1)) + p_min; 
	}
}
