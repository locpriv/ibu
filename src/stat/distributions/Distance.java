package stat.distributions;

import org.jblas.DoubleMatrix;
import femd.emd_hat;
import priv.plan.loss.LinGeoLoss;
import stat.spaces.GridSecretsSpace;

public class Distance {
	
    public static final String TV = "TV";
    public static final String KL  = "KL";
	public static final String EMD = "EMD";
	
	static LinGeoLoss loss;  // used to compute the EMD distance
	private static double[][] lossDArray;     // the ground distance between every two elements in xpace to compute earth mover's distance. 

	
	/** 
	 * returns L1-norm distance between two distributions. Note that the total variation distance TV is half of the returned distance
	 * 
	 * @param p_distribution1
	 * @param p_distribution2
	 * @return L1-norm-1 distance
	 */
    public static double getNorm1Dist(DoubleMatrix p_distribution1, DoubleMatrix p_distribution2) {
		return p_distribution1.distance1(p_distribution2); 
	}
   
	/** 
	 * returns total variation distance between two distributions. 
	 * Note that the total variation distance TV is half of the L1-norm distance (for discrete distributions).
	 * 
	 * @param p_distribution1
	 * @param p_distribution2
	 * @return TV distance
	 */
    public static double getTotalVariationDist(DoubleMatrix p_distribution1, DoubleMatrix p_distribution2) {
		   return  0.5*p_distribution1.distance1(p_distribution2);
	}
    
    
    // Using jni library
    private static double getEMDDist(DoubleMatrix p_grid_distribution_data1, DoubleMatrix p_grid_distribution_data2, double[][] p_cost) {
    	/*
    	double p1_zeros =  p_grid_distribution_data1.not().sum();
    	double p2_zeros =  p_grid_distribution_data2.not().sum();
    	double p1_p2_zeros =  p_grid_distribution_data1.not().and(p_grid_distribution_data2.not()).sum();
    	System.out.println("the vectors have sizes "+ p_grid_distribution_data1.length +","+ p_grid_distribution_data1.length);
    	System.out.println("p1_z = " + p1_zeros + " p2_z = " + p2_zeros + " p1_p2_z = " + p1_p2_zeros);
    	*/
    	return emd_hat.dist_gd_metric(p_grid_distribution_data1.toArray(), p_grid_distribution_data2.toArray(), p_cost, -1, null);
    }
    
    
    /**
     * returns the EMD distance between two distributions given the gridSecretSpace on which the distributions are defined
     * @param p_grid_distribution1
     * @param p_grid_distribution2
     * @param p_xspace
     * @return
     */
    public static double getEMDDist(DoubleMatrix p_grid_distribution1, DoubleMatrix p_grid_distribution2, GridSecretsSpace p_xspace) {
		if (lossDArray == null) {
			double maxDist = p_xspace.cell_side*Math.sqrt(p_xspace.hsize*p_xspace.hsize+p_xspace.vsize*p_xspace.vsize);
			loss = new LinGeoLoss(maxDist,maxDist);   // euclidean loss function
			lossDArray = loss.get_loss_DoubleArray(p_xspace.hsize, p_xspace.vsize, p_xspace.cell_side);
		}
		double ret_distance = getEMDDist(p_grid_distribution1, p_grid_distribution2, lossDArray);
		return ret_distance;
    }

	/**
	 * Returns the Symmetric Kullback-Liebler measure from the 1st distribution to the 2nd one.
	 * @param p_distribution1  first distribution row vector
	 * @param p_distribution2  2nd distribution row vector
	 * @return KL divergence
	 * 	zero probabilities in the 1st distribution (the original) are not an issue because pi log pi 
	 *  tends to 0, so they are handled by adding 0 for these components. However the zero components 
	 *  in the second distributions make pi log qi diverge. In privacy this situation happens when 
	 *  the cell size is small (e.g. 0.2km) or the number of samples is small (many cells do not include 
	 *  noisy checkins). In this case, we smooth the KL measure when qi=0 by setting q = a.U + (1-a).q, where 
	 *  U is the uniform distribution and a is a small probability such that a << 1/grid_size to keep 
	 *  the resulting distribution far from the uniform distribution in which qi=1/grid_size 
	 */
    public static double getSymKLDiff(DoubleMatrix p_distribution1, DoubleMatrix p_distribution2) {
		double sum=0; 
		double p; double q; 
		double a = 1e-8;  // smoothing factor. It should be much smaller than uniform probability, i.e. 1/grid_size
		DoubleMatrix distribution1;
		DoubleMatrix distribution2;
//		System.out.println("distribution sums = "+ p_distribution1.sum() +", "+ p_distribution2.sum());
		if (p_distribution1.length != p_distribution2.length) {
			System.out.println("the given distributions must have the same length");
			System.exit(0);
		}
		distribution1 = p_distribution1;
		distribution2 = p_distribution2;

		// smooth distribution2
		if (distribution2.min()< a) {  // smoothing
			int l= distribution2.length;
			distribution2 = DoubleMatrix.ones(l).divi(l).transpose();
			distribution2 = distribution2.mul(a).add(p_distribution2.mul(1-a));
		}
		// smooth distribution1
		if (distribution1.min()< a) {  // smoothing
			int l= distribution1.length;
			distribution1 = DoubleMatrix.ones(l).divi(l).transpose();
			distribution1 = distribution1.mul(a).add(p_distribution1.mul(1-a));
		}

		
		for (int i=0;i<distribution1.length;i++) {
    		p = distribution1.get(i); 
    		q = distribution2.get(i);
    		if (p>0)
    			sum = sum + p * Math.log(p/q)/Math.log(2); //returns the result in bits;
    		if (q>0)
    			sum = sum + q * Math.log(q/p)/Math.log(2); //returns the result in bits;
		}
    	return sum;
	}


}
