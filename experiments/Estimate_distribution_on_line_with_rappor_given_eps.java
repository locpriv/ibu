package experiments;

import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.BasicOneTimeRappor;
import priv.plan.priors.Prior;
import stat.distributions.Distance;
import stat.distributions.Distributions;
import stat.distributions.Sampling;
import stat.estimators.*;
import stat.spaces.BitVectorEmpData;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;

/**
 * 
 * Used to plot estimated distributions with basic one-time rappor on a range of integers (EM paper 2019).
 *  
 * In this experiment data are sampled from the set {0,1,..., 9} using a given hypothetical distribution. 
 * The data are obfuscated by a BOTRAPPOR mechanism with eps=0.5 to generate noisy data. Noisy distribution
 * is induced by the frequencies of bits in the noisy bit vectors. The noisy data are then used by 
 * BOTRAPPOR_INVN, BOTRAPPOR_INVP, BOTRAPPOR_EM estimators to learn the original distribution 
 * (on the original data). We use two distributions to sample the original data: Binomial with p=0.5, and 
 * Uniform on {3,4,5,6} and 0 otherwise. The plot data of the original and noisy distributions are stored, 
 * in addition to the estimated ones.
 * 
 * We used this experiment with the following configurations:
 * For Figure 15(a): use samplingDistributionType = Distributions.BINOMIAL
 * For Figure 15(b): use samplingDistributionType = Distributions.UNIFORM
 * Both figures with: samples = 100000, eps = 0.5
 * 
 */
public class Estimate_distribution_on_line_with_rappor_given_eps {

	static final double cell_side =1;
	static final int hsize=1;   // to force the grid to be one dimensional
	static int vsize=10;
	static int samples = 100000;   
	static double eps = 0.5;
	static String samplingDistributionType = Distributions.BINOMIAL;

	
	// folder/files information for output data
	// options to use: temp, geom, krr, botrappor. 
	static String results_base_folder = "./results/linear_space/invn_invp_em_fixed_eps/temp/"+samplingDistributionType +"/";    // used to place the distributions and data files
	// the following files are used if we want to record em information at every iteration 
	//static String logLikelihood_filename_prefix = results_base_folder+"logLikelihood";   // likelihood plot file prefix
	//static String distance_filename_prefix = results_base_folder+"distance_"+distType;             // distance plot file prefix
	//static String difference_filename_prefix =results_base_folder + "diff";              // difference between em iterations plot file prefix

	
	static GridEmpData original_empdata;
	static GridSecretsSpace xspace;

	public static void main(String[] args) {
		
		// create an array of frequencies for cells 0 .. vsize-1
	    int[] freq_array = new int[vsize];		
	    if (samplingDistributionType.equals(Distributions.BINOMIAL)) 
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getBinomial(vsize-1, 0.5);
			freq_array[sample]++;
			}
	    else if (samplingDistributionType.contentEquals(Distributions.UNIFORM)) {
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getUniform( (int)(0.3*vsize)  , (int)(0.6*vsize) ) ;  
			freq_array[sample]++;
			}
	    }
				
		original_empdata = new GridEmpData(hsize, vsize, freq_array);

		System.out.println("Constructing the space of secrets as a vector of locations ...");
		xspace = new GridSecretsSpace(hsize, vsize, cell_side);

		
		System.out.println("\nWriting original empirical distribution ("+ original_empdata.data.size() + " cells have nonzero frequencies)");
		DoubleMatrix original_distribution = xspace.get_distribution_from_emp_data(original_empdata);
		new Prior(original_distribution.transpose(), hsize,vsize, cell_side)
		.write_prior_to_file(results_base_folder + "distribution_original_"+samplingDistributionType+".txt",false);

		
		//  mechanism
		BasicOneTimeRappor mech = new BasicOneTimeRappor(hsize, vsize, cell_side, eps);
		String file_tag = "BOTRAPPOR"+"_eps"+eps+"_samples"+samples+"_"+ samplingDistributionType+".txt";
        
		// Obfuscate original data using the mechanism
		System.out.println("Constructing noisy empirical data...");
		//GridEmpData noisy_empdata = new GridEmpData(original_empdata,mech);
		BitVectorEmpData noisy_empdata = new BitVectorEmpData(original_empdata, mech);
		//System.out.println("number of different bitVectors = " + noisy_empdata.data.size());
		
		//DoubleMatrix noisy_distribution = xspace.get_distribution_from_emp_data(noisy_empdata);
		DoubleMatrix noisy_distribution = xspace.get_distribution_from_bitVectEmpData(noisy_empdata);
		
		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the noisy distribution");
		double statDistance = Distance.getTotalVariationDist(xspace.get_distribution_from_emp_data(original_empdata), noisy_distribution);
		System.out.println("Total Variation distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(noisy_distribution.transpose(), hsize,vsize, cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(results_base_folder+"distribution_noisy"+"_"+file_tag, true);
		
		Estimators.estimate_distribution_rappor_em(xspace, mech, noisy_empdata, 0.0001, original_empdata, Distance.TV, results_base_folder, file_tag);
		Estimators.estimate_distribution_rappor_invn(xspace, mech, noisy_empdata, original_empdata, Distance.TV,results_base_folder, file_tag);
		Estimators.estimate_distribution_rappor_invp(xspace, mech, noisy_empdata, original_empdata, Distance.TV,results_base_folder, file_tag);
	}
	
}
