package experiments;

import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.PRRespMechanism;
import priv.plan.mechanisms.TPLapMechanism;
import priv.line.mechanisms.TGeomMechanism;
import priv.plan.mechanisms.ExponentialMechanism;
import priv.plan.mechanisms.Mechanism;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;
import stat.distributions.Distance;
import stat.distributions.Distributions;
import stat.distributions.Sampling;
import stat.estimators.*;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;

/**
 * Used to plot the distribution on a range of integers (EM paper 2019).
 *  
 * This experiment data are sampled from the set {0,1,..., 99} using a given hypothetical distribution. 
 * The data are obfuscated by a given mechanism with eps=0.1 to generate noisy data. The noisy data are 
 * used by INVN, INVP, EM estimators to learn the original distribution (on the original data). We use 
 * two distributions to sample the original data: Binomial with p=0.5, and 
 * Uniform on {20, ...,40} and 0 otherwise. 
 * The plot data of the original and noisy distributions are stored, 
 * in addition to the estimated ones produced by EM, INVN, INVP
 * 
 * We used this experiment with the following configurations:
 * For Figure 1(a), 1(c), 1(e) use mechType = Mechanism.GEOM, samplingDistributionType = Distributions.BINOMIAL
 * For Figure 1(b), 1(d), 1(f) use mechType = Mechanism.GEOM, distanceType = Distributions.UNIFORM
 * 
 */
public class Estimate_distribution_on_line_given_eps {
	static final double cell_side =1;
	static final int hsize=1;   // to force the grid to be one dimensional
	static int vsize=100;

	// perform EM estimation with error 0.00001; 
	// this smaller value (compared to the grid) is needed to detect discontinuities in uniform distribution.
	static int samples = 100000;   
	static double eps = 0.1;
	static String samplingDistributionType = Distributions.UNIFORM;
	static String mechType = Mechanism.GEOM;

	
	// folder/files information for output data
	// options to use: temp, geom, krr, botrappor. 
	static String results_base_folder = "./results/linear_space/invn_invp_em_fixed_eps/temp/"+samplingDistributionType +"/";    // used to place the distributions and data files
	// the following files are used if we want to record em information at every iteration 
	//static String logLikelihood_filename_prefix = results_base_folder+"logLikelihood";   // likelihood plot file prefix
	//static String distance_filename_prefix = results_base_folder+"distance_"+distType;             // distance plot file prefix
	//static String difference_filename_prefix =results_base_folder + "diff";              // difference between em iterations plot file prefix

	
	static GridEmpData original_empdata;
	static GridSecretsSpace xspace;

	public static void main(String[] args) {
		
		// create an array of frequencies for cells 0 .. vsize-1
	    int[] freq_array = new int[vsize];		
	    if (samplingDistributionType.equals(Distributions.BINOMIAL)) 
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getBinomial(vsize-1, 0.5);
			freq_array[sample]++;
			}
	    else if (samplingDistributionType.contentEquals(Distributions.UNIFORM)) {
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getUniform( (int)(0.2*vsize)  , (int)(0.4*vsize) ) ;  //geometric: use 0.2..0.4. Rappor: 0.3 .. 0.6
			freq_array[sample]++;
			}
	    }
				
		original_empdata = new GridEmpData(hsize, vsize, freq_array);

		System.out.println("Constructing the space of secrets as a vector of locations ...");
		xspace = new GridSecretsSpace(hsize, vsize, cell_side);

		
		System.out.println("\nWriting original empirical distribution ("+ original_empdata.data.size() + " cells have nonzero frequencies)");
		DoubleMatrix original_distribution = xspace.get_distribution_from_emp_data(original_empdata);
		new Prior(original_distribution.transpose(), hsize,vsize, cell_side)
		.write_prior_to_file(results_base_folder + "distribution_original_"+samplingDistributionType+".txt",false);

		
		Mechanism mech=null;;
		String file_tag="";
		//  mechanism
		if (mechType.equals(Mechanism.GEOM)) {
			mech = new TGeomMechanism(vsize,cell_side,eps);
			file_tag = "GEOM"+"_eps"+eps+"_samples"+samples+"_"+ samplingDistributionType+".txt";
		}
		else if (mechType.equals(Mechanism.KRR)) {
			mech = new PRRespMechanism(hsize, vsize, cell_side, new GeoPriv(eps));
			file_tag = "kRR"+"_eps"+eps+"_samples"+samples+"_"+ samplingDistributionType+".txt";
		}
		else if (mechType.equals(Mechanism.LAP)) {
			mech = new TPLapMechanism(hsize, vsize, cell_side, eps);
			file_tag = "LAP"+"_eps"+eps+"_samples"+samples+"_"+ samplingDistributionType+".txt";
		}
		else if (mechType.equals(Mechanism.EXP)) {
			mech = new ExponentialMechanism(hsize, vsize, cell_side, new GeoPriv(eps));
			file_tag = "EXP"+"_eps"+eps+"_samples"+samples+"_"+ samplingDistributionType+".txt";
		}
		else {
			System.out.println("Mechanism type is invalid");
			System.exit(0);
		}
        
		// Obfuscate original data using the mechanism
		System.out.println("Constructing noisy empirical data...");
		GridEmpData noisy_empdata = new GridEmpData(original_empdata,mech);
		//BitVectorEmpData noisy_empdata = new BitVectorEmpData(original_empdata, mech);
		//System.out.println("number of different bitVectors = " + noisy_empdata.data.size());
		//
		
		DoubleMatrix noisy_distribution = xspace.get_distribution_from_emp_data(noisy_empdata);
		//DoubleMatrix noisy_distribution = xspace.get_distribution_from_bitVectEmpData(noisy_empdata);
		
		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the noisy distribution");
		double statDistance = Distance.getTotalVariationDist(xspace.get_distribution_from_emp_data(original_empdata), noisy_distribution);
		System.out.println("Total Variation distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(noisy_distribution.transpose(), hsize,vsize, cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(results_base_folder+"distribution_noisy"+"_"+file_tag, true);
		
		Estimators.estimate_distribution_em(xspace, mech, noisy_empdata, 0.0001, original_empdata, Distance.TV, results_base_folder, file_tag);
		Estimators.estimate_distribution_invn(xspace, mech, noisy_empdata, original_empdata, Distance.TV,results_base_folder, file_tag);
		Estimators.estimate_distribution_invp(xspace, mech, noisy_empdata, original_empdata, Distance.TV,results_base_folder, file_tag);
	}
	
}
