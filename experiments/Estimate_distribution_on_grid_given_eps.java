package experiments;

import org.jblas.DoubleMatrix;

import priv.plan.gdset.GDataset;
import priv.plan.gdset.GSettings;
import priv.plan.mechanisms.TPGeomMechanism;
import priv.plan.priors.Prior;
import stat.distributions.Distance;
import stat.estimators.Estimators;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;

/**
 * Used to plot the distribution on the cells of a grid as a heatmap (EM paper 2019).
 * 
 * In this experiments, the original_empdata is obtained from Gowalla for the specified region with coordinates
 * (north, south, east,west) partitioned into a grid with cell_side=0.5. From this data the original 
 * distribution is constructed and saved. Then a planar geometric mechanism with a given eps obfuscates the data
 * to produce noisy_empdata. Then We estimate the original distribution by calling Estimators.estimate_distribution() with the required estimation method, the mechanism, 
 * with the mechanism and the noisy_empdata. The estimation procedure saves the estimated distribution in a file. 
 * 
 * We used this experiment with the following configuration:
 * In Figure 11: use eps = 1.0.
 * 
 **/

public class Estimate_distribution_on_grid_given_eps {
	
	static double eps = 1.0;
	
	// folder/files information for output data
	static String results_base_folder = "./results/sf_8km_12km/invn_invp_em_fixed_eps/";    // used to place the distributiosn and data file
	// the following files are used if we want to record em information at every iteration 
	//static String logLikelihood_filename_prefix =results_base_folder + "logLikelihood";   // likelihood plot file prefix
	//static String distance_filename_prefix =results_base_folder + "distance_"+distType;             // distance plot file prefix
	//static String difference_filename_prefix =results_base_folder + "diff";             // difference plot file prefix

	
	static GridEmpData original_empdata;
	static GridSecretsSpace xspace;
	
	static int hsize;
	static int vsize;
	
	static double south = 37.7228;
	static double north = 37.7946;
	static double west = -122.5153;
	static double east = -122.3789;
	static double cell_side = 0.5;  //km
	

	public static void main(String[] args) {
		System.out.println("Constructing dataset...");
		GDataset ds= new GDataset(GSettings.data_file, south, north, west, east, cell_side, ".*");
		System.out.println(ds.get_info());
		System.out.println("// The data containts "+ ds.get_number_of_distinct_checkins()+" distinct checkins");
        
		System.out.println("\nConstructing original empirical data...");
		original_empdata = ds.get_emp_data(cell_side);
		hsize = original_empdata.hsize;
		vsize = original_empdata.vsize;

		System.out.println("Constructing the space of secrets as a grid of locations ...");
		xspace = new GridSecretsSpace(hsize, vsize, cell_side);
		
		System.out.println("\nWriting original empirical distribution ("+ original_empdata.data.size() + " cells have nonzero frequencies)");
		new Prior(xspace.get_distribution_from_emp_data(original_empdata)
				.transpose(), hsize,vsize, cell_side)
		.write_prior_to_file(results_base_folder + "distribution_original"+"_cell"+cell_side+".txt",false);
		

		TPGeomMechanism mech = new TPGeomMechanism(hsize,vsize,cell_side,eps);
        System.out.println("\nProcessing Geometric mechanism...");
		String file_tag = "geom"+"_cell"+cell_side+"_eps"+eps+".txt";
		
/*		
		TPLapMechanism lmech = new TPLapMechanism(hsize,vsize,cell_side,eps);
        System.out.println("\nProcessing Laplace mechanism...");
		process_mechanism(lmech, "lap"+"_cell"+cell_side+"_priv"+eps+".txt");
		
		PRRespMechanism mech = new PRRespMechanism(hsize,vsize,cell_side, new GeoPriv(eps)); 
        System.out.println("\nProcessing Randomised Response mechanism...");
        String file_tag = "rr"+"_cell"+cell_side+"_eps"+eps+".txt";
*/
		// Obfuscate original data using the mechanism
		System.out.println("Constructing noisy empirical data...");
		GridEmpData noisy_empdata = new GridEmpData(original_empdata,mech);

		
		// write estimated distribution with TV distance to original distribution
		System.out.println("\nWriting the noisy distribution");
		DoubleMatrix noisy_distribution = xspace.get_distribution_from_emp_data(noisy_empdata);

		//double statDistance = Distance.getTotalVariationDist(xspace.get_distribution_from_emp_data(original_empdata), noisy_distribution);
		double statDistance = Distance.getEMDDist(xspace.get_distribution_from_emp_data(original_empdata), noisy_distribution, xspace);
		System.out.println("Statistical distance to original distribution = " + statDistance);
		Prior p_estimated = new Prior(noisy_distribution.transpose(), hsize,vsize, cell_side);
		p_estimated.add_info("# TV to original distribution= " + statDistance);  // save the statistical distance to original distribution
		p_estimated.write_prior_to_file(results_base_folder+"distribution_noisy"+"_"+file_tag, true);
	
        Estimators.estimate_distribution_em(xspace, mech, noisy_empdata, 0.0001, original_empdata, Distance.TV, results_base_folder, file_tag);
		Estimators.estimate_distribution_invn(xspace, mech, noisy_empdata, original_empdata, Distance.TV, results_base_folder, file_tag);
		Estimators.estimate_distribution_invp(xspace, mech, noisy_empdata, original_empdata, Distance.TV, results_base_folder, file_tag);
	
	}


}
