package experiments;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.jblas.DoubleMatrix;

import priv.plan.mechanisms.BasicOneTimeRappor;
import priv.plan.mechanisms.Mechanism;
import priv.plan.priors.Prior;
import stat.distributions.Distance;
import stat.distributions.Distributions;
import stat.distributions.Sampling;
import stat.estimators.*;
import stat.spaces.BitVectorEmpData;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;


/**
 * (in the EM paper 2019)
 * 
 * This experiment program obfuscates numeric data {0,1,..., 9} using Basic One-Time RAPPOR 
 * and then estimate the original distribution, and plot the total variation distance 
 * between original and estimated distributions in a boxplot based on 100 runs. 
 * The real data are sampled from two distributions: Binomial with p=0.5, 
 * and Uniform on {3,4,5,6} and 0 elsewhere.
 * It uses the values of epsilon = 0.1, 0.2, 0.5, 1.0, 2.0, 3.0,,3.0,,5.0
 *
 * we set em_error = 0.000001 in the case of rappor
 *
 * We used this program with the following configuration:
 * For Figure 16(a) use Distributions.BINOMIAL
 * For Figure 16(b) use Distributions.UNIFORM

 * 
 * This experiment produces 6 files for the given mechanism and eps value every one is a column:
 * 1. logLikelihood_rappor_invn_botrappor_cell{cell_side}_eps{eps}.txt : 
 *     every value is the likelihood of the rappor_invn estimator resulting from the mechanism satisfying eps.

 * 2. distance_rappor_invn_botrappor_cell{cell_side}_dist{distanceType}.txt : 
 *    every value is the distance of the rappor_invn estimator resulting from the mechanism satisfying eps.

 * 3. logLikelihood_rappor_invp_botrappor_cell{cell_side}_eps{eps}.txt : 
 *     every value is the likelihood of the rappor_invp estimator resulting from the mechanism satisfying eps.

 * 4. distance_rappor_invp_botrappor_cell{cell_side}_dist{distanceType}.txt : 
 *    every value is the distance of the rappor_invp estimator resulting from the mechanism satisfying eps.

 * 5. logLikelihood_rappor_em_botrappor_cell{cell_side}_eps{eps}.txt : 
 *    a column file where every value is the likelihood of the rappor_em estimator resulting from the mechanism satisfying eps.

 * 6. distance_rappor_em_botrappor_cell{cell_side}_dist{distanceType}.txt : 
 *    every value is the distance of the rappor_em estimator resulting from the mechanism satisfying eps.
 */


public class Compare_estimators_on_line_with_rappor_varying_eps_box {

	// space specification
	static final double cell_side =1;
	static final int hsize=1;   // makes the grid one dimensional
	static int vsize=10;

	// original data
	static int samples = 100000;   
	static String originalDistributionType = Distributions.UNIFORM;
	static String distanceType = Distance.TV;

	
	// mechanism
	static String mechType = Mechanism.BOTRAPPOR;
	
	// em parameters
	static int em_max_iterations = 50000; // max iterations
	static double error = 0.000001;  
	
	// folder/files information for output data
	static String results_base_folder = "./results/linear_space/compare_estimators_varying_eps_boxplot/"
	                  +mechType+ "/"+ originalDistributionType+"/";

	static String logLikelihood_em_filename_prefix =results_base_folder + "logLikelihood_em";  // one file for every mechanism type
	static String distance_em_filename_prefix =results_base_folder + "distance_em";            // one file for every mechanism type and distance type 
	
	static String logLikelihood_invn_filename_prefix =results_base_folder + "logLikelihood_invn";  // one file for every mechanism type
	static String distance_invn_filename_prefix =results_base_folder + "distance_invn";            // one file for every mechanism type and distance type 
	
	static String logLikelihood_invp_filename_prefix =results_base_folder + "logLikelihood_invp";  // one file for every mechanism type
	static String distance_invp_filename_prefix =results_base_folder + "distance_invp";            // one file for every mechanism type and distance type 

	
	static GridEmpData original_empdata;
	static DoubleMatrix original_distribution_on_secrets;  // distribution obtained from original_empdata (fixed)
	static GridSecretsSpace xspace;
	
		
	// the following are only for debugging	
	//static Prior unifPrior;

	
	public static void main(String[] args) {

		System.out.println("\nConstructing original empirical data...");
		
		original_empdata = sampleOriginalData();

//		unifPrior = new Prior(hsize,vsize,cell_side);  // uniform prior

		System.out.println("Constructing the space of secrets as a grid of locations ...");
		xspace = new GridSecretsSpace(hsize, vsize, cell_side);
		
		original_distribution_on_secrets = xspace.get_distribution_from_emp_data(original_empdata);
		
		System.out.println("\nWriting original empirical distribution ("+ original_empdata.data.size() + " cells have nonzero frequencies)");
		
		new Prior(xspace.get_distribution_from_emp_data(original_empdata)
				.transpose(), hsize,vsize, cell_side)
		.write_prior_to_file(results_base_folder + "distribution_original"+"_cell"+cell_side+".txt",false); 
		
		process_otrappor_mechanism(distanceType, 0.1,100);        
		process_otrappor_mechanism(distanceType, 0.2,100);        
		process_otrappor_mechanism(distanceType, 0.5,100);        
		process_otrappor_mechanism(distanceType, 1.0,100);        
		process_otrappor_mechanism(distanceType, 2.0,100);        
		process_otrappor_mechanism(distanceType, 3.0,100);        
		process_otrappor_mechanism(distanceType, 4.0,100);        
		process_otrappor_mechanism(distanceType, 5.0,100);        

	}
	
	
	private static void process_otrappor_mechanism(String p_distType, double p_eps, int p_runs) {

		//String likelihood_em_file = logLikelihood_em_filename_prefix+"_"+Mechanism.RAPPOR+"_cell"+cell_side+"_eps"+p_eps+".txt";
		String distance_em_file = distance_em_filename_prefix+"_"+Mechanism.BOTRAPPOR+"_cell"+cell_side+"_dist"+p_distType+"_eps"+p_eps+".txt";

		//String likelihood_invn_file = logLikelihood_invn_filename_prefix+"_"+Mechanism.RAPPOR+"_cell"+cell_side+"_eps"+p_eps+".txt";
		String distance_invn_file = distance_invn_filename_prefix+"_"+Mechanism.BOTRAPPOR+"_cell"+cell_side+"_dist"+p_distType+"_eps"+p_eps+".txt";
		
		//String likelihood_invp_file = logLikelihood_invp_filename_prefix+"_"+Mechanism.RAPPOR+"_cell"+cell_side+"_eps"+p_eps+".txt";
		String distance_invp_file = distance_invp_filename_prefix+"_"+Mechanism.BOTRAPPOR+"_cell"+cell_side+"_dist"+p_distType+"_eps"+p_eps+".txt";

		//BufferedWriter likelihood_em_BufferedWriter = null;
		BufferedWriter distance_em_BufferedWriter = null;

		//BufferedWriter likelihood_invn_BufferedWriter = null;
		BufferedWriter distance_invn_BufferedWriter = null;

		//BufferedWriter likelihood_invp_BufferedWriter = null;
		BufferedWriter distance_invp_BufferedWriter = null;
//DecimalFormat eps_format = new DecimalFormat("0.0"); // format for eps

		//double exploss = min_exp_loss-step; //km
		//double eps = eps_min; //km

		try {
			//likelihood_em_BufferedWriter = new BufferedWriter(new FileWriter(likelihood_em_file));
			distance_em_BufferedWriter = new BufferedWriter(new FileWriter(distance_em_file));

			//likelihood_invn_BufferedWriter = new BufferedWriter(new FileWriter(likelihood_invn_file));
			distance_invn_BufferedWriter = new BufferedWriter(new FileWriter(distance_invn_file));

			//likelihood_invp_BufferedWriter = new BufferedWriter(new FileWriter(likelihood_invp_file));
			distance_invp_BufferedWriter = new BufferedWriter(new FileWriter(distance_invp_file));

			System.out.println("Constructing Mechanism "+Mechanism.BOTRAPPOR+ "for epsilon = "+ p_eps+" for "+p_runs+" runs");
				
				BasicOneTimeRappor mech = new BasicOneTimeRappor(hsize,vsize,cell_side, p_eps);
				
				for (int k=0; k<p_runs;k++) {
				// to debug
				System.out.println("Processing epsilon = " + p_eps +", run = "+ k);
			 	//	mech.get_exp_loss_for_prior(unifPrior, loss ));
				System.out.println("Constructing noisy empirical data...");
				BitVectorEmpData noisy_empdata = new BitVectorEmpData(original_empdata, mech);

				// estimate and produce plot data
				System.out.println("\nestimating using Rappor_EM method....");
				Estimator_BOTRappor_EM em = new Estimator_BOTRappor_EM(xspace, mech, noisy_empdata);
				DoubleMatrix pr_x_em = em.getMaximizer(error, em_max_iterations);

				// estimate and produce plot data
				System.out.println("\nestimating using Rappor_INVN method....");
				Estimator_BOTRappor_INVN invn = new Estimator_BOTRappor_INVN(xspace, mech, noisy_empdata);
				DoubleMatrix pr_x_invn = invn.getEstimator();

				// estimate and produce plot data
				System.out.println("\nestimating using Rappor_INVP method....");
				Estimator_BOTRappor_INVP invp = new Estimator_BOTRappor_INVP(xspace, mech, noisy_empdata);
				DoubleMatrix pr_x_invp = invp.getEstimator();
				
				//DoubleMatrix pr_z_giv_x = mech.get_mech_matrix();
				//DoubleMatrix emp_pr_z = xspace.get_distribution_from_emp_data(noisy_empdata);
				//int total_observations = noisy_empdata.total_observations;

				//double emLikelihood = Likelihood.get_log_likelihood(pr_x_em, pr_z_giv_x, emp_pr_z, total_observations);
				double emDistance = getDistToOrigDistribution(pr_x_em, p_distType);
				//double invnLikelihood = Likelihood.get_log_likelihood(pr_x_invn, pr_z_giv_x, emp_pr_z, total_observations);
				double invnDistance = getDistToOrigDistribution(pr_x_invn, p_distType);
				//double invpLikelihood = Likelihood.get_log_likelihood(pr_x_invp, pr_z_giv_x, emp_pr_z, total_observations);
				double invpDistance = getDistToOrigDistribution(pr_x_invp, p_distType);

				//likelihood_em_BufferedWriter.write(emLikelihood +"\n"); 
				distance_em_BufferedWriter.write(emDistance +"\n"); 
				//likelihood_invn_BufferedWriter.write(invnLikelihood +"\n"); 
				distance_invn_BufferedWriter.write(invnDistance +"\n");
				//likelihood_invp_BufferedWriter.write(invpLikelihood +"\n"); 
				distance_invp_BufferedWriter.write(invpDistance +"\n");
				System.out.println("Distances: = em =" + emDistance +", invn="+ invnDistance +", invp="+invpDistance);
				}
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			//Close the BufferedWrites
			try {
//				if (likelihood_em_BufferedWriter != null) {
//					likelihood_em_BufferedWriter.flush();
//					likelihood_em_BufferedWriter.close();
//				}
				if (distance_em_BufferedWriter != null) {
					distance_em_BufferedWriter.flush();
					distance_em_BufferedWriter.close();
				}
//				if (likelihood_invn_BufferedWriter != null) {
//					likelihood_invn_BufferedWriter.flush();
//					likelihood_invn_BufferedWriter.close();
//				}
				if (distance_invn_BufferedWriter != null) {
					distance_invn_BufferedWriter.flush();
					distance_invn_BufferedWriter.close();
				}
//				if (likelihood_invp_BufferedWriter != null) {
//					likelihood_invp_BufferedWriter.flush();
//					likelihood_invp_BufferedWriter.close();
//				}
				if (distance_invp_BufferedWriter != null) {
					distance_invp_BufferedWriter.flush();
					distance_invp_BufferedWriter.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}

	
	// get the original data by sampling from the assumed distribution.
	private static GridEmpData sampleOriginalData () {
	    int[] freq_array = new int[vsize];		
	    if (originalDistributionType.equals(Distributions.BINOMIAL)) 
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getBinomial(vsize-1, 0.5);
			freq_array[sample]++;
			}
	    else if (originalDistributionType.contentEquals(Distributions.UNIFORM)) {
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getUniform( (int)(0.3*vsize)  , (int)(0.6*vsize) ) ; 
			freq_array[sample]++;
			}
	    }
		return new GridEmpData(hsize, vsize, freq_array);
	}
	

	// return the distance the original distribution depending on the distance type
	private static double getDistToOrigDistribution(DoubleMatrix p_pr_x, String p_distType) {
		double dist=0;
		if (p_distType.equals(Distance.TV)) 
			dist = Distance.getTotalVariationDist(original_distribution_on_secrets, p_pr_x);
		else if (p_distType.equals(Distance.KL))
			dist = Distance.getSymKLDiff(original_distribution_on_secrets, p_pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			dist = Distance.getEMDDist(original_distribution_on_secrets, p_pr_x, xspace);
		else {
			System.out.println("invalide distance type");
			System.exit(0);
		}
		return dist;
	}
	

}
