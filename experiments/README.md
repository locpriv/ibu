# Experiments
The following programs have been used to produce the results reported in the paper: Full convergence of the Expectation-Maximization method and application to mechanisms for privacy protection (EuroS&P 2020).
Each one of these programs is run using different settings to serve many experiments.  
 

## Program 1: 
Source file: [experiments/Compare_estimators_on_grid_varying_eps_box.java](https://gitlab.com/locpriv/jpriv/blob/master/experiments/Compare_estimators_on_grid_varying_eps_box.java)
 
This experiment program obfuscates the real location data in the specified region (north, south, east, west) from the Gowalla dataset, using a mechanism and then estimates the original distribution using EM, INV-N, INV-P, and plot the specified statistical distance between original and estimated distributions (by each method) in a boxplot based on 100 runs. 
  
We performed this experiment with the following configurations:
- For Figure 12(a): mechType = Mechanism.GEOM, distanceType = Distance.TV
- For Figure 12(b): mechType = Mechanism.GEOM, distanceType = Distance.EMD
- For Figure 13(a): mechType = Mechanism.LAP, distanceType = Distance.TV
- For Figure 13(b): mechType = Mechanism.EXP, distanceType = Distance.TV
- For Figure 14(a): mechType = Mechanism.KRR, distanceType = Distance.TV
- For Figure 14(b): mechType = Mechanism.KRR, distanceType = Distance.EMD
  
  
This experiment produces 6 files for a given mechanism and eps value every one is a column:
1. logLikelihood_invn_{mechType}_cell{cell_side}_eps{eps}.txt : 
every value is the likelihood of the INV-N estimator resulting from the mechanism satisfying eps.
2. logLikelihood_invp_{mechType}_cell{cell_side}_eps{eps}.txt : 
every value is the likelihood of the INV-P estimator resulting from the mechanism satisfying eps.
3. logLikelihood_em_{mechType}_cell{cell_side}_eps{eps}.txt : every value is the likelihood of the EM estimator resulting from the mechanism satisfying eps.
4. distance_invn_{mechanismType}_cell{cell_side}_dist{distanceType}.txt : 
every value is the distance of the INV-N estimator resulting from the mechanism satisfying eps.
5. distance_invp_{mechanismType}_cell{cell_side}_dist{distanceType}.txt : 
every value is the distance of the INV-P estimator resulting from the mechanism satisfying eps.
6. distance_em_{mechanismType}_cell{cell_side}_dist{distanceType}.txt : 
every value is the distance of the EM estimator resulting from the mechanism satisfying eps.

## Program 2: 
Source file: [experiments/Compare_estimators_on_line_with_rappor_varying_eps_box.java](https://gitlab.com/locpriv/jpriv/blob/master/experiments/Compare_estimators_on_line_with_rappor_varying_eps_box.java)

This experiment program obfuscates numeric data {0,1,..., 9} using Basic One-Time RAPPOR and then estimate the original distribution, and plot the total variation distance between original and estimated distributions in a boxplot based on 100 runs. The real data are sampled from two distributions: Binomial with p=0.5, and Uniform on {3,4,5,6} and 0 elsewhere. It uses the values of epsilon = 0.1, 0.2, 0.5, 1.0, 2.0, 3.0,,3.0,,5.0.

We performed this experiment with the following configuration:
- For Figure 16(a): Distributions.BINOMIAL
- For Figure 16(b): Distributions.UNIFORM
 
This experiment produces 6 files (each contains one column of data) for the given mechanism and eps value:
1. logLikelihood_rappor_invn_botrappor_cell{cell_side}_eps{eps}.txt : every value is the likelihood of the rappor_invn estimator resulting from the mechanism satisfying eps.
2. distance_rappor_invn_botrappor_cell{cell_side}_dist{distanceType}.txt : every value is the distance of the rappor_invn estimator resulting from the mechanism satisfying eps.
3. logLikelihood_rappor_invp_botrappor_cell{cell_side}_eps{eps}.txt : every value is the likelihood of the rappor_invp estimator resulting from the mechanism satisfying eps.
4. distance_rappor_invp_botrappor_cell{cell_side}_dist{distanceType}.txt : every value is the distance of the rappor_invp estimator resulting from the mechanism satisfying eps.
5. logLikelihood_rappor_em_botrappor_cell{cell_side}_eps{eps}.txt : every value is the likelihood of the rappor_em estimator resulting from the mechanism satisfying eps.
6. distance_rappor_em_botrappor_cell{cell_side}_dist{distanceType}.txt : every value is the distance of the rappor_em estimator resulting from the mechanism satisfying eps.

## Program 3: 
Source file: [experiments/Estimate_distribution_on_grid_given_eps.java](https://gitlab.com/locpriv/jpriv/blob/master/experiments/Estimate_distribution_on_grid_given_eps.java)

Used to plot the distribution on the cells of a grid as a heatmap. In this experiments, the original_empdata is obtained from Gowalla for the specified region with coordinates (north, south, east,west) partitioned into a grid with cell_side=0.5. From this data the original distribution is constructed and saved. Then a planar geometric mechanism with a given eps obfuscates the data to produce noisy_empdata. Then We estimate the original distribution by calling Estimators.estimate_distribution() with the required estimation method, the mechanism, with the mechanism and the noisy_empdata. The estimation procedure saves the estimated distribution in a file. 
 
We performed this experiment with the following configuration:
- In Figure 11: eps = 1.0.

## Program 4: 
Source file: [experiments/Estimate_distribution_on_line_given_eps.java](https://gitlab.com/locpriv/jpriv/blob/master/experiments/Estimate_distribution_on_line_given_eps.java)

Used to plot the distribution on a range of integers. In this experiment data are sampled from the set {0,1,..., 99} using a given hypothetical distribution. The data are obfuscated by a given mechanism with eps=0.1 to generate noisy data. The noisy data are used by INVN, INVP, EM estimators to learn the original distribution (on the original data). We use two distributions to sample the original data: Binomial with p=0.5, and Uniform on {20, ...,40} and 0 otherwise. The plot data of the original and noisy distributions are stored, in addition to the estimated ones produced by EM, INVN, INVP.
 
We performed this experiment with the following configurations:
- For Figure 1(a), 1(c), 1(e): mechType = Mechanism.GEOM, samplingDistributionType = Distributions.BINOMIAL
- For Figure 1(b), 1(d), 1(f): mechType = Mechanism.GEOM, distanceType = Distributions.UNIFORM
  

## Program 5: 
Source file: [experiments/Estimate_distribution_on_line_with_rappor_given_eps.java](https://gitlab.com/locpriv/jpriv/blob/master/experiments/Estimate_distribution_on_line_with_rappor_given_eps.java)
 
Used to plot estimated distributions with basic one-time rappor on a range of integers. In this experiment data are sampled from the set {0,1,..., 9} using a given hypothetical distribution. The data are obfuscated by a BOTRAPPOR mechanism with eps=0.5 to generate noisy data. Noisy distribution is induced by the frequencies of bits in the noisy bit vectors. The noisy data are then used by BOTRAPPOR_INVN, BOTRAPPOR_INVP, BOTRAPPOR_EM estimators to learn the original distribution (on the original data). We use two distributions to sample the original data: Binomial with p=0.5, and Uniform on {3,4,5,6} and 0 otherwise. The plot data of the original and noisy distributions are stored, in addition to the estimated ones.

We performed this experiment with the following configurations:
- For Figure 15(a): samplingDistributionType = Distributions.BINOMIAL , samples = 100000, eps = 0.5
- For Figure 15(b): samplingDistributionType = Distributions.UNIFORM , samples = 100000, eps = 0.5