package experiments;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.jblas.DoubleMatrix;

import priv.line.mechanisms.TGeomMechanism;
import priv.plan.mechanisms.ExponentialMechanism;
import priv.plan.mechanisms.Mechanism;
import priv.plan.mechanisms.PRRespMechanism;
import priv.plan.mechanisms.TPLapMechanism;
import priv.plan.priors.Prior;
import priv.plan.privMetrics.GeoPriv;
import stat.distributions.Distance;
import stat.distributions.Distributions;
import stat.distributions.Likelihood;
import stat.distributions.Sampling;
import stat.estimators.*;
import stat.spaces.GridEmpData;
import stat.spaces.GridSecretsSpace;


/**
 * (in the EM paper 2019)
 * 
 * This experiment program obfuscates numeric data {0,1,..., vsize} using a mechanism and then 
 * estimate the original distribution, and plot the total variation distance 
 * between original and estimated distributions in a boxplot based on 100 runs. 
 * The real data are sampled from two distributions: Binomial with p=0.5, 
 * and Uniform on {3,4,5,6} and 0 elsewhere.
 *
 * We used this experiment with the following configurations:
 *          (not used in this paper)
 *  
 * 
 * This experiment produces 6 files for a given mechanism and eps value every one is a column:
 * 1. logLikelihood_invn_{mechTyple}_cell{cell_side}_eps{eps}.txt : 
 *     every value is the likelihood of the invn estimator resulting from the mechanism satisfying eps.

 * 2. distance_invn_{mechTyple}_cell{cell_side}_dist{distanceType}.txt : 
 *    every value is the distance of the invn estimator resulting from the mechanism satisfying eps.

 * 3. logLikelihood_invp_{mechTyple}_cell{cell_side}_eps{eps}.txt : 
 *     every value is the likelihood of the invp estimator resulting from the mechanism satisfying eps.

 * 4. distance_invp_{mechTyple}_cell{cell_side}_dist{distanceType}.txt : 
 *    every value is the distance of the invp estimator resulting from the mechanism satisfying eps.

 * 5. logLikelihood_em_{mechType}_cell{cell_side}_eps{eps}.txt : 
 *    a column file where every value is the likelihood of the em estimator resulting from the mechanism satisfying eps.

 * 6. distance_em_{mechType}_cell{cell_side}_dist{distanceType}.txt : 
 *    every value is the distance of the em estimator resulting from the mechanism satisfying eps.
 */


public class Compare_estimators_on_line_varying_eps_box {

	// space specification
	static final double cell_side =1;
	static final int hsize=1;   // to force the grid to be one dimensional
	static int vsize=10;

	// original data
	static int samples = 100000;   
	static String originalDistributionType = Distributions.UNIFORM;
	static String distanceType = Distance.TV;

	
	// mechanism
	static String mechType = Mechanism.GEOM;
	
	// em parameters
	static int em_max_iterations = 50000; // max iterations
	static double error = 0.0001;  
	
	// folder/files information for output data
	static String results_base_folder = "./results/linear_space/compare_estimators_varying_eps_boxplot/"
	                  +mechType+ "/"+ originalDistributionType+"/";

	static String logLikelihood_em_filename_prefix =results_base_folder + "logLikelihood_em";  // one file for every mechanism type
	static String distance_em_filename_prefix =results_base_folder + "distance_em";            // one file for every mechanism type and distance type 
	
	static String logLikelihood_invn_filename_prefix =results_base_folder + "logLikelihood_invn";  // one file for every mechanism type
	static String distance_invn_filename_prefix =results_base_folder + "distance_invn";            // one file for every mechanism type and distance type 
	
	static String logLikelihood_invp_filename_prefix =results_base_folder + "logLikelihood_invp";  // one file for every mechanism type
	static String distance_invp_filename_prefix =results_base_folder + "distance_invp";            // one file for every mechanism type and distance type 

	
	static GridEmpData original_empdata;
	static DoubleMatrix original_distribution_on_secrets;  // distribution obtained from original_empdata (fixed)
	static GridSecretsSpace xspace;
	
	
	public static void main(String[] args) {

		System.out.println("\nConstructing original empirical data...");
		
		original_empdata = sampleOriginalData();

//		unifPrior = new Prior(hsize,vsize,cell_side);  // uniform prior

		System.out.println("Constructing the space of secrets as a grid of locations ...");
		xspace = new GridSecretsSpace(hsize, vsize, cell_side);
		
		original_distribution_on_secrets = xspace.get_distribution_from_emp_data(original_empdata);
		
		System.out.println("\nWriting original empirical distribution ("+ original_empdata.data.size() + " cells have nonzero frequencies)");
		
		new Prior(xspace.get_distribution_from_emp_data(original_empdata)
				.transpose(), hsize,vsize, cell_side)
		.write_prior_to_file(results_base_folder + "distribution_original"+"_cell"+cell_side+".txt",false); 

		process_mechanism(mechType, originalDistributionType, 0.2,1);       
		process_mechanism(mechType, originalDistributionType, 0.5,1);       
		process_mechanism(mechType, originalDistributionType, 1.0,1);       
		process_mechanism(mechType, originalDistributionType, 2.0,1);       
		process_mechanism(mechType, originalDistributionType, 3.0,1);       
		process_mechanism(mechType, originalDistributionType, 4.0,1);       
		process_mechanism(mechType, originalDistributionType, 5.0,1);       
		process_mechanism(mechType, originalDistributionType, 6.0,1);       

/*
		process_mechanism(Mechanism.KRR, Distance.KL, 1.0,100);        
		process_mechanism(Mechanism.KRR, Distance.KL, 2.0,100);        
		process_mechanism(Mechanism.KRR, Distance.KL, 3.0,100);        
		process_mechanism(Mechanism.KRR, Distance.KL, 4.0,100);        
		process_mechanism(Mechanism.KRR, Distance.KL, 5.0,100);      
		process_mechanism(Mechanism.KRR, Distance.KL, 6.0,100);      
		process_mechanism(Mechanism.KRR, Distance.KL, 7.0,100);      
		process_mechanism(Mechanism.KRR, Distance.KL, 8.0,100);      
		process_mechanism(Mechanism.KRR, Distance.KL, 9.0,100);      
		process_mechanism(Mechanism.KRR, Distance.KL, 10.0,100);     
*/

	}
	
	private static void process_mechanism(String p_mechType, String p_distType, double p_eps, int p_runs) {

		String likelihood_em_file = logLikelihood_em_filename_prefix+"_"+p_mechType+"_cell"+cell_side+"_eps"+p_eps+".txt";
		String distance_em_file = distance_em_filename_prefix+"_"+p_mechType+"_cell"+cell_side+"_dist"+p_distType+"_eps"+p_eps+".txt";

		String likelihood_invn_file = logLikelihood_invn_filename_prefix+"_"+p_mechType+"_cell"+cell_side+"_eps"+p_eps+".txt";
		String distance_invn_file = distance_invn_filename_prefix+"_"+p_mechType+"_cell"+cell_side+"_dist"+p_distType+"_eps"+p_eps+".txt";
		
		String likelihood_invp_file = logLikelihood_invp_filename_prefix+"_"+p_mechType+"_cell"+cell_side+"_eps"+p_eps+".txt";
		String distance_invp_file = distance_invp_filename_prefix+"_"+p_mechType+"_cell"+cell_side+"_dist"+p_distType+"_eps"+p_eps+".txt";

		BufferedWriter likelihood_em_BufferedWriter = null;
		BufferedWriter distance_em_BufferedWriter = null;

		BufferedWriter likelihood_invn_BufferedWriter = null;
		BufferedWriter distance_invn_BufferedWriter = null;

		BufferedWriter likelihood_invp_BufferedWriter = null;
		BufferedWriter distance_invp_BufferedWriter = null;

		try {
			likelihood_em_BufferedWriter = new BufferedWriter(new FileWriter(likelihood_em_file));
			distance_em_BufferedWriter = new BufferedWriter(new FileWriter(distance_em_file));

			likelihood_invn_BufferedWriter = new BufferedWriter(new FileWriter(likelihood_invn_file));
			distance_invn_BufferedWriter = new BufferedWriter(new FileWriter(distance_invn_file));

			likelihood_invp_BufferedWriter = new BufferedWriter(new FileWriter(likelihood_invp_file));
			distance_invp_BufferedWriter = new BufferedWriter(new FileWriter(distance_invp_file));

			System.out.println("Constructing Mechanism "+p_mechType+ "for epsilon = "+ p_eps+" for "+p_runs+" runs");
				
				Mechanism mech;
				if (p_mechType.equals(Mechanism.GEOM))
					mech = new TGeomMechanism(vsize,cell_side, p_eps);
				else if (p_mechType.equals(Mechanism.LAP))
					mech = new TPLapMechanism(hsize,vsize,cell_side, p_eps);
				else if (p_mechType.equals(Mechanism.EXP))
					mech = new ExponentialMechanism(hsize,vsize,cell_side, new GeoPriv(p_eps));
				else if (p_mechType.equals(Mechanism.KRR))
					mech = new PRRespMechanism(hsize,vsize,cell_side, new GeoPriv(p_eps));
				else {
					System.out.println("Invalid mechanism type");
					mech =null;
					System.exit(0);
				}
				
				
				for (int k=0; k<p_runs;k++) {
				// to debug
				System.out.println("Processing epsilon = " + p_eps +", run = "+ k);
			 	//	mech.get_exp_loss_for_prior(unifPrior, loss ));
				System.out.println("Constructing noisy empirical data...");
				GridEmpData noisy_empdata = new GridEmpData(original_empdata,mech);

				// estimate and produce plot data
				System.out.println("\nestimating using EM method....");
				Estimator_EM em = new Estimator_EM(xspace, mech, noisy_empdata);
				DoubleMatrix pr_x_em = em.getMaximizer(error, em_max_iterations);

				// estimate and produce plot data
				System.out.println("\nestimating using INVN method....");
				Estimator_INVN invn = new Estimator_INVN(xspace, mech, noisy_empdata);
				DoubleMatrix pr_x_invn = invn.getMaximizer();

				// estimate and produce plot data
				System.out.println("\nestimating using INVP method....");
				Estimator_INVP invp = new Estimator_INVP(xspace, mech, noisy_empdata);
				DoubleMatrix pr_x_invp = invp.getMaximizer();
				
				DoubleMatrix pr_z_giv_x = mech.get_mech_matrix();
				DoubleMatrix emp_pr_z = xspace.get_distribution_from_emp_data(noisy_empdata);
				int total_observations = noisy_empdata.total_observations;

				double emLikelihood = Likelihood.get_log_likelihood(pr_x_em, pr_z_giv_x, emp_pr_z, total_observations);
				double emDistance = getDistToOrigDistribution(pr_x_em, p_distType);
				double invnLikelihood = Likelihood.get_log_likelihood(pr_x_invn, pr_z_giv_x, emp_pr_z, total_observations);
				double invnDistance = getDistToOrigDistribution(pr_x_invn, p_distType);
				double invpLikelihood = Likelihood.get_log_likelihood(pr_x_invp, pr_z_giv_x, emp_pr_z, total_observations);
				double invpDistance = getDistToOrigDistribution(pr_x_invp, p_distType);

				likelihood_em_BufferedWriter.write(emLikelihood +"\n"); 
				distance_em_BufferedWriter.write(emDistance +"\n"); 
				likelihood_invn_BufferedWriter.write(invnLikelihood +"\n"); 
				distance_invn_BufferedWriter.write(invnDistance +"\n");
				likelihood_invp_BufferedWriter.write(invpLikelihood +"\n"); 
				distance_invp_BufferedWriter.write(invpDistance +"\n");
				System.out.println("Distances: = em =" + emDistance +", invn="+ invnDistance +", invp="+invpDistance);
				}
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			//Close the BufferedWrites
			try {
				if (likelihood_em_BufferedWriter != null) {
					likelihood_em_BufferedWriter.flush();
					likelihood_em_BufferedWriter.close();
				}
				if (distance_em_BufferedWriter != null) {
					distance_em_BufferedWriter.flush();
					distance_em_BufferedWriter.close();
				}
				if (likelihood_invn_BufferedWriter != null) {
					likelihood_invn_BufferedWriter.flush();
					likelihood_invn_BufferedWriter.close();
				}
				if (distance_invn_BufferedWriter != null) {
					distance_invn_BufferedWriter.flush();
					distance_invn_BufferedWriter.close();
				}
				if (likelihood_invp_BufferedWriter != null) {
					likelihood_invp_BufferedWriter.flush();
					likelihood_invp_BufferedWriter.close();
				}
				if (distance_invp_BufferedWriter != null) {
					distance_invp_BufferedWriter.flush();
					distance_invp_BufferedWriter.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}
	
	
	// get the original data by sampling from the assumed distribution.
	private static GridEmpData sampleOriginalData () {
	    int[] freq_array = new int[vsize];		
	    if (originalDistributionType.equals(Distributions.BINOMIAL)) 
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getBinomial(vsize-1, 0.5);
			freq_array[sample]++;
			}
	    else if (originalDistributionType.contentEquals(Distributions.UNIFORM)) {
	    	for (int i=0;i<samples;i++) {
			int sample = Sampling.getUniform( (int)(0.3*vsize)  , (int)(0.6*vsize) ) ;  //geometric: use 0.2..0.4. Rappor: 0.3 .. 0.6
			freq_array[sample]++;
			}
	    }
		return new GridEmpData(hsize, vsize, freq_array);
	}
	

	// return the distance the original distribution depending on the distance type
	private static double getDistToOrigDistribution(DoubleMatrix p_pr_x, String p_distType) {
		double dist=0;
		if (p_distType.equals(Distance.TV)) 
			dist = Distance.getTotalVariationDist(original_distribution_on_secrets, p_pr_x);
		else if (p_distType.equals(Distance.KL))
			dist = Distance.getSymKLDiff(original_distribution_on_secrets, p_pr_x);
		else if (p_distType.equals(Distance.EMD)) 
			dist = Distance.getEMDDist(original_distribution_on_secrets, p_pr_x, xspace);
		else {
			System.out.println("invalide distance type");
			System.exit(0);
		}
		return dist;
	}
	

}
